#!/usr/bin/perl

use strict;

use IO::File;

our $DAGS_DIR;
our $RSLT_DIR;
our $EASY_RSLT_DIR;
our $LOGS_DIR;
our $ODAGS_DIR;
our $TIME;
our $CFGANALYSER;
our $ANALYSER;
our $FLTANALYSER;
our $ROBANALYSER;
our $LOG_FILE_NAME;
our $FAILURES_FILE_NAME;
our $step_max=2;
our $epsilon;
require("sxlfg_conf");

my $flt_nbest=0;
my $std_nbest=0;
my $rob_nbest=0;

my $i=0;
my $flt_cp=2;
my $std_cp=2;
my $rob_cp=2;

my $replace_old_run=0;
my $parse_selected_sentence=0;
my $parse_previously_unanalysed_sentence=0;
my $file;
my $file_with_sentenceslist;
my $num;
my $snum;
my $lsnum;
my $order=0;
my $order_kind;
my $die_on_warning=0;
my $parse=1;
my $listsentences=0;
my $segment=0;
my $log=1;
my $failures=0;
my $timeout=0;
my $stimeout=$timeout;
my $view_stderr=0;
my $view_stdout=0;
my $view_infos=0;
my $view_warnings=1;
my $view_cmd=0;
my $view_parser=0;
my $save_measures=0;
my $robust=0;
my $filter=0;
my $no_std=0;
my $timed_out=0;
my $segment_on_timeout=0;
my $ok_if_no_rcvr=0; # default = ok if consistent main f_structure
my %files;
my $failure_id="";
my $failure_string="";
my $warning_string="";
my %result;
my $sentence_easyID=0;
my @last_group_id=();
my @last_relation_id=();
my $no_analysis_output=0;
my $save_output_dag=0;
my $only_CFG=0;
my %logfilehandle;
my $parse_options;
my %dags_hash=();
my $skip_uw_only_sentences=1;
my $sb_segment=0;
my $min_weights_file=""; my @min_weights;
my $max_length=0;
my ($fid,$last_fid);
my $to_be_pushed = "";
my $current_orig_group_id;
my $easy=0;

my $did_log;

my $runname=`date`;
chomp ($runname);
$runname=~s/[: ]+/_/g;

while (1) {
    $_=shift;
    $parse_options.=" ".$_;
    if (/^$/) {last}
    elsif (/^-s=(.*\.dag(?::[0-9\-]+)?)$/) {$parse_selected_sentence=1; $file=$1}
    elsif (/^-rn=(.*)$/ || /^--run_?name=(.*)/) {$runname=$1;}
    elsif (/^-a$/ && $parse_previously_unanalysed_sentence==0) {$parse_selected_sentence=0}
    elsif (/^-e(?:=(.*))?$/ && $parse_selected_sentence==0) {
	$parse_previously_unanalysed_sentence=1;
	$file_with_sentenceslist=$1;
	if ($file_with_sentenceslist eq "") {
	    $file_with_sentenceslist=$FAILURES_FILE_NAME;
	}
    }
    elsif (/^-o=(l|i)$/) {$order=1; $order_kind=$1;}
    elsif (/^-d$/) {$die_on_warning=1;}
    elsif (/^-t(?:\=([0-9\.]+))?$/) {if ($1 eq "") {$timeout=5} else {$timeout=$1} if ($stimeout==0) {$stimeout=$timeout}}
    elsif (/^-T(?:\=([0-9\.]+))?$/) {if ($1 eq "") {$stimeout=5} else {$stimeout=$1}}
    elsif (/^-flt(?:\=([v0-9]+)(?:\:([0-9a]+))?)?$/ || /^--filter/) {$filter=1; if ($1 ne "") {$flt_nbest=$1; if ($2 ne "") {$flt_cp=$2}}}
    elsif (/^-std\=([v0-9]+)(?:\:([0-9a]+))?$/) {$std_nbest=$1; if ($2 ne "") {$std_cp=$2}}
    elsif (/^-no_std$/ || /^--no_standard/) {$no_std=1; $robust=1;}
    elsif (/^-r(?:\=([v0-9]+)(?:\:([0-9a]+))?)?$/ || /^--robust(?:\=([0-9\.]+))?/) {$robust=1; if ($1 ne "") {$rob_nbest=$1; if ($2 ne "") {$rob_cp=$2}}}
    elsif (/^-ns$/ || /^--?no_seg(?:mentation)?$/) {$segment=0;}
    elsif (/^-np$/ || /^--?no_parse$/) {$parse=0;}
    elsif (/^-nsus$/ || /^--?no_skip_uw_sentences$/) {$skip_uw_only_sentences=0;}
    elsif (/^-ls$/ || /^--?list_sentences$/) {$listsentences=1; $parse=0;}
    elsif (/^--?cfg$/) {$only_CFG=1;}
    elsif (/^-nao$/ || /^--?no_analysis_output$/) {$no_analysis_output=1;}
    elsif (/^-oinr$/ || /^--?ok_if_no_rcvr$/) {$ok_if_no_rcvr=1;}
    elsif (/^-sod$/ || /^--?save_output_dag$/) {$save_output_dag=1;}
    elsif (/^-l$/ || /^--?log$/) {$log=1;}
    elsif (/^-f$/ || /^--?fail(?:ure)?/) {$failures=1;}
    elsif (/^-stderr$/) {$view_stderr=1}
    elsif (/^-stdout$/) {$view_stdout=1}
    elsif (/^-nw$/ || /^-no_warn(?:ing)?s?$/) {$view_warnings=0}
    elsif (/^-infos?$/) {$view_infos=1}
    elsif (/^-cmd$/) {$view_cmd=1}
    elsif (/^-parser$/) {$view_parser=1}
    elsif (/^-ror$/) {$replace_old_run=1}
    elsif (/^-maxl=(.*)$/) {$max_length=$1}
    elsif (/^-st$/ || /^--segment_on_timeout$/) {$segment_on_timeout=1}
    elsif (/^-sm$/ || /^--save_measures$/) {$save_measures=1}
    elsif (/^-ssb$/ || /^--segment_on_sentbound$/) {$sb_segment=1}
    elsif (/^-mwf=(.*)$/ || /^--min_weights_file=(.*)$/) {$min_weights_file=$1}
    elsif (/^-dd=(.*)$/ || /^--dags_dir=(.*)$/) {$DAGS_DIR=$1}
    elsif (/^-easy$/) {$easy=1}
    elsif (/^-h$/ || /^--help/) {
	print STDERR <<OPTIONS;
Usage:        esxanalyse.pl [options]
Options:
1. Run characteristics
  -rn=[runname] --run_name=[]  specify the name given to the run (default is wday_month_day_time_year)
  -dd=[dir] --dags_dir=[dir]   specify the folder in which are the DAG files to be parsed
  -s=[filename]                parse sentences of file [filename]
  -s=[filename]:[line_number]  parse sentence number [line_number] of file [filename]
  -e                           parse sentences whose ids are in the file of previously failed sentences
  -e=[filename]                parse sentences whose ids are in the specified file
  -l --log                     generate log file
  -f --fail --failure          generate file of failed sentences
  -infos                       show informations about the current parsing process on STDERR
  -cmd                         show the commands used to parse
  -parser                      show the parser invoked
  -ror                         replace previous run instead of renaming it

2. Global parsing strategies
  -o=[ordering_method]         parse sentences in a given order.
                               Possible values: [ordering_method]=\'l\' : by increasing length
                                                [ordering_method]=\'i\' : by decreasing length

3. Sentence-level parsing options

3.a Timeouts
  -t(=[number])                timout on analysis time, 5 sec if not specified, [number] seconds otherwise
  -T(=[number])                timout on segment analysis time, 5 sec if not specified, [number] seconds otherwise
                               (if not specified, this timeout is the same as the global timeout set by -t)

3.b Multi-pass options
  -std=[parseropts]            sets parser options for standard parser
  -flt=[parseropts] --filter   if defined, try the filtering parser before (with options)
  -r=[parseropts] --robust     if defined, call the robust parser if the standard parser failed to give an analysis (with options)
         [parseropts]=                          Default is no nbest filtering
         [parseropts]=[nbest]                   Use [nbest]-best filtering (except if [nbest]=0) with default context type 2
         [parseropts]=[nbest]:[context type]    Use [nbest]-best filtering (except if [nbest]=0) with context type [context type]
  -no_std --no_standard        if defined, do not use standard parser, use robust parser instead (implies -r, compatible with -flt)
  -cfg --cfg                   perform only CFG parsing with the support grammar

3.c Over-segmentation
  -os --over_segmentation      if no global analysis is found, over-segment input sentence and try to parse these segments 
  -ssb --segment_on_sentbound  always split sentences on _SENT_BOUND forms
  -st --segment_on_timeout     if analysis timed out, sent the sentence by segments (over-segmentation)

3.d Other options
  -nsus --no_skip_uw_sentences do parse sentences containing only _Uw and _uw (they are skipped by default)
  -oinr                        a sentence is considered as ok if no CFG recovery was needed
                                   (default = if a consistent main f-structure was found)
  -mwf=[filename]              use minimum weights given in [filename] to filter the CFG parse forest

4. Special behaviours
  -np --no_parse               do not parse
  -ls --list_sentences         do not parse, but print to STDOUT the list of sentences to parse in the computed order
  -nao --no_analysis_output    do not save analyses
  -sod --save_output_dag       save the DAG that is consistent with the parse
  -stderr                      redirect to STDERR error messages of called processes
  -stdout                      redirect to STDERR the raw output of the parser
  -sm                          save sxlfg measures (requires -l)
  -d                           die on first warning, instead of just printing it on STDERR

5. Miscanellous
  -h --help                    print this
OPTIONS
	exit(0);
    }
    else {die ("###### ERROR: Unrecognized or mistyped option (e.g. check if file names have the .dag extension; -a and -e are incompatible): $_\n")}
}

$runname="run_".$runname;

$ANALYSER=~s/(^| )(-m [^ ]*n[^ ]*)( |$)/\1-n $std_nbest -cp $std_cp -f \2\3/;
$ANALYSER=~s/ rules_weights/ $runname\/rules_weights/;
$ANALYSER=~s/ best_weights/ $runname\/best_weights/;
$FLTANALYSER=~s/(^| )(-m [^ ]*n[^ ]*)( |$)/\1-n $flt_nbest -cp $flt_cp -f \2\3/;
$FLTANALYSER=~s/ rules_weights/ $runname\/rules_weights/;
$FLTANALYSER=~s/ best_weights/ $runname\/best_weights/;
$ROBANALYSER=~s/(^| )(-m [^ ]*n[^ ]*)( |$)/\1-n $rob_nbest -cp $rob_cp -f \2\3/;
$ROBANALYSER=~s/ rules_weights/ $runname\/rules_weights/;
$ROBANALYSER=~s/ best_weights/ $runname\/best_weights/;

$RSLT_DIR=$runname."/".$RSLT_DIR;
$EASY_RSLT_DIR=$runname."/".$EASY_RSLT_DIR;
$LOGS_DIR=$runname."/".$LOGS_DIR;
$ODAGS_DIR=$runname."/".$ODAGS_DIR;
if ($parse) {
  if ($replace_old_run) {
    die("Suspicious runname ($runname): I don't want to risk 'rm -rf $runname'") if ($runname eq "" || $runname eq "/" || $runname =~ /\*/);
    system("rm -rf $runname");
  } else {
    my $lastdate;
    if ($lastdate=`ls -ldi $runname 2>/dev/null`) {
	chomp($lastdate);
	if ($lastdate ne "") {
	    $lastdate=~s/^([^ ]+) [^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +(.*) $runname.*$/$1\__$2/;
	    $lastdate=~s/ /_/g;
	    $lastdate=~s/:/-/g;
	    system("mv $runname $runname.$lastdate");
	}
    }
  }
  system("mkdir $runname");
  if (!$no_analysis_output) {
    system("mkdir $RSLT_DIR");
    if ($easy) {
      system("mkdir $EASY_RSLT_DIR");
    }
  }
  system("mkdir $LOGS_DIR");
  if ($save_output_dag) {
    system("mkdir $ODAGS_DIR");
    system("rm -f $ODAGS_DIR/*.odag");
  }
}

# Building list of sentences (DAGs) to be parsed. Options: -s specifyies a unique sentence in a file or a unique file, -e specifies to parse sentences stored in $FAILURES_FILE_NAME
my %lengths_hash=();
my @sentences_refs=();
my $w;
my $nummax; my $nummin;
if ($parse || $listsentences) {
    if ($parse_previously_unanalysed_sentence) {
	open(FAILURES,"< $file_with_sentenceslist") || die ("###### ERROR: -e option called, but impossible to open failures file $file_with_sentenceslist.\n");
	while (<FAILURES>) {
	    chomp;
	    s/^.*\/([^\/]+)$/\1/;
	    s/\s+$//;
	    $snum=0;
	    if (s/\.dag(?::([0-9]+))?//) {$snum=$1;} else {die("Problematic line in sentences list file: $_\n");}
	    $files{$_}{$snum}=1;
	}
	close(FAILURES);
    } elsif ($parse_selected_sentence) {
	$_=$file;
	s/^.*\/([^\/]+)$/\1/;
	if (/\*/) {
	    for (split(/\n/,qx/ls $DAGS_DIR\/$file/)) {
		s/^.*\/([^\/]+)$/\1/;
		s/\.dag//;
		$files{$_}{0}=1;
	    }
	} else {
	    $snum=0;
	    if (s/\.dag:([0-9]+)$//) {
		$snum=$1;
		$files{$_}{$snum}=1;
	    } elsif (s/\.dag:([0-9]+)\-([0-9]+)$//) {
		for $snum ($1..$2) {
		    $files{$_}{$snum}=1;
		}
	    } elsif (s/\.dag$//) {
		$files{$_}{0}=1;
	    } else {die("Problematic unique sentence id: $_\n");}
	}
    } else {
	for (split(/\n/,qx/ls $DAGS_DIR\/*.dag/)) {
	    s/^.*\/([^\/]+)$/\1/;
	    s/\.dag//;
	    $files{$_}{0}=1;
	}
    }
    
# creating dispatch file (infos about run)
    if ($parse) {
	open(DISPATCH,"> $runname/dispatch.conf") || die ("###### ERROR: could not open $runname/dispatch.conf\n");
	print DISPATCH "results logs\nparse_options =$parse_options\nhost ".(`uname -n`)."\n";
	for (keys %files) {print DISPATCH "corpus $_\n";}
	close(DISPATCH);
    }
    
# parsing min_weights_file
    if ($parse && $min_weights_file ne "") {
	open(MWF,"< $min_weights_file") || die ("###### ERROR: could not open $min_weights_file\n");
	while (<MWF>) {
	    chomp;
	    /^\#([0-9]+) [0-9]+ ([0-9]+) ([^ ]+)$/ || die "Bad input format in min weights file $min_weights_file: $_\n";
	    $min_weights[$1][$2]=$3;
	}
	close(MWF);
	open(BWDATA,"> $runname/best_weights_data") || die ("###### ERROR: could not open $runname/best_weights_data\n");
    }
    
# Parse DAGs files
    my $skip=0;
    my $count=0;
    my $length;
    for my $corpus (sort keys %files) {
	open (IN, "< $DAGS_DIR/$corpus.dag") || die ("###### ERROR: Input file $DAGS_DIR/$corpus.dag could not be opened for reading. It may not exist.\n");
	if ($view_warnings) {print STDERR "Loading corpus $corpus.dag             \r";}
	while (<IN>) {
	    chomp;
	    next if (/^\/\//);
	    next if (/_XML\s*$/);
	    $count++;
	    if ($skip_uw_only_sentences && $_!~/\} [^_]/ && $_!~/\} _[^Uu]/ && $_!~/\} _[Uu][^w]/) {
#		print STDERR "Skipping $_\n";
		$skip++;
		if ($view_warnings) {print STDERR "   Skipped $skip sentences out of $count\r";}
		next;
	    }
	    /^.*?E([0-9]+)F/;
	    $nummin=$1;
	    /^.*E([0-9]+)F/;
	    $nummax=$1;
	    s/$/ /;
	    for $num ($nummin..$nummax) {
		if (defined($files{$corpus}{0}) || defined($files{$corpus}{$num})) {
		    chomp;
		    push(@sentences_refs,$corpus.".dag:".$num);
		    s/ +/ /g;
		    s/^ //;
		    s/(\{[^\}]+\} +$epsilon(?:__epsilon)?)([ \(\)\|]|$)/$1?$2/g; # on rend les _EPSILON facultatifs
		    s/ $//;
		    $length=scalar(split(/\{/,$_));
		    if ($max_length == 0 || $max_length >= $length) {
		      if ($order_kind ne "") {
			$lengths_hash{$length}{$corpus.".dag:".$num}=$_;
		      }
		      $dags_hash{$corpus.".dag:".$num}=$_;
		    } else {
		      $dags_hash{$corpus.".dag:".$num}="___REJECTED___";
		    }
		    last;
		}
	    }
	}
	close(IN);
    }
    if ($view_warnings) {print STDERR "Loaded $count sentences                                          \n";}
# Build the strategy for the analysing process (-o option). -o=l sorts by sentence length.
    if ($order_kind eq "l") {
	@sentences_refs=();
	for my $key (sort {$a <=> $b}  keys %lengths_hash) {
	    for (sort keys %{$lengths_hash{$key}}) {
		push(@sentences_refs,$_);
	    }
	}
    } elsif ($order_kind eq "i") {
	@sentences_refs=();
	for my $key (sort {$b <=> $a}  keys %lengths_hash) {
	    for (sort keys %{$lengths_hash{$key}}) {
		push(@sentences_refs,$_);
	    }
	}
    }
}
%lengths_hash=();

my $analyse_cmd="";
my @analyse_cmd=();
my $anOK = 0;
my @segments_number=();
my $temp;
my ($sr, %corpora, %snums, %lsnums, %ntransitions, %slength);
my $sentence;
my $sentence_ref;
my $corpus="";

# no-parsing option
if ($listsentences) {
    for (@sentences_refs) {
	if ($dags_hash{$_} ne "___REJECTED___") {
	  print "$_\t".split(/\{/,$dags_hash{$_})."\n";
	}
    }
}
if ($parse) {
    if ($log) {
	for (keys %files) {
	    s/\.dag(?::[0-9]+)?//;
	    $logfilehandle{$_} = IO::File->new("> $LOGS_DIR/$_.log") || die ("###### ERROR: Log file $LOGS_DIR/$_.log could not be opened.\n");
	}
	%files=();
	open(GLOBALLOG,"> $runname/global_log") || die ("###### ERROR: Global log file $runname/global_log could not be opened.\n");
	if ($only_CFG) {&log("### CFG ONLY ###\n")};
    }
# Parse and build intermediate files (xml results for individual sentences)
    if ($failures) {
	open (FAILURES, "> $runname/$FAILURES_FILE_NAME") || die ("###### ERROR: Log file $LOG_FILE_NAME could not be opened in $runname.\n");
    }
    if ($view_warnings) {print STDERR "Preparing parsing                             \n";}
    my $count=0;
    for my $sref (@sentences_refs) {
	next if ($dags_hash{$sref} eq "___REJECTED___");
        $count++;
	$sr=$sref;
	$sr=~s/^.*\/([^\/]+)$/\1/;
	$sr=~s/^(.*)\.dag(?::([0-9]+))?// || die "###### ERROR: Could not understand sentence name $sr\n";
	$corpora{$sref}=$1;
	$snums{$sref}=$2;
	$corpora{$sref}=~s/^.*\/([^\/]*)$/$1/;
	$lsnums{$sref}="0000000".$snums{$sref};
	$lsnums{$sref}=~s/^.*(........)$/\1/;
	$dags_hash{$sref}=~/<F id=\"E[0-9]+F([0-9]+)\"[^\}]+\}[^\}]+$/;
	$slength{$sref}=$1;
	$temp=$dags_hash{$sref};
	$ntransitions{$sref}=0;
	while ($temp=~s/\{[^\}]*\}//) {$ntransitions{$sref}++;}
	$ntransitions{$sref};
	if ($view_warnings && $count % 1000 == 0) {print STDERR "$count\r";}
    }
    if ($view_warnings) {print STDERR "Starting parsing\n";}
    $count=0;
    for my $sref (@sentences_refs) {
	next if ($dags_hash{$sref} eq "___REJECTED___");
	$sentence=$dags_hash{$sref};
	$corpus=$corpora{$sref};
	$lsnum=$lsnums{$sref};
	$snum=$snums{$sref};
	$sentence_ref=$sref;
#	chomp;
	$sentence_easyID=0;
	@last_group_id=();
	@last_relation_id=();
	%result=();
	&parse($sentence,$timeout);
#	&push_partial_result("0");
        $count++;
	if ($warning_string ne "") {
	    &log("\tparsing $warning_string\n");
	}
	if (!$no_analysis_output && !$only_CFG) {
	    if ($segment && !$anOK && (!$timed_out || ($timed_out && $segment_on_timeout))) { # on va tenter l'analyse par morceaux
		$_=$sentence;
		&log("\tparsing $failure_string - trying segmented parsing ");
# 		print OUT "<!-- ********************************************************************\n";
# 		print OUT "        The file \"$corpus\__$lsnum\" contains the XML format of the input sentence \#$lsnum\n";
# #		    print OUT "        \"$_\"\n";
# 		print OUT "        which has been generated by the concatenation of\n";
# 		print OUT "        the parsing by the SYNTAX(*) LFG processor SXLFG(*)\n";
# 		print OUT "        of successive partial segments \n";
# 		print OUT "     ********************************************************************\n";
# 		print OUT "        (*) SYNTAX and SXLFG are trademarks of INRIA.\n";
# 		print OUT "     ******************************************************************** -->\n";
# 		print OUT "\n";
		%result=();
		$sentence_easyID=0;
		@last_group_id=();
		@last_relation_id=();
		$segments_number[1]=scalar split(/\{[^\}]+\} +_SENT_BOUND/,$_);
		# tentative 1 : segmentation sur les _SENT_BOUND
		for (split(/\{[^\}]+\} +_SENT_BOUND/,$_)) { 
		    if ($_!~/^ *$/) {
			$anOK=0;
			if ($segments_number[1] > 1) {
			    &log("\t\t<S1>\t$_ \n");
			    &parse($_,$stimeout);
#			    &push_partial_result("1");
			}
			if (!$anOK) {
			    s/(\{[^\}]+\}) +([;\-:\!\?\'\�]+|_UNDERSCORE) +/\1 \2 \1 _SENT_BOUND /g;
			    s/(\{[^\}]+\}) +(\") +(?:\| *\[ *\{[^\}]+\} *$epsilon *\] *\) *)?(?=[^\| ])/\1 \2 \3 \1 _SENT_BOUND /g;
			    $segments_number[2]=scalar split(/\{[^\}]+\} +_SENT_BOUND/,$_);
			    # tentative 2 : segmentation sur les ponctuations fortes
			    for (split(/\{[^\}]+\} +_SENT_BOUND/,$_)) { 
				if ($_!~/^ *$/) {
				    $anOK=0;
				    if ($segments_number[2] > 1) {
					&log("\t\t<S2>\t$_ \n");
					&parse($_,$stimeout);
#					&push_partial_result("2");
				    }
				    if (!$anOK) {
					s/(\{[^\}]+\}) +(,)/\1 \2 \1 _SENT_BOUND/g;
					$segments_number[3]=scalar split(/\{[^\}]+\} +_SENT_BOUND/,$_);
					# tentative 3 : segmentation sur les virgules
					for (split(/\{[^\}]+\} +_SENT_BOUND/,$_)) { 
					    if ($_!~/^ *$/) {
						$anOK=0;
						if ($segments_number[3] > 1) {
						    &log("\t\t<S3>\t$_ \n");
						    &parse($_,$stimeout);
#						    &push_partial_result("3");
						}
						if (!$anOK) {
						    s/(\{[^\}]+\}) +(et|ou)/\1 _SENT_BOUND \1 \2 \1 _SENT_BOUND/g;
						    $segments_number[5]=scalar split(/\{[^\}]+\} +_SENT_BOUND/,$_);
						    # tentative 3b : segmentation sur les "et" et les "ou"
						    for (split(/\{[^\}]+\} +_SENT_BOUND/,$_)) { 
							if ($_!~/^ *$/) {
							    $anOK=0;
							    if ($segments_number[5] > 1) {
								&log("\t\t<S3b>\t$_ \n");
								&parse($_,$stimeout);
#								&push_partial_result("3b");
							    }
							    if (!$anOK) {
								s/(\{[^\}]+\}) +([^ ]+)/\1 \2 \1 _SENT_BOUND/g;
								#suppression des ambigu�t�s dans le dag
								s/\\\(/_PARENTH_O/g;
								s/\\\)/_PARENTH_F/g;
								s/\\\|/_PIPE/g;
								while (s/\(([^\(\)]*)\|[^\(\)\|]*\)/\(\1\)/g || s/\(([^\(\|\)]*)\)/\1/g) {}
								s/  +/ /g;
								s/_PARENTH_O/\\\(/g;
								s/_PARENTH_F/\\\)/g;
								s/_PIPE/\\\|/g;
								# tentative 4 : segmentation par mots
								for (split(/\{[^\}]+\} +_SENT_BOUND/,$_)) { 
								    if ($_!~/^ *$/) {
									$anOK=0;
									&log("\t\t<S4>\t$_ \n");
									&parse($_,$stimeout);
#									&push_partial_result("4");
									if (!$anOK) {
									    $failure_id="S-".$failure_id;
									    $failure_string.=" (despite trying segmentation)";
									} else {
									    &log("\tsegmented parsing (level 4) succeded\n");
									    print STDERR "\t(S4)";
									}
									close(ANALYSE);
								    }
								}
							    } else {
								&log("\tsegmented parsing (level 3.1) succeded\n");
								print STDERR "\t(S3.1)";
							    }
							}
						    }
						} else {
						    &log("\tsegmented parsing (level 3) succeded\n");
						    print STDERR "\t(S3)";
						}
					    }
					}
				    } else {
					&log("\tsegmented parsing (level 2) succeded\n");
					print STDERR "\t(S2)";
				    }
				}
			    }
			} else {
			    &log("\tsegmented parsing (level 1) succeded\n");
			    print STDERR "\t(S1)";
			}
		    }
		}
		if (!$anOK) {
		    &announce_failure(0,$failure_id,$corpus,$snum,$failure_string); 
		}
		if ($view_infos) {
		    print STDERR "\n";
		}
	    }
	}
#	for my $i (sort keys %{$result{ids}}) {
#	  for (@{$result{data}[$i]}) {print OUT $_}
#	}
#	close(OUT);
    }
    if ($failures) {
	close (FAILURES);
    }
    if ($log) {
	close (GLOBALLOG);
    }
}

sub warn {
    my $w=shift;
    if ($die_on_warning) {$w=~s/^(......) WARNING/\1 ERROR/; die($w)}
    else {print STDERR $w;}
}

sub clog {
    my $s=shift;
    if ($log) {
	if (defined($logfilehandle{$corpus})) {
	    my $handle=$logfilehandle{$corpus};
	    print $handle $s;
	} else {
	    die ("###### ERROR: could not find filehandle for the logfile of corpus \"$corpus\" (known corpus: ".join(",",keys %logfilehandle).")\n");
	}
    }
}

sub log {
    my $s=shift;
    if ($log) {
	print GLOBALLOG $s;
    }
}

sub announce_failure {
    my $nl=shift;
    my $k=shift;
    my $f=shift;
    my $n=shift;
    my $s=shift;
    if ($nl) {print STDERR "\n";}
    if ($view_warnings) {
	&warn("###### WARNING ($k): Analysing process $s on file $f.dag, sentence $n\n");
    }
    &log("        !!! parsing $s !!!\n");
    if ($failures) {
	print FAILURES "$DAGS_DIR/$f.dag:$n\n";
    }
}

sub parse {
    my $sentence=shift;
    my $to=shift;
    my $parsing_mode;
    my $in;
    $sentence=~/^[^\{]*\{<F id=\"E([0-9]+)F/;
    my $easynum=$1;
    if ($easynum eq "") {
	$easynum="0"; print STDERR "Impossible d'extraire le easynum de:\n$sentence\n";
    }
    my $sentid="$corpus\__$lsnum";
    #    $sentence=~s/\{[^\}]*\}//g;
    $did_log=0;
    if ($filter) {
	$parsing_mode=1;
    } elsif ($no_std) {
	$parsing_mode=3;
    } else {
	$parsing_mode=2;
    }
    if ($sb_segment && $sentence=~/_SENT_BOUND/) {
	my $segnum=1;
	for my $subsentence (split(/\{[^\}]+\} +_SENT_BOUND/,$sentence)) { 
	    &log("parsing sentence $snum-".$segnum++." of file $corpus : $subsentence\n");
	    if ($view_infos) {
		print STDERR "$corpus\__$lsnum\t\t$sentence\n";
	    } else {
		print STDERR "\r$corpus\__$lsnum                                \r";
	    }
	    if ($subsentence!~/^ *$/) {
 		$in=$subsentence;
 		$subsentence=~s/  +/ /g;
 		$subsentence=~s/^ //;
 		$subsentence=~s/(?<=[^\\ ])([\(\)\|])/ $1/g;
 		$subsentence=~s/(?<=[^\\])([\(\)\|])(?=[^ ])/$1 /g;
 		$in=~s/\\/\\\\/g;
 		$in=~s/\"/\\\"/g;
 		$in=~s/\`/\\\`/g;
 		&do_parse($subsentence,$in,$easynum,$sentid,$to,$parsing_mode);
# 		&push_partial_result("1");
	    }
	}
    } else {
	&log("parsing sentence $snum of file $corpus : $sentence\n");
	if ($view_infos) {
	    print STDERR "$corpus\__$lsnum\t\t$sentence\n";
	} else {
	    print STDERR "\r$corpus\__$lsnum                                \r";
	}
	$in=$sentence;
	$sentence=~s/  +/ /g;
	$sentence=~s/^ //;
	#    $sentence=~s/ /_/g;
	$sentence=~s/(?<=[^\\ ])([\(\)\|])/ $1/g;
	$sentence=~s/(?<=[^\\])([\(\)\|])(?=[^ ])/$1 /g;
	$in=~s/\\/\\\\/g;
	$in=~s/\"/\\\"/g;
	$in=~s/\`/\\\`/g;
	&do_parse($sentence,$in,$easynum,$sentid,$to,$parsing_mode);
#	&push_partial_result("0");
    }
}

sub do_parse {
  my $sentence=shift;
  my $in=shift;
  my $easynum=shift;
  my $sentid=shift;
  my $to=shift;
  my $parsing_mode=shift;
  my $compact_infos;
  my $logstring="";
  my $forced_logstring="";
  my $outputfile;
  my $dagoutputfile;
  my $cp;
  if ($parsing_mode==3) {
    print STDERR "$sentid\t        rob \r";
    $logstring.="\trobust mode\n";
    $compact_infos.="r"
  } elsif ($parsing_mode==2) {
    print STDERR "$sentid\t    std    \r";
    $logstring.="\tstandard mode\n";
    $compact_infos.="s"
  } elsif ($parsing_mode==1) {
    print STDERR "$sentid\tflt\r";
    $logstring.="\tfilter mode\n";
    $compact_infos.="f"
  }
  $warning_string= "";
  $timed_out=0;
  $failure_string="exited abnormally";
  $failure_id="?";
  my $CFGflag=1;
  #    system("echo \"\" > temp_file_stderr");
  $analyse_cmd="";
  $analyse_cmd="echo \"$in\" |";
  if ($only_CFG) {
    $analyse_cmd.="($CFGANALYSER";
  } elsif ($parsing_mode==3) {
    $analyse_cmd.="($ROBANALYSER";
    $cp = $rob_cp;
  } elsif ($parsing_mode==1) {
    $analyse_cmd.="($FLTANALYSER";
    $cp = $flt_cp;
  } else {
    $analyse_cmd.="($ANALYSER";
    $cp = $std_cp;
  }
  if ($save_output_dag) {
    $analyse_cmd.=" -pod";
  }
  if ($min_weights_file) {
    if (defined($min_weights[$easynum][$cp])) {
      $analyse_cmd.=" -mw $min_weights[$easynum][$cp]";
    } else {
      return 1;
    }
  }
  $analyse_cmd.=" -vto $to)";
  if ($easy) {
    $analyse_cmd.=" 2>/dev/null";
  } else {
    $analyse_cmd.=" 2>&1";
  }
  if ($no_analysis_output) {
    $analyse_cmd .= " > /dev/null";
  } else {
    if ($easy) {
      $analyse_cmd .= " | tee -a $RSLT_DIR/$corpus.xml | ../sxlfg-core/bin/lfg2easy-2.pl >> $EASY_RSLT_DIR/$corpus.xml";
    } else {
      $analyse_cmd .= " | tee -a $RSLT_DIR/$corpus.xml";
    }
  }
  
  if ($std_nbest eq "v") {
    my $local_nbest=10**(2+$ntransitions{$sentence_ref}/10);
    $local_nbest=~s/\..*//;
    if ($local_nbest>1000000) {
      $local_nbest=20000;
    }
    if ($local_nbest>20000) {
      $local_nbest=0;
    }
    $analyse_cmd=~s/(^| )-n $std_nbest( |$)/\1-n $local_nbest\2/;
    #  print STDERR "\t$ntransitions{$sentence_ref}\t$analyse_cmd\n";
  }
  
  if ($view_cmd) {
    print STDERR "$analyse_cmd\n";
  }
  if ($view_parser) {
    my $parser = $analyse_cmd;
    $parser =~ s/^.*\" \|\(/(/;
    print STDERR "$parser\n";
  }
  my @stderror=`$analyse_cmd`;
  my $infos="- ";
  my $step=0;
  my $localtimeout=0;
  my $cfgdone=0;
  my $sentence_ok;
  if ($ok_if_no_rcvr) {$sentence_ok=1} else {$sentence_ok=0};
  my $parsing_time=0;
  my $outputdag="";
  my $compact_infos="";
  for (@stderror) {
    if ($view_stderr) {
      print STDERR "$_";
    }
    chomp;
    if ($CFGflag && /Earley (trivial )?recovery/) {
      $warning_string= "needed CFG error recovery";
      $CFGflag=0;
      $infos.="CFG recovery - ";
      $compact_infos.="R";
      if ($ok_if_no_rcvr) {$sentence_ok=0}
    } elsif (/^[0-9\.]+$/ && !$localtimeout) {
      $forced_logstring.="\tfailed \tparsing attempt time (s) :\t$_\n";
      $logstring.="\tparsing time (s) :\t$_\n";
      $parsing_time=$_;
      $infos.="time = $_ - ";
    } elsif (/user[ \t]*([0-9]+)m([0-9\.]+)s/ && !$localtimeout) {
      $_=60*$1+$2;
      $forced_logstring.="\tfailed \tparsing attempt time (s) :\t$_\n";
      $logstring.="\tparsing time (s) :\t$_\n";
      $parsing_time=$_;
      $infos.="time = $_ - ";
    } elsif (/\(\|N\|=([0-9e\.\+]+), \|T\|=([0-9e\.\+]+), \|P\|=([0-9e\.\+]+), S=[0-9e\.\+]+, \|outpuG\|=([0-9e\.\+]+), TreeCount=([0-9e\.\+]+|inf)\)$/) {
      $logstring.="\tCFG data (N T P outpuG TreeCount) :\t$1\t$2\t$3\t$4\t$5\n";
      $infos.="#trees = $5 - ";
      $cfgdone=1;
    } elsif (/([1-9][0-9]* consistent f_structure)/) {
      $step++;
      $logstring.="\t$1 (step $step)\n";
      $infos.="$1 (step $step) - ";
      if (!$ok_if_no_rcvr) {$sentence_ok=1}
      if ($step==$step_max) {
	$compact_infos.="C";
      }
    } elsif (/([1-9][0-9]* unconsistent f_structure)/) {
      $step++;
      $logstring.="\t$1 (step $step)\n";
      $infos.="$1 (step $step) - ";
      if (!$ok_if_no_rcvr) {$sentence_ok=0}
      if ($step==$step_max) {
	$compact_infos.="U";
      }
    } elsif (/ ([0-9]+) f_structure/) {
      $step++;
      $logstring.="\t$1 unconsistent f_structures (step $step)\n";
      $infos.="$1 unconsistent f_structures (step $step) - ";
      if (!$ok_if_no_rcvr) {$sentence_ok=0}
      if ($step==$step_max) {
	$compact_infos.="U";
      }
    } elsif (/The first attempt failed/) {
      $logstring.="\tno first attempt f_structure (step ".($step+1).")\n";
      $infos.="1st att. failed (step ".($step+1).") - ";
      if (!$ok_if_no_rcvr) {$sentence_ok=0}
      if ($step==$step_max) {
	$compact_infos.="G";
      }
    } elsif (/ ([0-9]+ partial f_structures?)/) {
      $step++;
      $logstring.="\t$1 (step $step)\n";    
      $infos.="$1 (step $step) - ";
      if (!$ok_if_no_rcvr) {$sentence_ok=0}
      $compact_infos="P";
    } elsif (/^\s*MEASURES/ && $save_measures) {
      $logstring.="\t$_\n";    
    } elsif (/Cannot open \(create\) (.*)/) {
      $logstring.="\tSXLFG CAUGHT ERROR : \"$_\"\n";    
      $infos.="\t### SXLFG CAUGHT ERROR : \"$_\"\n\t- ";
      $failure_id="OP ";
      $failure_string="failed (sxlfg detected an error: open/create xml file)";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (/Command exited with non-zero status 4/) {
      $forced_logstring.="\failed tparsing attempt timed out\n";    
      $logstring.="\tparsing timed out\n";    
      $infos.="timeout - ";
      $localtimeout=1;
      $failure_id="TO ";
      $failure_string="timed out";
      $timed_out=1;
      $sentence_ok=0;
      $parsing_time=$timeout." **timeout**";
      $compact_infos.="T";
    } elsif (/Command exited with non-zero status 2/ && !$cfgdone) {
      $logstring.="\tparsing failed (probably while processing CFG error recovery)\n";    
      $failure_id="CFG";
      $infos.="CFG recovery - CFG failure - ";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (/Command exited with non-zero status 1/) {
    } elsif (/Unknown word/) {
      $logstring.="\tSXLFG CAUGHT ERROR : \"unknown word: $_\"\n";    
      $infos.="\n\t### SXLFG CAUGHT ERROR : \"unknown word: $_\"\n\t- ";
      $failure_id="UW ";
      $failure_string="failed (sxlfg detected a lexical error: unknown word)";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (s/^.*(Command terminated by signal 6)/\1/) {
      $logstring.="\tSXLFG UNCAUGHT ERROR : \"$_\" (glibc error?)\n";
      $infos.="\n\t### SXLFG UNCAUGHT ERROR : \"$_\" (glibc error?)\n\t- ";
      $sentence_ok=0;
      $failure_id="UCE";
      $failure_string="failed (sxlfg undetected error)";
      $compact_infos.="E";
    } elsif (/Command exited with non-zero status 6/) {
      $logstring.="\tparsing failed (bad options)\n";    
      $failure_id="BO ";
      $failure_string="failed (bad options)";
      $infos.="Bad options - ";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (/Internal system ERROR in/) {
      $logstring.="\tSXLFG CAUGHT ERROR : \"sxtrap: $_\"\n";    
      $infos.="\n\t### SXLFG CAUGHT ERROR : \"sxtrap: $_\"\n\t- ";
      $failure_id="CE ";
      $failure_string="failed (sxlfg detected an error: sxtrap)";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (/Command exited with non-zero status 3/) {
      $logstring.="\tparsing failed (memory overflow)\n";
      $failure_id="MOF";
      $failure_string="failed (memory overflow)";
      $infos.="Memory overflow - ";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (s/^.*(Command exited with non-zero status ([0-9]+))/\1/ && $2 ne "5" && $2 ne "2" && $2 ne "1" && $2 ne "3") {
      $logstring.="\tSXLFG CAUGHT ERROR : \"recovery: $_\"\n";
      $infos.="\n\t### SXLFG CAUGHT ERROR : \"recovery: $_\"\n\t- ";
      $failure_id="REC";
      $failure_string="failed (sxlfg detected an error: recovery)";
      $sentence_ok=0;
      $compact_infos.="E";
    } elsif (s/^.*(Command terminated by signal [0-9]+)/\1/) {
      $logstring.="\tSXLFG UNCAUGHT ERROR : \"$_\"\n";
      $infos.="\n\t### SXLFG UNCAUGHT ERROR : \"$_\"\n\t- ";
      $sentence_ok=0;
      $failure_id="UCE";
      $failure_string="failed (sxlfg undetected error)";
      $compact_infos.="E";
    } elsif ($save_output_dag && s/^\toutputdag: //) {
      $outputdag=$_." // $compact_infos";
    } elsif ($min_weights_file ne "" && s/^best_weight = (.*), exit at step (.*) with weight = (.*)//) {
      print BWDATA "$easynum\t$1\t$slength{$sentence_ref}\t$ntransitions{$sentence_ref}\t$2\t$3\n";
    }
  }
  if ($view_infos) {
    print STDERR "\t$infos";
  }
  #### cacher �a sous une option!!!
  if (!$timed_out) {
    open(BW,">>$runname/best_weights") || die ("c'est la cata\n");
    print BW "$compact_infos\n";
    close(BW);
  }
  if ($timed_out || $sentence_ok || ($parsing_mode==2 && !$robust) || $parsing_mode==3) {
    if ($save_output_dag) {
      $dagoutputfile=$ODAGS_DIR."/".$corpus.".odag";
      open (ODAGS,">>$dagoutputfile") || die "could not open $dagoutputfile\n";
      print ODAGS $outputdag."\n";
      close(ODAGS);
    }
    $infos=~s/^- \n\t//;
    $infos.="\n";
    if ($sentence_ok) {
      &clog("ok",$corpus);
    } else {
      &clog("***********************************\nfail",$corpus);
    }
    &clog(" $snum\t$sentence\n",$corpus);
    if (!$sentence_ok) {
      &clog("***********************************\n",$corpus);
    }
    &clog(" <time> $parsing_time\n");
    &clog(" <length> $slength{$sentence_ref}\n");
    &clog(" <transitions> $ntransitions{$sentence_ref}\n");
    if (!$timed_out || !$did_log) {
      &log("$logstring");
    } else {
      &log("$forced_logstring");
    }
  } elsif ($parsing_mode==1 && !$no_std) {
    if (!$timed_out) {
      $did_log=1;
      &log("$logstring");
    } else {
      &log("$forced_logstring");
    }
    &do_parse($sentence,$in,$easynum,$sentid,$to,2);
  } elsif ($robust) {
    if (!$timed_out) {
      $did_log=1;
      &log("$logstring");
    } else {
      &log("$forced_logstring");
    }
    &do_parse($sentence,$in,$easynum,$sentid,$to,3);
  }
  return 1;
}

# sub push_partial_result {
#   return if ($no_analysis_output);
#   my $parselevel=shift;
#   my $groups_number=0;
#   my $relations_number=0;
#   my $is_in_group=0;
#   my ($new_group_number, $new_sentence_easyID);
#   my %raw_gid2gid;
#   if ($warning_string ne "" && $parselevel>0) {
#     &log("\tparsing (level $parselevel) $warning_string\n");
#   }
#   open(ANALYSE,"<$ANLS_DIR/temp______.xml") || die ("///////////////////// INTERNAL ERROR /////////////////////\nImpossible to open $ANLS_DIR/temp______.xml, that should have been created a few milliseconds earlier: $!");
#   my $state=-1;
#   for my $l (<ANALYSE>) {
#     if ($l=~/^ *<E sid=\"E(.*?)\"/) {
#       $sentence_easyID=$1;
#       $result{ids}{$sentence_easyID}++;
#     }
#     push(@{$result{data}[$sentence_easyID]},$l);
#   }
#   system("rm -f $ANLS_DIR/temp______.xml");
# }
