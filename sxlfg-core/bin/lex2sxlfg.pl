#!/usr/bin/perl
# $Id$

#$lefffdir="/usr/local/share/lefff";

$format="sxlfg-fr";
$lexfile="";
$tplfile="";
$vblfile="";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif ($_ eq "-polgram") {$format="polgram";}
    elsif ($_ eq "-sxlfg-fr") {$format="sxlfg-fr";}
    elsif ($_=~/^-lex=(.*)$/) {$lexfile=$1;}
    elsif ($_=~/^-tpl=(.*)$/) {$tplfile=$1;}
    elsif ($_=~/^-vbl=(.*)$/) {$vblfile=$1;}
    elsif ($_ !~ /^-/) {$lexdir=$_;}
}

if ($tplfile eq "") {$tplfile = "spec/$format.tpl"}
if ($vblfile eq "") {$vblfile = "spec/$format.vbl"}

print "#include $vblfile\n";
print "#include $tplfile\n";

# print "AMALGAM\n";
# open(FILE,"<$lexdir/amlgm") || die("$lexdir/amlgm not found\n");
# while (<FILE>) {
#     chomp;
#     s/^([^#].*)$/\1;/;
#     s/^([^#])/\t\1/;
#     s/([^\.]\.)\.(\t|$)/$1$2/g;
#     print "$_\n";
# }

# print "COMPOUND\n";
# open(FILE,"<$lexdir/cmpnd") || die("$lexdir/amlgm not found\n");
# while (<FILE>) {
#     chomp;
#     s/^([^#].*)$/\1;/;
#     s/^([^#])/\t\1/;
#     s/([^\.]\.)\.(\t|$)/$1$2/g;
#     print "$_\n";
# }


my %macros;
if ($tplfile ne "") {
  open(FILE,"<$tplfile") || die("$tplfile not found\n");
  while (<FILE>) {
    chomp;
    if (/^\s*\@([^ ]+)\s*=\s*\[\s*([^;]*)\s*\]/) {
      $macros{$1}=$2;
    }
  }
  my $didsomething=1;
  while ($didsomething) {
    $didsomething=0;
    for (keys %macros) {
      if ($macros{$_} =~ s/\@([^ \],]+)/$macros{$1}/g) {$didsomething=1}
    }
  }
}

my @lexfiles;
if ($lexfile eq "") {@lexfiles=`ls $lexdir/*.lex`}
else {@lexfiles = ($lexdir."/".$lexfile)}

for $file (@lexfiles) {
  chomp($file);
  open(FILE,"<$file") || die("$file could not be opened\n");
  while (<FILE>) {
    if ($l % 1000 == 0) {
      print STDERR "\tParsing lex files: $l \[$file\]                 \r";
    }
    $l++;
    chomp;
    
    if ($format eq "polgram") {next if /cat = interp/}
    next if (/^\s*$/);

    s/^([^\t]+\t[^\t]*\t[^\t]*\t[^\t]*).*$/\1/ || next;

    s/[^\"]\#.*$//;
    s/^\#.*$//;
    s/\@e *,//;
    s/,* *\@e *\]/\]/;
    s/\\//;
    s/^ +//;
    s/ +\'/\'/g;
#    s/ *\- */\-/g;
    s/ +/ /g;

    s/([^\.]\.)\.\t/$1\t/;

    #hack anti-bug
    s/synt_head=,//g;
    s/,synt_head=\]/\]/g;

    $cadre="";
    if (/^.*pred='[^<]*([^>]*>).*$/) {
      $cadre = $1;
      $cadre =~ s/[\(\)]//g;
    }

    while (s/(pred=.*<[^>]*)(Suj|Objde|Obj�|Obj|Att|Obl2|Obl|Loc|Dloc):(\(?)([^,>\)]+)(\)?)(.*)\]/\1\3\2\5\6, \2 =? \[real = \4\] \]/) {}
    # 1=prefixe , 2=fonction , 3=parentO , 4=reals , 5=parentF , 6=suffixe
    
    s/(pred=.*<[^>]*>)[^']+/\1/;

    # expansion des macros
    while (/\@([^\],\+\s]+)/g) {
      if (!defined($macros{$1})) {
	die "\n\t### ERROR : macro $1 undefined\n";
      }
    }
    s/\@([^\],\+\s]+)/$macros{$1}/g;
    # "unification" des diff�rentes infos de type FonctionSyntaxique = [...]([0-9]+)
    $parenth=qr/(?:[^\[\]]+(?:\[(?:[^\[\]]*(?:\[[^\[\]]*\][^\[\]]*)*)\][^\[\]]*)*)/;
    while (s/(Suj|Objde|Obj�|Obj|Att|Obl2|Obl|Loc|Dloc)\s*(=\??)\s*\[($parenth)\]($parenth)\1\s*(=\??)\s*\[($parenth)\]/\1 \2\5 \[\3, \6\]\4/g
	   || s/(Suj|Objde|Obj�|Obj|Att|Obl2|Obl|Loc|Dloc)\s*(=\??)\s*\[($parenth)\][0-9]*($parenth)\1\s*(=\??)\s*\[\]([0-9]+)/\1 \2\5 \[\3\]\6\4/g
	   || s/(Suj|Objde|Obj�|Obj|Att|Obl2|Obl|Loc|Dloc)\s*(=\??)\s*\[\]([0-9]+)($parenth)\1\s*(=\??)\s*\[($parenth)\][0-9]*/\1 \2\5 \[\6\]\3\4/g) {
      s/=\?=\?/=\?/g; # =? union =? donne =?
      s/=\??=\??/=/g; # les 3 autres cas donnent =
    }
    s/(,\s*)+\]/\]/;
    s/,(\s*,)+/,/g;

    if ($_!~/^[ \t]*$/) {
      /^([^\t]+)\t([0-9]*)\t(.*)$/;
      $c1=$1;
      $cw=$2;
      $c2=$3;
      $c1=~s/ /_/g;
      $c1=~s/\"/\"\\\"\"/g;
      $c1=~s/^(.*[\.\&\|\!\?\(\);\:,\*\[\]\%\#\$$\=\+\>\<\/�����\�\�����\&\@\+�\~].*)$/\"\1\"/ ||
	$c1=~s/^([0-9].*)$/\"\1\"/;
      # poids
      $n=0;
      while ($c1=~/[^\"]_/g) {
	$n++;
      }
      $c1=$c1."\t";
      if ($cw<100 && $cw > 0) {
	$c1.=$cw;
      }			 # poids n�gatif venant du lexique: on oublie l'apport de la multi-mot-itude
      else {			# poids nul ou positif venant du lexique
	if ($n>=1 || $cw > 100) {
	  $c1.=(100+200*$n+$cw);
	}
      }
      $c2=~s/==/=c/g;
      # deal_with_quotes
      $c2=~s/pred *= *\'([^\[]*)\'((?: *,[^>]*)? *\])([^\]]*)$/pred=\"\1\"\2\3/g;
      $c2=~s/=\[pred *= *\'([^\']*)\' *([\,\]])/=\[pred=\"\1\"\2/g;
      $c2=~s/\\//g;
      while ($c2=~s/(\"[^ \"\[]*) ([^\[]*\")/\1_\2/) {
      }
      ;

      $c2=~s/^adv[mp]/adv/;

      # terminaux: ":"->"-" / eh bien plus maintenant
      if ($format eq "polgram") {
	$c2=~s/^(ppron3)([^\t]*:praep)/$1-prep$2/;
	$c2=~s/^([^\t:]*):[^\t]*/\1/;
	$c2=~s/^pred\t/prd\t/;
	if ($c2=~s/^([^\t]*)(?::BS|:AAA)\t/$1\t/) {$c2=~s/cat\s*=\s*$1(?::BS|:AAA)/cat = $1/}
	if ($c2=~s/^([^\t]*):ABBR\t/$1\t/) {$c2=~s/cat\s*=\s*$1:ABBR/cat = $1/}
      }
      # ":" est maintenant accept�
      #      while ($c2=~s/^([^\t]*):/\1-/) {
      #      }
      # AUGMENTATION DU NOMBRE DE TERMINAUX
      if ($format ne "polgram") {
	if ($c2=~/form\s*=\s*virgule/) {
	  $c2=~s/^ponctw/virgule/;
	}
	if ($c1=~/^rien\t/ && $c2=~/^pro/) {
	  $c2=~s/^[^ \t]+/rien/;
	}
	if ($c1=~/^et\t/ && $c2=~/^coo/) {
	  $c2=~s/^[^ \t]+/et/;
	}
	if ($c1=~/^_NUMBER\t/) {
	  $c2=~s/^([^ \t]+)/\1Number/;
	}
	### pri et prel
	if ($c2=~/^pr(?:i|el)\t/) {
	  if ($c2=~s/, *\@pro_([a-z]+)\b// || $c2=~s/, *case = ([a-z]+)\b//) {
	    my $cas=$1;
	    $c2=~s/^pr(i|el)\t/pr\1\_$cas\t/;
	  }
	}
	### verbes et auxilliaires
	$c2=~s/=\? \[Suj =\?/=\? \[SujC =\?/g;
	$c2=~s/(pred=\"[^<]*):/$1/;
	$c2=~s/^aux(Etre|Avoir)/aux\[aux=$1\]/;
	if ($c2=~/^v[^\t]*\t\[pred=\"(?:re)?(voici|voil�|v\'l�)___/) {
	  $c2=~s/^v/vVoici/;
	} elsif ($c2=~/^(v|cfi?)[\t\[]/) { # r�gler le sort des @pron_possible...
	  if ($c2=~/^(v|cfi?)[\t\[].*pred=\"se_/ || $c2=~/^(v|cfi?)[\t\[].*refl *=c *\+/) { # r�gler le sort des @pron_possible...
	    $c2=~s/^(v|cfi?)/\1\[pron=+\]/;
	  } else {
	    $c2=~s/^(v|cfi?)/\1\[pron=-\]/;
	  }
	}
	if ($c2=~/^(v|aux)[\[\t].*\@K/ || $c2=~/^(v|aux)[\[\t].*v-form *= *past-participle/) {
	  $c2=~s/^(v|aux)/\1\[mode=ppart\]/;
	} elsif ($c2=~/^(v|aux)[\[\t].*\@W/ || $c2=~/^(v|aux)[\[\t].*v-form *= *infinitive/) {
	  $c2=~s/^(v|aux)/\1\[mode=inf\]/;
	  $c2=~s/(?<=[<,])(\(?)Suj(\)?)(?=[>,])/\1SujC\2/o;
	  $nop=qr/[^\[\]]*/o;
	  $c2=~s/^([^\t]*\t\[$nop(?:\[$nop(?:\[$nop\]$nop)*\]$nop)*)Suj =/\1SujC =/o;
	} elsif ($c2=~/^(v|aux)[\[\t].*\@Y/ || $c2=~/^(v|aux)[\[\t].*mode *= *imperative/) {
	  $c2=~s/^(v|aux)/\1\[mode=imp\]/;
	} elsif ($c2=~/^(v|aux)[\[\t].*\@G/ || $c2=~/^(v|aux)[\[\t].*v-form *= *participle/) {
	  $c2=~s/^(v|aux)/\1\[mode=prespart\]/;
	} elsif ($c2=~/^(v|aux)[\[\t]/) {
	  $c2=~s/^(v|aux)/\1\[mode=std\]/;
	}
	if ($c2=~/^(v|adj|cfi?)[\[\t].*\@impers/ || $c2=~/^(v|adj|cfi?)[\[\t].*imp = -/) {
	  $c2=~s/^(v|adj|cfi?)/\1\[impers=-\]/;
	} elsif ($c2=~/^(v|adj|cfi?)[\[\t]/) { #pers_possible???
	  $c2=~s/^(v|adj|cfi?)/\1\[impers=il\]/;
	}
	$c2=~s/\]\[/,/g;
	$c2=~/^([^ \t]+)(.*)$/;
	$c2=sortattrs($1).$2;
	$cadre=~s/^<(.*)>$/\1/;
	$c2=~s/^(vVoici|v|cfi?|adj|pro)(\[[^ \t]*\])?\t/\1\2\[scat=$cadre\]\t/;
	$c2=~s/\]\[/,/;
      }
      ### verbes et auxilliaires ###
      if ($format ne "sxlfg-fr" && $c2=~/^v.*pred=\"[^<]*<([^>]*)>/ && $format ne "sxlfg-fr") {
	$souscat=$1;
	$souscat=~s/\(?[sv]?subj[^>,\"<]*//;
	$souscat=~s/(^|,),/$1/;
	$souscat=~s/,$//;
	$souscat=~s/whcomp/scomp/g;
	$souscat=~s/(de|�)-/$1--/g;
	$souscat=~s/[^-,<\|\(]+-([^-])/prep$1/g;
	$souscat=~s/de--/de/g;
	$souscat=~s/�--/a/g;
	while ($souscat=~s/([,\|]|^)([^,\|]+)((?:\|[^,]+)?)\|\2([\|,]|$)/$1$2$3$4/g) {
	}
	$souscat=join(",",sort(split(/,/,$souscat)));
      }
      ### adj pre-det
      if ($c2=~/^adj\t.*pre_det=+/) {
	$c2=~s/^adj/adjPredet/;
      }
      ### adv � la forme d'un groupe pr�positionnel ou nominal (!)
      if ($c2=~/chunk_type\s*=\s*(GP|GN|NV)/) {
	my $chunk_type = $1;
	$c2=~s/^([^ \t_\[\],=\+]+)/\1$chunk_type/;
      }
      if ($c2=~/adv_kind\s*=\s*mod(nc|prep)/) {
	my $adv_mod = $1;
	$c2=~s/^([^ \t_\[\],=\+]+)/\1Mod$adv_mod/;
      }
      if ($c2=~/adv_kind\s*=\s*tr�s/) {
	my $adv_mod = $1;
	$c2=~s/^([^ \t_\[\],=\+]+)/\1Tres/;
      }
      
      ### nc de temps
      if ($c2=~/^nc\t.*\@time/ || $c2=~/^nc\t.*time *= *\+/) {
	$c2=~s/^nc/ncTemps/;
      }
      if ($c2=~/^nc\t.*\@weekday/ || $c2=~/^nc\t.*weekday *= *\+/) {
	$c2=~s/^nc/ncJour/;
      }
      if ($c2=~/^nc\t.*\@year/ || $c2=~/^nc\t.*year *= *\+/) {
	$c2=~s/^nc/ncAnnee/;
      }
      ### preps '�' et 'de'
      if ($c2=~/^prep\t\[pred=\"�___/) {
	$c2=~s/^prep/prepA/;
      }
      if ($c2=~/^prep\t\[pred=\"de___/) {
	$c2=~s/^prep/prepDe/;
      }
      if ($c2=~/^prep\t\[pred=\"en___/) {
	$c2=~s/^prep/prepEn/;
      }
      if ($c2=~/^prep\t\[pred=\"comme___/) {
	$c2=~s/^prep/prepComme/;
      }
      ### d�terminant 'de' (de bonnes id�es)
      if ($c1=~/^de\t/ && $c2=~/^det\t/) {
	$c2=~s/^det/detDe/;
      }

      # output finale
      if ($c2=~/^v[\[\t].*\@Ctrl/ && (/vcomp\|/ || /\|(?:[a-z���]+\-)?vcomp/)) {
	$c2=~/^(.*[<,])((?:[^<,]*[^-a-z���]))((?:[a-z���]+\-)?vcomp)([^,>]*)(.*?)(, *\@Ctrl[A-Za-z�]+)(.*)$/;
	$c21=$1.$2.$4.$5.$7;
	$c22=$1.$3.$5.$6.$7;
	$c21=~s/([\(\|,<])\|/$1/;
	$c21=~s/\|([\)\|,>])/$1/;
	$c22=~s/,\@[A-Za-z]*Comp[A-Za-z]+//;
	$lexhash{"$c1\t$c21;"}=1;
	$lexhash{"$c1\t$c22;"}=1;
      } elsif ($c2 !~ /^aux[^\t]mode=imp/) {
	if ($c2!~/vcomp/) {
	  $c2=~s/,\@Ctrl[A-Za-z�]+//;
	}							     # � faire dans syntax.props?
	if ($c2!~/ssubj/) {
	  $c2=~s/,\@S(CompSuj)(Subj|Ind)//;
	}			# idem?
	$c2=~s/^(\S+)// || die "erreur: $c2";
	$term = $1;
	$origterm=$term;
	$term=~s/\|/-/g;
	$term=~s/[\[\],=\+]/_/g; # ":" est maintenant accept�
	$term=~tr/[�,�,�]/[a,e,e]/;
	$lexhash{"$c1\t$term$c2;"}=1;
      }
      $origterm=~s/\[.*//;
      $cadres{$origterm}{$cadre}++;
    }
  }
}
print STDERR "\tParsing lex files: $l                                              \n";
if ($format eq "sxlfg-fr") {
    ### virgule comme coo
    $lexhash{"les_plus\t300\tadjPref\t[pred=\"le_plus_____1\",cat = adv];"}=1;
    $lexhash{"le_plus\t300\tadjPref\t[pred=\"le_plus_____1\",cat = adv];"}=1;
    $lexhash{"le_moins\t300\tadjPref\t[pred=\"le_plus_____1\",cat = adv];"}=1;
    $lexhash{"\"(\"\t100\tponctw\t[];"}=1;
    $lexhash{"\")\"\t100\tponctw\t[];"}=1;
    $lexhash{"\"[\"\t100\tponctw\t[];"}=1;
    $lexhash{"\"]\"\t100\tponctw\t[];"}=1;
}
if ($format eq "polgram") {
    $lexhash{"nie\t200\tnie\t[];"}=1;
}

my @macrosSF;
my (%macrosSF,%macrosNonSF);
my ($macrosSF_tot,$macrosNonSF_tot);
if ($format eq "sxlfg-fr") {
  $l=0;
  for (keys %lexhash) {
    if ($l % 1000 == 0) {
      print STDERR "\tExpanding and re-factorizing macros: $l\r";
    }
    $l++;
    chomp;
    
    if (/^\s*;\s*$/) {delete($lexhash{$_}); next}
    if (/^(.*\t\[.*?),([^<>\t]+)(\].*)$/) {
      $debut=$1;
      $macros=$2;
      $fin=$3;
      @macrosSF=();
      delete($lexhash{$_});
      while ($macros=~s/((?:SujC|Suj|Objde|Obj�|Obj|Att|Obl2|Obl|Loc|Dloc)\s*=\??\s*\[(?:$parenth)?\][0-9]*)//) {
	push(@macrosSF,$1);
	$t=$1;
      }
      $macros=~s/,(\s*,)+/,/g;
      $macros=~s/^,//;
      $macros=~s/^\s*//;
      $macros=~s/,\s*$//;
      $macros=~s/\s*$//;
      $macros =~ s/=c([^ ])/= c\1/g;
      $macrosSF = join(",",@macrosSF);
      if ($macros ne "") {
	if (!defined($macrosNonSF{$macros})) {
	  $macrosNonSF_tot++;
	  $macrosNonSF{$macros}=$macrosNonSF_tot;
	}
	$debut.=",\@macro1_$macrosNonSF{$macros}";
      }
      if ($macrosSF ne "") {
	if (!defined($macrosSF{$macrosSF})) {
	  $macrosSF_tot++;
	  $macrosSF{$macrosSF}=$macrosSF_tot;
	}
	$debut.=",\@macro2_$macrosSF{$macrosSF}";
      }
      $lexhash{$debut.$fin}=1;
    }    
  }
  for (keys %macrosNonSF) {
    print "\@macro1_".$macrosNonSF{$_}." = [".$_."];\n";
  }
  for (keys %macrosSF) {
    print "\@macro2_".$macrosSF{$_}." = [".$_."];\n";
  }
  print STDERR "\n";
}


for (keys %lexhash) {
    next if (/^\s*;\s*$/);
    print "$_\n";
}

for my $cat (keys %cadres) {
  for (keys %{$cadres{$cat}}) {
    print "#SCAT\t$cat\t$_\n";
  }
}

sub sortattrs {
  my $rule=shift;
  my $srule;
  my @a;
  while ($rule=~s/^([^\[]*)(?=\[)//) {
    $srule.=$1."[";
    @a=();
    $rule=~s/^\[([^\]]*)\]// || die "Error (sortattrs) : \"$rule\"";
    @a=split(/\s*,\s*/,$1);
    $srule.=(join(",",sort @a))."]";
  }
  $srule.=$rule;
  return $srule;
}
