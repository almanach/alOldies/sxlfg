#!/usr/bin/perl

while (<>) {
    if (/^\s*<E sid=\"(.*?)\"/) {
	&output();
	%fs2arg = ();
	%fs2cat = ();
	%fs2vform = ();
	%fs2real = ();
	%fs2lexeme = ();
	$sid = $1;
    } elsif (/^\s*<f_structure id=\"(.*?)\"/) {
	$fsid = $1;
    } elsif (/^\s*<\/f_structure/) {
	$fsid = "";
    } elsif ($fsid ne "" && /^\s*<lexeme value=\"(.*)\"\/>/) {
	$fs2lexeme{$fsid} = $1;
    } elsif ($fsid ne "" && /^\s*<attribute name=\"(.*?)\"/) {
	$att = $1;
	$_ = <>;
	if (/^\s*<value [^ ]+=\"(.*)\"\/>/) {
	    $val = $1;
	    if ($att eq "cat") {
		$fs2cat{$fsid} = $val;
	    } elsif ($att eq "v-form") {
		$fs2vform{$fsid} = $val;
	    } elsif ($att eq "real") {
		$fs2real{$fsid} = $val;
	    } elsif ($att =~ /^(SujC?|Objde|Objà|Obj|Dloc|Loc|Att|Obl2?)$/) {
		$fs2arg{$fsid}{$att} = $val;
	    }
	}
    }
}
&output();

sub output {
    for $fsid (keys %fs2arg) {
	next unless (defined($fs2lexeme{$fsid}));
	print $sid."\t".$fs2lexeme{$fsid}."\t".$fs2cat{$fsid}."\t".$fs2vform{$fsid}."\t<";
	$bool = 0;
	for $arg (qw/Suj SujC Obj Objà Objde Att Loc Dloc Obl Obl2/) {
	    next unless (defined($fs2arg{$fsid}{$arg}));
	    if ($bool == 1) {print ","} else {$bool=1}
	    print $arg.":";
	    if (defined($fs2real{$fs2arg{$fsid}{$arg}})) {
		print $fs2real{$fs2arg{$fsid}{$arg}};
	    } else {
		print "?";
	    }
	}
	print ">\n";
    }
}
