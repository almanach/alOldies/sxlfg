#!/usr/bin/perl

use strict;
use constant {
    RULE      => 0,
    COMMENT   => 1,
    EQUATIONS => 2,
    VARIABLES => 3,
    SCATS => 3,
};

my $stop_at_END=1;
my $scatsfile=0;
my $oopt=0;
my $noeq=0;
my $escape_chars=1;
my $language_name="";
while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif ($_ eq "-all") {$stop_at_END=0}
    elsif ($_ eq "-opt" || $_ eq "-o") {$oopt=1}
    elsif ($_ eq "-nec") {$escape_chars=0}
    elsif ($_ eq "-bnf") {$escape_chars=0; $oopt=1}
    elsif ($_ eq "-noeq") {$noeq=1}
    elsif ($_ eq "-scats") {$scatsfile=1;}
    elsif ($_ =~ /^([^-].*)$/) {$language_name=$1;}
}

die "No language name specified" if ($language_name eq "");

my $add_null_rule=0;
my $status=RULE;
my $prevstatus;
my %star;
my %plus;
my %coo;
my @rules;
my $eq;
my @allrules;
my @alleqs;
my %vars;
my %scats;
my (%disjunction_rules,%scat_rules,%opt_rules);
my %scat_extract_cache;
my ($tmp,$tmp1,$tmp2,$tmp3);

my $line=0;

print STDERR "  Parsing scats file...";
if ($scatsfile) {
  open(SC,"<$language_name.scats") || die "Could not open $language_name.scats: $!";
  while (<SC>) {
    chomp;
    s/\|/\!\!/g;
    if (/^([^\t]+)\t(.*)/) {
      $scats{$1}{$2}=1;
      push(@{$vars{scat}{$1}},$2);
    }
  }
  close(SC);
}

print STDERR "done\n  Parsing mlfg grammar...";
open(MLFG,"<$language_name.mlfg") || die "Could not open $language_name.mlfg: $!";
while (<MLFG>) {
  chomp;
  $line++;
  if ($line % 10) {
    print STDERR "\r  Parsing mlfg grammar...$line";
  }
  if (/^__END__$/) {
    if ($stop_at_END) {
      last;
    } else {
      next;
    }
  }
  if (s/ *\/\/(.*)$//) {
    #	print "/* $1 */\n";
  }
  if (/^\#include/) {
    print "$_\n";
  } elsif (s/ *\#(.*)$//) {
    #	print "/* $1 */\n";
  }
  if (/\/\*/) {
    $prevstatus=$status; $status=COMMENT;
  }
  if ($status!=COMMENT) {
    if (/->/) {
      @{$allrules[$#allrules+1]}=@rules;
      $alleqs[$#alleqs+1]=$eq;
      $status=RULE;
      $eq="";
      $_=sortattrs($_);
#      while (s/(^|\n)([^\|\n]+) (\(?)([^ \|\(\n]+)\|([^ \)\n]+)(\)?)([^\n]*)($|\n)/$1$2 $3$4$6$7\n$2 $3$5$6$7$8/) {
#      }
      
#      print STDERR "\n";
      @rules=();
##      for (split(/\n/,$_)) {
#	print STDERR "$_\n";
      $_=expand_rule($_);
      
      push(@rules,split(/\n/,$_));
      #	print STDERR $#rules." ";

##      }
#      print STDERR "\n";

      for my $i (0..$#rules) {
	$_=$rules[$i];
	s/$/\n/;
	while (s/([^ \(\n\/]*[^ \(\n\/=])\*/$1\___STAR/) {
	  $star{$1}++;
	}
	while (s/([^ \(\n\/]*[^ \(\n\/=])\+/$1\___PLUS/) {
	  $plus{$1}++;
	}
	while (s/(^|\n)([^\n ]+) *-(?:([A-Z0-9a-z_]+),)?([A-Z0-9a-z_]+)(?:\[([^\]\n]*)\])?->([^\n]+)($|\n)/$1$2\___1 ->$6$7/) {
	  $tmp=$2;
	  $tmp1=$3;
	  $tmp2=$4;
	  $tmp3=$5;
	  $tmp=~s/\!\!/\|/g;
	  if ($tmp1 eq "") {$tmp1 = $tmp2}
	  $coo{$tmp}{tok1}=$tmp1;
	  $coo{$tmp}{tok}=$tmp2;
	  $coo{$tmp}{args}=$tmp3;
	}
	
	$tmp=$_;
	$tmp=~s/^(.*->\s*)//;
	while ($tmp=~s/^(\S+)\s*//) {
	  $tmp2=$1;
	  if ($tmp2=~s/^\((.*)\)$/\1/) {
	    $opt_rules{$tmp2}=1;
	  }
	  if ($tmp2=~/\|/) {
	    $disjunction_rules{$tmp2}=1;
	  }
	}
	
	s/\!\!/\|/g;
	$rules[$i]=$_;
      }

#      if (s/(^|\n)([^\n ]+) *-([A-Z0-9a-z_]+)(?:\[([^\]]*)\])?->([^\n]+)($|\n)/$1$2\___1 ->$5$6/g) {
	# rien � faire, on poursuit les substitutions entam�es � la ligne d'au dessus, donc $coo a d�j� �t� rempli
#      }


    } elsif (/^\s*([a-zA-Z0-9]+)={(.*)}$/) {
      $status=VARIABLES;
      @{$vars{$1}}=split(",",$2);
    } elsif (/([=<]|[\+-]\()/ || /^$/) {
      $status=EQUATIONS;
      if (!$noeq) {
	$eq.=$_."\n";
      }
      #	} else {
      #	    print "$_\n";
    }
    #    } else {
    #	print "$_\n";
  }
  if (/\*\// || /\/\//) {
    $status=$prevstatus;
  }
}
$status=RULE;
@{$allrules[$#allrules+1]}=@rules;
$alleqs[$#alleqs+1]=$eq;

print STDERR "\r  Parsing mlfg grammar...done        \n  Generating expanded grammar...";

#my (%cooRE1,%cooRE2,%cooRE3);
my %cooRE;
my $escnt;
my $cooRE;
for my $nt (sort keys %coo) {
  $escnt=quotemeta($nt);
  $cooRE.="|$escnt";
#  $cooRE{$nt}=qr/(^| \(?)$escnt(\)?) /;
#   $cooRE1{$nt}=qr/ $escnt /;
#   $cooRE2{$nt}=qr/ \($escnt\) /;
#   $cooRE3{$nt}=qr/^$escnt /;
}
$cooRE=~s/^\|//;
$cooRE=qr/(^| \(?)($cooRE)(\)?) /o;

print STDERR "\r  Generating expanded grammar...regular rules                     ";
print "\n/* regular rules*/\n\n";
my $temprule;
my $rhsntn;
my $rhsnt;
my $rulenumber;
my %seenruleparts;
for my $multirulenumber (0..$#allrules) {
  @rules=@{$allrules[$multirulenumber]};
  print STDERR "\r  Generating expanded grammar...regular rules (".($multirulenumber+1)."/".($#allrules+1).")             ";
  for $rulenumber (0..$#rules) {
    $_=$rules[$rulenumber];
#    if ($escape_chars) {
      s/(?<=\s)([^\[][^\s\[]*(?:\[[^\s\[]*\][^\s\[]*)*\|\S+) /DISJ__\1 /g;
#    }
    s/$cooRE/$1$2\___c$3 /g;
    s/$cooRE/$1$2\___c$3 /g;
    s/\n*$/\n/;
    $eq=$alleqs[$multirulenumber];
    if ($eq ne "") {
      $temprule=$_;
      $temprule=~s/^.*-> *//;
      $rhsntn=0;
      while ($temprule=~s/([^ ]+)\)? +//) {
	$rhsntn++;
	if ($1=~/^\(?(.*)___c\)?$/) {
	  $rhsnt=$1;
	  for (split(/, */,$coo{$rhsnt}{args})) {
	    $eq=~s/\$$rhsntn([ \.]$_[ \.\);])/\$$rhsntn.arg1$1/g;
	  }
	}
      }
    }
    if ($escape_chars) {
      s/\|/-/g;
      s/[\[\],=\+]/_/g; # ":" est devenu ok
      tr/[�,�,�]/[a,e,e]/;
    }
    print "$_$eq\n";
  }
}
print STDERR "\r  Generating expanded grammar...coo rules                                               ";
my $orig;
print "\n/* coo-like productions */\n\n";
for (keys %coo) {
  $orig=$_;
  if ($escape_chars) {
    s/\|/-/g;
    s/[\[\],=\+]/_/g; # ":" est devenu ok
    tr/[�,�,�]/[a,e,e]/;
  }
  print "$_\___c -> $_\___1 ;\n";
  if (!$noeq) {
    print "\t\$\$ = \$1;\n";
    for (split(/, */,$coo{$orig}{args})) {
      if ($_ ne "adjunct") {
	if (s/^\@//) {
	  print "\t\$\$ $_ =? ();\n";
	} else {
	  print "\t\$\$ $_ =? [];\n";
	}
      }
    }
  }
  print "$_\___cn -> $_\___1 ;\n";
  if (!$noeq) {
    print "\t\$\$ = \$1;\n";
    for (split(/, */,$coo{$orig}{args})) {
      if ($_ ne "adjunct") {
	if (s/^\@//) {
	  print "\t\$\$ $_ =? ();\n";
	} else {
	  print "\t\$\$ $_ =? [];\n";
	}
      }
    }
  }
  print "$_\___c -> ($coo{$orig}{tok1}) $_\___1 $coo{$orig}{tok} $_\___cn ;\n";
  if (!$noeq) {
    print "\t\$\$ = \$3;\n";
    print "\t\$\$ arg1 = \$2;\n";
    print "\t\$\$ arg2 = \$4;\n";
    for (split(/, */,$coo{$orig}{args})) {
      s/^\@//;
      print "\t\$\$ arg1 $_ =? \$\$.arg2.arg1 $_;\n";
    }
  }
  print "$_\___cn -> $_\___1 $coo{$orig}{tok} $_\___cn ;\n";
  if (!$noeq) {
    print "\t\$\$ = \$2;\n";
    print "\t\$\$ arg1 = \$1;\n";
    print "\t\$\$ arg2 = \$3;\n";
    for (split(/, */,$coo{$orig}{args})) {
      s/^\@//;
      print "\t\$\$ arg1 $_ =? \$\$.arg2.arg1 $_;\n";
    }
  }
#   print "$_\___c -> ($coo{$orig}{tok}) $_\___1 $coo{$orig}{tok} $_\___1 ;\n";
#   if (!$noeq) {
#     print "\t\$\$ = \$3;\n";
#     print "\t\$\$ arg1 = \$2;\n";
#     print "\t\$\$ arg2 = \$4;\n";
#     for (split(/, */,$coo{$orig}{args})) {
#       s/^\@//;
#       print "\t\$\$ arg1 $_ =? \$\$.arg2 $_;\n";
#     }
#   }
}
print STDERR "\r  Generating expanded grammar...* and + rules                     ";
print "\n/* ___STAR and ___PLUS non-terminals */\n\n";
for (keys %star) {
  $orig=$_;
  if ($escape_chars) {
    s/\|/-/g;
    s/[\[\],=\+]/_/g;  # ":" est devenu ok
    tr/[�,�,�]/[a,e,e]/;
  }
  if (defined($coo{$orig})) {
    print "$_\___STAR -> $_\___STAR $_\___c ;\n";
    if (!$noeq) {
      print "           (\$\$ = \$1);\n";
      print "           (\$2 < \$\$ adjunct);\n";
    }
  } else {
    print "$_\___STAR -> $_\___STAR $_ ;\n";
    if (!$noeq) {
      print "           (\$\$ = \$1);\n";
      print "           (\$2 < \$\$ adjunct);\n";
    }
  }
  print "$_\___STAR -> ;\n";
}
for (keys %plus) {
  $orig=$_;
  if ($escape_chars) {
    s/\|/-/g;
    s/[\[\],=\+]/_/g;  # ":" est devenu ok
    tr/[�,�,�]/[a,e,e]/;
  }
  if (defined($coo{$orig})) {
    print "$_\___PLUS -> $_\___PLUS $_\___c ;\n";
    if (!$noeq) {
      print "           (\$\$ = \$1);\n";
      print "           (\$2 < \$\$ adjunct);\n";
    }
    print "$_\___PLUS -> $_\___c ;\n";
    if (!$noeq) {
      print "           (\$\$ = \$1);\n";
    }
  } else {
    print "$_\___PLUS -> $_\___PLUS $_ ;\n";
    if (!$noeq) {
      print "           (\$\$ = \$1);\n";
      print "           (\$2 < \$\$ adjunct);\n";
    }
    print "$_\___PLUS -> $_ ;\n";
    if (!$noeq) {
      print "           (\$\$ = \$1);\n";
    }
  }
}
if ($oopt) {
  print STDERR "\r  Generating expanded grammar...optionality rules                     ";
  print "\n/* optional productions */\n\n";
  for my $or (keys %opt_rules) {
    $orig=$or;
    if ($escape_chars) {
      $or=~s/\|/-/g;
      $or=~s/[\[\],=\+]/_/g;  # ":" est devenu ok
      $or=~tr/[�,�,�]/[a,e,e]/;
    }
    if (defined($coo{$orig})) {$or.="___c"}
    print "($or) -> $or ;\n";
    if (!$noeq) {
      print "    \$\$ = \$1 ;\n";
    }
    print "($or) -> ;\n";
  }
}
print STDERR "\r  Generating expanded grammar...disjunction rules                     ";
print "\n/* disjunction productions */\n\n";
my $drorig;
for my $dr (keys %disjunction_rules) {
  if (defined($coo{$dr})) {$dr.="___c"}
  $drorig = $dr;
  $dr=~s/\!\!/\|/g;
  if ($escape_chars) {
    $dr=~s/\|/-/g;
    $dr=~s/[\[\],=\+]/_/g;  # ":" est devenu ok
    $dr=~tr/[�,�,�]/[a,e,e]/;
#    if ($dr=~/^[a-z]/) {
      $dr=~s/^/DISJ__/;
#    }
  }
  for (split(/\|/,$drorig)) {
    $orig=$_;
    s/\!\!/\|/g;
    if ($escape_chars) {
      s/\|/-/g;
      s/[\[\],=\+]/_/g;  # ":" est devenu ok
      tr/[�,�,�]/[a,e,e]/;
    }
    if (defined($coo{$orig})) {$_.="___c"}
    print "$dr -> $_ ;\n";
    if (!$noeq) {
      print "    \$\$ = \$1 ;\n";
    }
  }
}
print STDERR "\r  Generating expanded grammar...scat rules                     ";
print "\n/* scat productions */\n\n";
my %subcats;
for my $sc (keys %scat_rules) {
  $sc=~s/\!\!/\|/g;
  $orig=$sc;
  if ($sc=~/\|/) {
    $sc=~s/^/DISJ__/;
  }
  if ($escape_chars) {
    $sc=~s/\|/-/g;
    $sc=~s/[\[\],=\+]/_/g;  # ":" est devenu ok
    $sc=~tr/[�,�,�]/[a,e,e]/;
  } 
  for (split(/\|/,$orig)) {
    /^(.*):(.*)$/ || die "erreur subcats : $_";
    if (&real2nt($2) ne "") {
      $orig=$_;
      if ($escape_chars) {
	s/\|/-/g;
	s/[\[\],=\+]/_/g;  # ":" est devenu ok
	tr/[�,�,�]/[a,e,e]/;
      }
      print "$sc -> REAL__$_ ;\n";
      if (!$noeq) {
	print "    \$\$ = \$1 ;\n";
      }
      $subcats{$orig}++;
    }
  }
  print "$sc -> ;\n";
}
if ($add_null_rule) {
  print "NULL -> ;\n";
}
print STDERR "\r  Generating expanded grammar...scat realization rules                     ";
for (keys %subcats) {
  /^(.*):(.*)$/ || die "erreur subcats";
  if (&real2nt($2) ne "") {
    if ($escape_chars) {
      s/\|/-/g;
      s/[\[\],=\+]/_/g;  # ":" est devenu ok
      tr/[�,�,�]/[a,e,e]/;
    }
    print "REAL__$_ -> ".&real2nt($2)." ;\n";
    if (!$noeq) {
      print "   \$\$ $1 = \$1 ;\n";
    }
  }
}
if ($stop_at_END) {
  print "CONSTITUANT_OU_TERMHORSCONSTITUANT -> CONSTITUANT ;\n";
  if (!$noeq) {
    print"     \$\$ = \$1;\n";
  }
  print "CONSTITUANT_OU_TERMHORSCONSTITUANT -> TERMHORSCONSTITUANT ;\n";
  if (!$noeq) {
    print"     \$\$ = \$1;\n";
  }
  # terminaux hors constituants Easy (chunks)
  for (
       "Easy_00sbound",
       "Easy_00csu",
       "Easy_00ponctw",
       "Easy_00coo",
       "Easy_00poncts",
       "Easy_00poncts",
       "Easy_00que",
       "Easy_00pri",
       "Easy_00pri_nom",
       "Easy_00pri_acc",
       "Easy_00pri_loc"
      ) {
    for (split(/\n/,expand_rule("TERMHORSCONSTITUANT -> $_ ;"))) {
      s/\!\!/\|/g;
      if ($escape_chars) {
	s/\|/-/g;
	s/[\[\],=\+]/_/g;  # ":" est devenu ok
	tr/[�,�,�]/[a,e,e]/;
      }
      print "$_\n";
      if (!$noeq) {
	print"     \$\$ = \$1;\n";
      }
    }
  }
  # constituants EASy (chunks)
  for (
       "Easy_GA[scat(adj)=\$s]",
       "Easy_GA[scat(v)=\$s]",
       "Easy_GA[scat=]",
       "Easy_GN",
       "Easy_GNdet",
       "Easy_GNpri_nom",
       "Easy_GNpri_acc",
       "Easy_GNrel",
       "Easy_GNrel_nom",
       "Easy_GNrel_acc",
       "Easy_GNtmps",
       "Easy_GNpred[impers=\$i,pron=\$p,scat(cf|cfi)=\$s]",
       "Easy_GNpro",
       "Easy_GNpres",
       "Easy_GNadv",
       "Easy_GPadv",
       "Easy_GP",
       "Easy_GPdet",
       "Easy_GPrel_loc",
       "Easy_GPrel_gen",
       "Easy_GPpres",
       "Easy_GPpreppri",
       "Easy_GPpreppri_acc",
       "Easy_GPpreppri_loc",
       "Easy_GPpreprel",
       "Easy_GPpreprel_nom",
       "Easy_GPpreprel_loc",
       "Easy_GPdeDate",
       "Easy_GPaDate",
       "Easy_GPprepDate",
       "Easy_GPadj",
       "Easy_GPqual",
       "Easy_GR",
       "Easy_GRmodnc",
       "Easy_GRmodprep",
       "Easy_GRpres",
       "Easy_GRneg",
       "Easy_GRpri",
       "Easy_GRpri_loc",
       "Easy_NVadv",
       "GPnv",
       "NV[impers=\$i,scat(v)=\$s]___c",
       "NV[impers=\$i,scat(adj)=\$s]___c",
       "NV[impers=-,scat(vVoici)=\$s]___c",
       "NV_IMP[scat(v)=\$s]___c",
       "NV_INF[scat(v)=\$s]___c",
       "PV_INF_A[scat(v)=\$s]___c",
       "PV_INF_DE[scat(v)=\$s]___c",
       "PV_INF_PREP[scat(v)=\$s]___c",
       "PV_GERONDIF[scat(v)=\$s]___c"
      ) {
    for (split(/\n/,expand_rule("CONSTITUANT -> $_ ;"))) {
      s/\!\!/\|/g;
      if ($escape_chars) {
	s/\|/-/g;
	s/[\[\],=\+]/_/g;  # ":" est devenu ok
	tr/[�,�,�]/[a,e,e]/;
      }
      print "$_\n";
      if (!$noeq) {
	print"     \$\$ = \$1;\n";
      }
    }
  }

} else {
    print "CONSTITUANT_OU_TERMHORSCONSTITUANT -> prep prep prep prep prep prep ;\n";
    if (!$noeq) {
      print "	    \$\$ = \$1;\n";
    }
}

print STDERR "\r  Generating expanded grammar...done                                            \n";

sub sortattrs {
  my $rule=shift;
  my $srule;
  my @a;
  while ($rule=~s/^([^\[]*)(?=\[)//) {
    $srule.=$1."[";
    @a=();
    $rule=~s/^\[([^\]]*)\]// || die "non";
    @a=split(/\s*,\s*/,$1);
    $srule.=(join(",",sort @a))."]";
  }
  $srule.=$rule;
  return $srule;
}

sub scat_extract {
  my $scat = shift;
  my $filter = shift;
  if (defined($scat_extract_cache{$scat}{$filter})) {
    return $scat_extract_cache{$scat}{$filter};
  } else {
    my $tmp;
    my $origfilter=$filter;
    if ($filter =~ s/^\~//) {
      $tmp=$scat;
      $tmp=~s/(^|,)($filter\:[^,]*)//;
      $tmp=~s/^,//;
    } else {
      if ($scat=~/(^|,)($filter\:[^,]*)/) {
	$tmp=$2;
      }
    }
    my $fs;
    my $answer="NULL";
    for (split(/,/,$tmp)) {
      /^([^:]+)/;
      $fs=$1;
      s/(\!\!|\|)/\1$fs:/g;
      if ($answer eq "NULL") {
	$answer = $_;
      } else {
	$answer.=" # ".$_;
      }
      $scat_rules{$_}=1;
    }
    $answer=~s/^ \# //;
    $scat_extract_cache{$scat}{$origfilter}=$answer;
    if ($answer eq "NULL") {$add_null_rule=1;}
    return $answer;
  }
}

sub real2nt {
  my $real = shift;
  my $answer;
  if (!$stop_at_END) {
    if ($real eq "sn") {$answer = "SN"}
    elsif ($real eq "sa") {$answer = "SA"}
    elsif ($real eq "sinf") {$answer = "INFINITIVE"}
    elsif ($real eq "�-sinf") {$answer = "INFINITIVE_A"}
    elsif ($real eq "de-sinf") {$answer = "INFINITIVE_DE"}
    elsif ($real eq "scompl") {$answer = "Prop_COMPLETIVE"}
    elsif ($real eq "�-scompl") {$answer = "Prop_COMPLETIVE_A"}
    elsif ($real eq "de-scompl") {$answer = "Prop_COMPLETIVE_DE"}
    elsif ($real =~ /�-sn/) {$answer = "SNP_prep_a____c"}
    elsif ($real =~ /de-sn/) {$answer = "SNP_prep_de____c"}
    elsif ($real =~ /-sn/) {$answer = "SNP_prep_autre____c"}
  } else {
    if ($real eq "sn") {$answer = "GNdet"}
    elsif ($real eq "sa") {$answer = "Easy_GA"}
#    elsif ($real eq "sinf") {$answer = "NV_INF"}
#    elsif ($real eq "scompl") {$answer = "COMPLETIVE"}
    elsif ($real =~ /-sn/) {$answer = "Easy_GPdet"}
  }
  if (defined($coo{$answer})) {$answer.="___c"}
  return $answer;
}

sub expand_rule {
  my $rule = shift;
  for my $var (keys %vars) {
    if ($rule=~s/($var)(?:\(([^\)]*)\))?\s*=\s*\$([^,\]]+)/\1=\$\3/g) {
      my $origrule=$rule;
      my $temprule;
      my $subvars=$2;
      my $varname=$3;
      $rule="";
      if ($subvars eq "") {
	for my $val (@{$vars{$var}}) {
	  $temprule=$origrule;
	  $temprule=~s/\$$varname/$val/g;
	  $rule.="\n".$temprule;
	}
      } else {
	for my $subvar (split(/\s*\|\s*/,$subvars)) {
	  if (!defined($vars{$var}{$subvar})) {
	    die "\nLine $line: no sub-attribute $subvar for attribute $var";
	  }
	  for my $val (@{$vars{$var}{$subvar}}) {
	    $temprule=$origrule;
	    $temprule=~s/\$$varname/$val/g;
	    $rule.="\n".$temprule;
	  }
	}
      }
      $rule=~s/^\n//;
      if ($rule=~/$var=\$/) {
	die "\nLine $line: 2 different variables for the same field ($var); which is not yet supported (varname=$varname)";
      }
    }
    #	  print STDERR "\t$var\t$_\n";
  }
  
  $rule=~s/\&scat_extract\(([^\)]*),([^;\)]+)\)/&scat_extract($1,$2)/ge;
  #	print STDERR "\nFIN\t$_\n";

  # A#B#C doit devenir A B C ou A C B ou C A B ou C B A ou B A C ou B C A --- pas plus que 3
  while ($rule=~/\#/) {
    $rule=~s/ *\# */\#/g;
    # 2
    $rule=~s/(^|\n)([^\n\#]*) ([^ \#\n]+)\#([^ \#\n]+)([^\n]*)($|\n)/\1\2 \3 \4\5\n\2 \4 \3\5\6/g;
    # 3
    $rule=~s/(^|\n)([^\n\#]*) ([^ \#\n]+)\#([^ \#\n]+)\#([^ \#\n]+)([^\n]*)($|\n)/\1\2 \3 \4 \5\6\n\2 \4 \3 \5\6\n\2 \3 \5 \4\6\n\2 \3 \4 \5\6\n\2 \4 \5 \3\6\n\2 \5 \3 \4\6\n\2 \5 \4 \3\6\7/g;
  }
  # s�curit�
  if ($rule=~/\#/) {
    print STDERR "### WARNING: the following expanded rule still contains \"#\":\n";
    for (split(/\n/,$rule)) {
      if (/\#/) {
 	print STDERR $_."\n";
      }
    }
  }
  return $rule;
}
