#!/usr/bin/perl

#usage: cat run_runname/odags/corpus.odag | perl odag2adag.pl runname.alp -c > runname.adag
#usage: cat run_runname/odags/corpus.odag | perl odag2adag.pl runname.alp > runname.adag

$alp_only_on_consistent=0;

while (1) {
  $option=shift;
  if ($option eq "") {
    last;
  } elsif ($option eq "-c") {
    $alp_only_on_consistent=1;
  } else {
    $lexprobasfile=$option;
  }
}

while (<>) {
    next if (/} [^_]+ /);
    chomp;
#    print $_;
    s/(^|[^\\])([\(\|\)])/\1 \2 /g;
    s/(^| )_error__( |$)/\1\2/g;
#    print $_;
    s/{[^\}]+} //g;
    while (s/(^| )\( ([^\(\)\|]+)(?: \| [^\(\)]+)? \)/\1 \2 /g) {}
    s/ +/ /g;
#    print $_;
#    print $_;
    s/(^|__)cl[ad]r/\1clr/g;
    s/(^|__)cl[adgl]/\1clo/g;
    s/(^|__)aux(Avoir|Etre)/\1v/g;
    s/(^|__)ncpred/\1nc/g;
    s/(^|__)(pri|prel)_[^ ]+/\1\2/g;
    s/(^|__)vVoici/\1pres/g;
    s/Se([^_ ]*)( |$)/\1\2/g;
    s/(^|__)epsilon ?/ /g;
    s/\\([\[\]\|\(\)\%\\])/$1/g;
    s/^ +//;
    if ($lexprobasfile ne "" && ($alp_only_on_consistent==0 || /\/\/ C$/)) {
      for $t (split(" ",$_)) {
	$t=~/^(.*)__(.*)$/;
	$word=$1;
	$cat=$2;
	if ($word !~/^_/ && $word !~/_$/) {$word=~s/_/ /g}
	$hash{$cat}{$word}++;
      }
    }
    s/(^| )[^ ]+__([^_]|$)/\1\2/g;
    next if ($_!~/^[a-zA-Z \/]+$/);
    if (/^[^ ].* \/\//) {
	print "$_\n";
    }
}

if ($lexprobasfile ne "") {
  open (TMP,"> $lexprobasfile") || die "could not open $lexprobasfile: $!\n";
  for $cat (keys %hash) {
    for $form (keys %{$hash{$cat}}) {
      print TMP  $hash{$cat}{$form}."\t$cat\t$form\n";
    }
  }
}
