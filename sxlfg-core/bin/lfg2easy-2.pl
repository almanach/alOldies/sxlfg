#!/usr/bin/perl

use XML::Twig;

use strict;

my $filename = shift;

my $twig= new XML::Twig( 
			twig_roots    => { 'E' => 1 },
			twig_handlers =>  { E => \&process_sentence }
                       );

my %ids;
my $xmlparsedata;
my $close_DOCUMENT = 0;
while (<STDIN>) {
  if ($xmlparsedata eq "" && $_ !~ /^<\?xml/) {
    $close_DOCUMENT = 1;
    $xmlparsedata = '<?xml version="1.0" encoding="ISO-8859-1"?>';
    $xmlparsedata .= "<DOCUMENT>";
  }
    for (split(/(?<=>)\s*(?=<)/,$_)) {
	if (/ id=\"([^\"]+)\"/) {
	    if ($ids{$1} == 0) {
		$ids{$1}=1;
		s/ id=/ xml:id=/g;
	    } else {
		s/ id=/ xml:idref=/g;
	    }
	} elsif (/ idref=\"([^\"]+)\"/) {
	    s/ idref=/ xml:idref=/g;
	}
	if (/^<E /) {
	    while (s/(input=\".*?)\"(.*\">)$/\1&quot;\2/) {}
	    while (s/(input=\".*?)<(.*\">)$/\1&lt;\2/) {}
	    while (s/(input=\".*?)>(.*\">)$/\1&gt;\2/) {}
	}
	$xmlparsedata .= $_;
    }
}
if ($close_DOCUMENT) {$xmlparsedata.="</DOCUMENT>";}

#fichier="general_elda.xml" id="reference" date="05/09/2006" 
print <<END;
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DOCUMENT SYSTEM "easy.dtd">
<DOCUMENT xmlns:xlink="http://www.w3.org/1999/xlink">
END

my ($eid,%tid2fid,$gid,$tid,$rid,$fstructures,$cstructure);
my ($localtwig, $parseroot);

$twig->parse($xmlparsedata) || die "Non-XML Parse"; # build the twig
$twig->purge;                                       # flush the end of the twig  

print <<END;
</DOCUMENT>
END




sub process_sentence {
  ($localtwig, $parseroot)= @_;
  $eid = $parseroot->{'att'}->{"sid"};
  $eid =~ s/^E//;
  print STDERR "$eid\r";
  %ids = ();
  $cstructure = $parseroot->first_child("c_structure");
  if ($cstructure == undef) {
    print STDERR "WARNING: No c-structures for sentence E$eid\n";
  } else {
    $fstructures = $parseroot->first_child("f_structures");
    print STDERR "WARNING: No f-structures for sentence E$eid\n" if ($fstructures == undef);    
    $gid = 0;
    $tid = "";
    %tid2fid = ();
    $rid = 0;
    print "<E id=\"E$eid\">\n";
    print "<constituants>\n";
    &print_easy_chunks($cstructure);
    print "</constituants>\n";
    print "<relations>\n";
    if ($fstructures) {
      &print_easy_relations();
    }
    print "</relations>\n";
    print "</E>\n";
  }
  $twig->purge();
}

sub print_easy_chunks {
    my $node=shift;
    my $easy_chunk=0;
    my $gtype;
    if ($node->gi eq "F") {
	$node->{'att'}->{"gid"} = "E${eid}G$gid";
	if ($node->{'att'}->{"xml:id"}) {
	    $tid2fid{$tid} = $node->{'att'}->{"xml:id"};
	    print "    <F id=\"".$node->{'att'}->{"xml:id"}."\">".$node->first_child->text."</F>\n";
	} else {
	    $tid2fid{$tid} = $node->{'att'}->{"xml:idref"};
	}
    } elsif ($node->gi eq "T") {
	$tid = $node->{'att'}->{"xml:id"};
	for ($node->children) {
	    print_easy_chunks ($_);
	}
    } else {
	if ($node->gi eq "node"
	    && $node->{'att'}->{"name"} =~ /^Easy_(G[ANPR]|[NP]V)/) {
	    $gid++;
	    $easy_chunk = 1;
	    $gtype = $1;
	    $node->{'att'}->{"gid"}="E${eid}G$gid";
	    $node->{'att'}->{"gtype"}="$gtype";
	}
	if ($easy_chunk) {
	    print "  <Groupe type=\"$gtype\" id=\"E${eid}G$gid\">\n";
	}
	for ($node->children) {
	    print_easy_chunks ($_);
	}
	if ($easy_chunk) {
	    print "  </Groupe>\n";
	}
    }
}

sub print_easy_relations {
  &print_SUJ_V();
  &print_AUX_V();
  &print_COD_V();
  &print_CPL_V();
  &print_MOD_V();
  &print_MOD_N();
  &print_MOD_A();
  &print_MOD_R();
  &print_MOD_P();
  &print_COMP();
  &print_ATB_SO();
  &print_COORD();
  &print_APPOS();
  &print_JUXT();
}

sub print_SUJ_V {
    my ($suj,$sujterm);
    my ($v,$origv,$cop,$copnt,$copntid,$fs);
    my $sujgroup;
    for $v ($fstructures->descendants("f_structure")) {
	next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
	$origv=$v;
	for my $type ("Suj","SujC") {
	    $v=$origv;
	    $suj = getSubStruct($v,$type);
	    if ($suj && hasPred($suj)) {
		$sujterm = getAnchorTerm($suj);
		next unless $sujterm;
		$v = getAnchorTerm($v);
		next if (!$v);
		$v = getClosestAncestor($v,"NV");
		next if (!$v);
		for ($v->children("node")) {
		    if ($_->{'att'}->{"name"} =~ /^Easy_NV/) {
			$v = $_->{'att'}->{"gid"};
			last;
		    }
		}
		$sujgroup = getClosestAncestor($sujterm,"Easy_");
		if ($sujgroup && $sujgroup->{'att'}->{"name"} !~ /^Easy_(00|NV)/) {
		    $suj = $sujgroup->{'att'}->{"gid"};
		} else {
		    $suj = getAnchor($suj);
		}
		if ($suj && $v) {
		    print_relation("SUJ-V","sujet","verbe",$suj,$v);
		}
	    }
	    $suj = undef;
	}
    }
    # constructions � copule: on r�cup�re la relation sujet-copule
    for $copnt ($cstructure->descendants("node")) {
	next if $copnt->{'att'}->{"name"} !~ /Easy_NVA[1U]copule/;
	$copntid = $copnt->{'att'}->{"xml:id"};
	$copnt = $copnt->{'att'}->{"gid"};
	fsloop: for $fs ($fstructures->descendants("f_structure")) {
	    $v=$fs;
	    next if !hasPred($fs);
	    for ($fs->children("attribute")) {
		if ($_->{'att'}->{"name"} eq "aij") {
		    for ($_->children("value")) {
			if ($_->{'att'}->{"xml:idref"} eq $copntid) {
			  last fsloop;
			}
		    }
		}
	    }
	}
	if ($v) {
	  for my $type ("Suj","SujC") {
	    $suj = getSubStruct($v,$type);
	    if ($suj && hasPred($suj)) {
	      $suj = getAnchorTerm($suj);
	      next unless $suj;
	      $sujgroup = getClosestAncestor($suj,"Easy_");
	      if ($sujgroup && $sujgroup->{'att'}->{"name"} !~ /^Easy_(00|NV)/) {
		$suj = $sujgroup->{'att'}->{"gid"};
	      } else {
		$suj = getAnchor($suj);
	      }
	      if ($suj && $copnt) {
		print_relation("SUJ-V","sujet","verbe",$suj,$copnt);
	      }
	    }
	    $suj = undef;
	  }
	}
    }
}

sub print_AUX_V {
    my $aux1 = undef;
    my $aux2 = undef;
    my $v = undef;
    my $nv;
    for $aux1 ($cstructure->descendants("node")) {
	next if $aux1->{'att'}->{"name"} !~ /Easy_NVA[1U]/;
	$nv = $aux1->parent;
	for ($nv->descendants("node")) {
	    if ($_->{'att'}->{"name"} =~ /^Easy_NVA[1U]/) {
		$aux1 = $_;
	    } elsif ($_->{'att'}->{"name"} =~ /^Easy_NVAn/) {
		$aux2 = $_;
	    } elsif ($_->{'att'}->{"name"} =~ /^Easy_NV/) {
		$v = $_;
	    }
	}
	if ($v) {
	    if ($aux2) {
		print_relation("AUX-V","auxiliaire","verbe",getLastToken($aux1),getLastToken($aux2));
		print_relation("AUX-V","auxiliaire","verbe",getLastToken($aux2),getLastToken($v));
	    } else {
		print_relation("AUX-V","auxiliaire","verbe",getLastToken($aux1),getLastToken($v));
	    }
	}
    }
}

sub print_COD_V {
    my ($objterm,$obj);
    my $v;
    my $objgroup;
    for $v ($fstructures->descendants("f_structure")) {
	next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
	$obj = getSubStruct($v,"Obj");
	if ($obj && hasPred($obj)) {
	    $v = getAnchorTerm($v);
	    next if (!$v);
	    $v = getClosestAncestor($v,"Easy_");
	    next if (!$v);
	    $v = $v->{'att'}->{"gid"};
	    next if (!$v);
	    $objterm = getAnchorTerm($obj);
	    next unless $objterm;
	    $objgroup = getClosestAncestor($objterm,"Easy_");
	    if ($objgroup && $objgroup->{'att'}->{"name"} !~ /^Easy_(00|NV)/) {
		$obj = $objgroup->{'att'}->{"gid"};
	    } else {
		$obj = getAnchor($obj);
	    }
	    if ($obj && $v) {
	      if ($objgroup->{'att'}->{"name"} =~ /^Easy_PV/
		 && $objgroup->{'att'}->{"name"} !~ /^Easy_PV.*INF_DE_/) {
		print_relation("CPL-V","complement","verbe",$obj,$v);		
	      } else {
		print_relation("COD-V","cod","verbe",$obj,$v);
	      }
	    }
	}
	$obj = undef;
    }
}

sub print_CPL_V {
  my ($obj,$objterm);
  my ($v, $origv);
  my $objgroup;
  for $v ($fstructures->descendants("f_structure")) {
    next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
    my $v_not_computed=1;
    $origv = $v;
    for my $type ("Obj�","Objde","Loc","Dloc","Obl","Obl2") {
      $obj = getSubStruct($origv,$type);
      if ($obj && hasPred($obj)) {
	if ($v_not_computed) {
	  $v = getAnchorTerm($v);
	  next if (!$v);
	  $v = getClosestAncestor($v,"Easy_");
	  next if (!$v);
	  $v = $v->{'att'}->{"gid"};
	  $v_not_computed=0;
	}
	next if (!$v);
	$objterm = getAnchorTerm($obj);
	next unless $objterm;
	$objgroup = getClosestAncestor($objterm,"Easy_");
	if ($objgroup && $objgroup->{'att'}->{"name"} !~ /^Easy_NV/) {
	  $obj = $objgroup->{'att'}->{"gid"};
	} else {
	  $obj = getAnchor($obj);
	}
	if ($obj && $v) {
	  print_relation("CPL-V","complement","verbe",$obj,$v);
	}
      }
      $obj = undef;
    }
  }
}

sub print_MOD_V {
    my ($mod,$modterm);
    my ($v,$origv);
    my $modgroup;
    for $v ($fstructures->descendants("f_structure")) {
	next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
	$origv=$v;
	for $mod (getSubStructsList($v,"adjunct")) {
	    $v=$origv;
	    if ($mod && hasPred($mod)) {
		$v = getAnchorTerm($v);
		next if (!$v);
		$v = getClosestAncestor($v,"Easy_");
		next if (!$v);
		$v = $v->{'att'}->{"gid"};
		next if (!$v);
		$modterm = getAnchorTerm($mod);
		next unless $modterm;
		$modgroup = getClosestAncestor($modterm,"Easy_");
		if ($modgroup && $modgroup->{'att'}->{"name"} !~ /^Easy_(NV|00)/) {
		    $mod = $modgroup->{'att'}->{"gid"};
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $v) {
		    if ($modgroup && ($modgroup->{'att'}->{"gtype"} eq "GP" || $modgroup->{'att'}->{"gtype"} eq "GP")) {
			print_relation("CPL-V","complement","verbe",$mod,$v);
		    } else {
			print_relation("MOD-V","modifieur","verbe",$mod,$v);
		    }
		}
	    }
	}
	$mod = undef;
    }
}

sub print_MOD_N {
  my ($mod,$modterm);
  my ($nom,$orignom);
  my $modgroup;
  for $nom ($fstructures->descendants("f_structure")) {
     next if !hasPred($nom) || getAtomicVal($nom,"cat") !~ /^n/;
     $orignom=$nom;
     for $mod (getSubStructsList($nom,"adjunct")) {
       next unless $mod;
       next unless getAnchorTerm($mod);
       next if getAnchorTerm($mod)->{'att'}->{"name"} =~ /^pro/;
      $nom=$orignom;
      if ($mod && hasPred($mod)) {
	$nom = getAnchorTerm($nom);
	next if (!$nom);
	$nom = getClosestAncestor($nom,"Easy_");
	next if (!$nom);
	$nom = $nom->{'att'}->{"gid"};
	next if (!$nom);
	$modterm = getAnchorTerm($mod);
	next unless $modterm;
	$modgroup = getClosestAncestor($modterm,"Easy_");
	if ($modgroup && $nom ne $modgroup->{'att'}->{"gid"}) {
	  $mod = $modgroup->{'att'}->{"gid"};
	} else {
	  $mod = getAnchor($mod);
	}
	if ($mod && $nom) {
	  print_relation("MOD-N","modifieur","nom",$mod,$nom);
	}
      }
     }
     $mod = undef;
  }
}

sub print_MOD_A {
    my ($mod,$modterm);
    my ($adj,$origadj);
    my $modgroup;
    for $adj ($fstructures->descendants("f_structure")) {
	next if !hasPred($adj) || getAtomicVal($adj,"cat") !~ /^adj/;
	$origadj=$adj;
	for $mod (getSubStructsList($adj,"adjunct")) {
	    $adj=$origadj;
	    if ($mod && hasPred($mod)) {
		$adj = getAnchorTerm($adj);
		next if (!$adj);
		$adj = getClosestAncestor($adj,"Easy_");
		next if (!$adj);
		$adj = $adj->{'att'}->{"gid"};
		next if (!$adj);
		$modterm = getAnchorTerm($mod);
		next unless $modterm;
		$modgroup = getClosestAncestor($modterm,"Easy_");
		if ($modgroup && $adj ne $modgroup->{'att'}->{"gid"}) {
		    $mod = $modgroup->{'att'}->{"gid"};
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $adj) {
		    print_relation("MOD-A","modifieur","adjectif",$mod,$adj);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_MOD_R {
    my ($mod,$modterm);
    my ($adv,$origadv);
    my $modgroup;
    for $adv ($fstructures->descendants("f_structure")) {
	next if !hasPred($adv) || getAtomicVal($adv,"cat") !~ /^adv/;
	$origadv=$adv;
	for $mod (getSubStructsList($adv,"adjunct")) {
	    $adv=$origadv;
	    if ($mod && hasPred($mod)) {
		$adv = getAnchorTerm($adv);
		next if (!$adv);
		$adv = getClosestAncestor($adv,"Easy_")->{'att'}->{"gid"};
		next if (!$adv);
		$modterm = getAnchorTerm($mod);
		next unless $modterm;
		$modgroup = getClosestAncestor($modterm,"Easy_");
		if ($modgroup) {
		    $mod = $modgroup->{'att'}->{"gid"};
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $adv) {
		    print_relation("MOD-R","modifieur","adverbe",$mod,$adv);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_MOD_P {
    my ($mod,$modterm);
    my ($prep,$origprep);
    my $modgroup;
    for $prep ($fstructures->descendants("f_structure")) {
	next if !hasPred($prep) || getAtomicVal($prep,"cat") !~ /^prep/;
	$origprep=$prep;
	for $mod (getSubStructsList($prep,"adjunct")) {
	    $prep=$origprep;
	    if ($mod && hasPred($mod)) {
		$prep = getAnchorTerm($prep);
		next if (!$prep);
		$prep = getClosestAncestor($prep,"Easy_")->{'att'}->{"gid"};
		next if (!$prep);
		$modterm = getAnchorTerm($mod);
		next unless $modterm;
		$modgroup = getClosestAncestor($modterm,"Easy_");
		if ($modgroup) {
		    $mod = $modgroup->{'att'}->{"gid"};
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $prep) {
		    print_relation("MOD-P","modifieur","preposition",$mod,$prep);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_COMP {
    my ($prop,$csuform);
    my$prophead = undef;
    for $prop ($cstructure->descendants("node")) {
	next unless $prop->{'att'}->{"name"} =~ /^Easy_00(que|csu)/;
	$csuform = $prop;
	$csuform = ($csuform->descendants("F"))[-1];
	last unless ($csuform);
	$csuform = $csuform->{'att'}->{"xml:id"};
	$prop = $prop->parent->{'att'}->{"xml:id"};
	for my $v ($fstructures->descendants("f_structure")) {
	    next if !hasPred($v);
	    for ($v->children("attribute")) {
		if ($_->{'att'}->{"name"} eq "aij") {
		    for ($_->children("value")) {
			if ($_->{'att'}->{"xml:idref"} eq $prop) {
			    $prophead = getAnchorTerm($v);
			    next if (!$prophead);
			    $prophead = getClosestAncestor($prophead,"Easy_")->{'att'}->{"gid"};
			    last;
			}
		    }
		}
	    }
	}
	if ($csuform && $prophead) {
	    print_relation("COMP","complementeur","verbe",$csuform,$prophead);
	}
    }
}

sub print_ATB_SO {
  my $att;
  my $v;
  for $v ($fstructures->descendants("f_structure")) {
    next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
    $att = getSubStruct($v,"Att");
    if ($att && hasPred($att)) {
      $v = getAnchorTerm($v);
      next unless $v;
      $v = getClosestAncestor($v,"Easy_");
      next unless $v;
      $v = $v->{'att'}->{"gid"};
      next unless $v;
      $att = getAnchorTerm($att);
      next unless $att;
      $att = getClosestAncestor($att,"Easy_");
      next unless $att;
      if ($att !~ /^Easy_00/) {
	$att = $att->{'att'}->{"gid"};
      } else {
	$att = getAnchor($att);
      }
      if ($att && $v) {
	print_relation("ATB-SO","attribut","verbe",$att,$v,"    <s-o valeur=\"sujet\"/>");
      }
    }
    $att = undef;
  }
  # constructions � copule: on r�cup�re la relation sujet-copule
  my ($att, $attgroup, $copnt, $copntid, $fs);
  for $copnt ($cstructure->descendants("node")) {
    next if $copnt->{'att'}->{"name"} !~ /Easy_NVA[1U]copule/;
    $copnt = $copnt->{'att'}->{"gid"};
    $att = node2fs($copnt);
    if ($att && hasPred($att)) {
      $att = getAnchorTerm($att);
      next unless $att;
      $attgroup = getClosestAncestor($att,"Easy_");
      if ($attgroup && $attgroup->{'att'}->{"name"} !~ /^Easy_NV/) {
	$att = $attgroup->{'att'}->{"gid"};
      } else {
	$att = getAnchor($att);
      }
      if ($att && $copnt) {
	print_relation("ATB-SO","attribut","verbe",$att,$copnt,"    <s-o valeur=\"sujet\"/>");
      }
    }
    $att = undef;
  }
}

sub print_COORD {
  my @arg;
  my $coo;
  for $coo ($fstructures->descendants("f_structure")) {
    next if !hasPred($coo) || getAtomicVal($coo,"cat") ne "coo";
    $arg[1]=$arg[2]=undef;
    for my $i (1,2) {
      $arg[$i] = getSubStruct($coo,"arg$i");
      if ($arg[$i] && hasPred($arg[$i])) {
	$arg[$i] = getAnchorTerm($arg[$i]);
	next unless $arg[$i];
	$arg[$i] = getClosestAncestor($arg[$i],"Easy_");
	if ($arg[$i]) {
	  $arg[$i] = $arg[$i]->{'att'}->{"gid"};
	}
      }
    }
    if ($arg[2]) {
      #	    print "coo=$coo\n";
      $coo = getAnchor($coo);
      next if (!$coo);
      if ($arg[1]) {
	print_relation_3("COORD","coordonnant","coord-g","coord-d",$coo,$arg[1],$arg[2]);
      } else {
	print_relation("COORD","coordonnant","coord-d",$coo,$arg[2]);
      }
    }
  }
}

sub print_APPOS {
    my $nom = undef;
    my $nomAppos = undef;
    for $nomAppos ($cstructure->descendants("node")) {
	next if $nomAppos->{'att'}->{"name"} ne "SN_appos";
	$nom = $nomAppos->parent;
	while ($nom->{'att'}->{"name"} =~ /appos/) {
	  $nom = $nom->parent;
	}
	next unless ($nom);
	$nom = node2fs($nom);
	next unless ($nom);
	$nom = getAnchorTerm($nom);
	next unless ($nom);
	$nom = getClosestAncestor($nom,"Easy_")->{'att'}->{"gid"};
	next unless ($nom);
	$nomAppos = node2fs($nomAppos);
	next unless ($nomAppos);
	$nomAppos = getAnchorTerm($nomAppos);
	next unless ($nomAppos);
	$nomAppos = getClosestAncestor($nomAppos,"Easy_")->{'att'}->{"gid"};
	next unless ($nomAppos);	
	print_relation("APPOS","premier","appose",$nom,$nomAppos);
    }
    my $mod;
    my $orignom;
    my $modgroup;
    for $nom ($fstructures->descendants("f_structure")) {
	next if !hasPred($nom) || getAtomicVal($nom,"cat") !~ /^n/;
	$orignom=$nom;
	for $mod (getSubStructsList($nom,"adjunct")) {
	  next unless $mod;
	  next unless getAnchorTerm($mod);
	  next if getAnchorTerm($mod)->{'att'}->{"name"} !~ /^pro/;
	    $nom=$orignom;
	    if ($mod && hasPred($mod)) {
		$nom = getAnchorTerm($nom);
		next if (!$nom);
		$nom = getClosestAncestor($nom,"Easy_");
		next if (!$nom);
		$nom = $nom->{'att'}->{"gid"};
		next if (!$nom);
		$mod = getAnchorTerm($mod);
		next unless $mod;
		$modgroup = getClosestAncestor($mod,"Easy_");
		if ($modgroup && $nom ne $modgroup->{'att'}->{"gid"}) {
		    $mod = $modgroup->{'att'}->{"gid"};
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $nom) {
		    print_relation("APPOS","premier","appose",$nom,$mod);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_JUXT {
    my @arg;
    my $coo;
    # Easy_JUXT2
    for $coo ($cstructure->descendants("node")) {
	next if $coo->{'att'}->{"name"} ne "Easy_JUXT2";
	$arg[1]=undef;
	$arg[2]=undef;
	for ($coo->children("node")) {
	    if ($_->{'att'}->{"name"} eq "PHRASE") {
		$arg[2]=$_;
	    } elsif ($_->{'att'}->{"name"} eq "ENONCES") {
		$arg[1]=($_->children("node"))[0];
		if ($arg[1] && $arg[1]->{'att'}->{"name"} eq "Easy_JUXT2b") {
		    for ($arg[1]->children("node")) {
			if ($_->{'att'}->{"name"} eq "PHRASE") {
			    $arg[1]=$_;
			    last;
			}
		    }	    
		}
	    }
	}
	if ($arg[1] && $arg[2]) {
	    for my $i (1,2) {
		$arg[$i]=$arg[$i]->{'att'}->{"xml:id"};
		for my $v ($fstructures->descendants("f_structure")) {
		    next if !hasPred($v);
		    for ($v->children("attribute")) {
			if ($_->{'att'}->{"name"} eq "aij") {
			    for ($_->children("value")) {
				if ($_->{'att'}->{"xml:idref"} eq $arg[$i]) {
				    $arg[$i] = getAnchorTerm($v);
				    next if (!$arg[$i]);
				    $arg[$i] = getClosestAncestor($arg[$i],"Easy_");
				    if ($arg[$i] && $arg[$i] !~ /^Easy_00/) {
				      $arg[$i] = $arg[$i]->{'att'}->{"gid"};
				    } else {
				      $arg[$i] = getAnchor($arg[$i]);
				    }
				    last;
				}
			    }
			}
		    }
		}
	    }
	}
	if ($arg[1] && $arg[2]) {
	    print_relation("JUXT","premier","suivant",$arg[1],$arg[2]);
	}
    }
    # Easy_JUXT2b
    juxt2: for $coo ($cstructure->descendants("node")) {
	next if $coo->{'att'}->{"name"} ne "Easy_JUXT2b";
	$arg[1]=undef;
	$arg[2]=undef;
	for ($coo->children("node")) {
	    if ($_->{'att'}->{"name"} eq "S3") {
		$arg[2]=$_;
	    } elsif ($_->{'att'}->{"name"} eq "S4") {
		$arg[1]=$_;
		my $tmp = ($_->children("node"))[0];
		if ($tmp && $tmp->{'att'}->{"name"} eq "Easy_JUXT2b") {
		    $arg[1]=undef;
		    for ($tmp->children("node")) {
			if ($_->{'att'}->{"name"} eq "S3") {
			    $arg[1]=$_;
			    last;
			}
		    }	    
		}
	    }
	}
	if ($arg[1] && $arg[2]) {
	    for my $i (1,2) {
		$arg[$i]=$arg[$i]->{'att'}->{"xml:id"};
		arg: for my $v ($fstructures->descendants("f_structure")) {
		    next if !hasPred($v);
		    for ($v->children("attribute")) {
			if ($_->{'att'}->{"name"} eq "aij") {
			    for ($_->children("value")) {
				if ($_->{'att'}->{"xml:idref"} eq $arg[$i]) {
				    $arg[$i] = getAnchorTerm($v);
				    next juxt2 if (!$arg[$i]);
				    if (getClosestAncestor($arg[$i],"Easy_")) {
				      $arg[$i] = getClosestAncestor($arg[$i],"Easy_")->{'att'}->{"gid"};
				    } else {
				      $arg[$i] = getAnchor($arg[$i]);
				    }
				    last arg;
				}
			    }
			}
		    }
		}
		next juxt2 if ($arg[$i] =~ /^R/);
	    }
	}
	if ($arg[1] && $arg[2]) {
	    if ($arg[1]!~/^R/ && $arg[2]!~/^R/) {
		print_relation("JUXT","premier","suivant",$arg[1],$arg[2]);
	    } else {
		print STDERR join("\n\t","JUXT","premier","suivant",$arg[1],$arg[2])."\n";
	    }
	}
    }
}



sub getSubStruct {
    my $fs = shift;
    my $sfs_name = shift;
    my $ptr;
    for $ptr ($fs->descendants("attribute")) {
	if ($ptr->{'att'}->{"type"} eq "struct" && $ptr->{'att'}->{"name"} eq $sfs_name) {
	    $ptr = ($ptr->descendants("value"))[0];
	    $ptr = $ptr->{'att'}->{"xml:idref"};
	    return ($parseroot->descendants("[\@xml:id=\"$ptr\"]"))[0];
	}
    }
    return undef;
}

sub getSubStructsList {
    my $fs = shift;
    my $sfs_name = shift;
    my $ptr;
    my @answer=();
    for $ptr ($fs->descendants("attribute")) {
	if ($ptr->{'att'}->{"type"} eq "set" && $ptr->{'att'}->{"name"} eq $sfs_name) {
	    for my $ptr2 ($ptr->descendants("value")) {
		$ptr2 = $ptr2->{'att'}->{"xml:idref"};
		push(@answer,($parseroot->descendants("[\@xml:id=\"$ptr2\"]"))[0]);
	    }
	}
    }
    return @answer;
}

sub getAtomicVal {
    my $fs = shift;
    my $at_name = shift;
    my $ptr;
    for $ptr ($fs->descendants("attribute")) {
	if ($ptr->{'att'}->{"type"} eq "atom" && $ptr->{'att'}->{"name"} eq $at_name) {
	    return undef if $fs == undef;
	    $ptr = ($ptr->descendants("value"))[0];
	    return undef if $fs == undef;
	    $ptr = $ptr->{'att'}->{"atomic_val"};
	    return $ptr;
	}
    }
    return undef;
}

sub getAnchor {
    my $fs = shift;
    $fs = ($fs->descendants("pred"))[0];
    return undef if $fs == undef;
    $fs = ($fs->descendants("anchor"))[0];
    return undef if $fs == undef;
    $fs = $fs->{'att'}->{"xml:idref"};
    return $tid2fid{$fs};
}

sub getAnchorTerm {
    my $fs = shift;
    $fs = ($fs->descendants("pred"))[0];
    return undef if $fs == undef;
    $fs = ($fs->descendants("anchor"))[0];
    return undef if $fs == undef;
    $fs = $fs->{'att'}->{"xml:idref"};
    return ($parseroot->descendants("[\@xml:id=\"$fs\"]"))[0];
}

sub getLastToken {
    my $node = shift;
    my $tid = "";
    for ($node->descendants("F")) {
	if ($_->{'att'}->{"xml:id"}) {
	    $tid = $_->{'att'}->{"xml:id"};
	} else {
	    $tid = $_->{'att'}->{"xml:idref"};
	}
    }
    return $tid;
}

sub hasPred {
    my $fs = shift;
    my $ptr = $fs->first_child("pred");
    return !($ptr == undef);
}

sub getClosestAncestor {
    my $node = shift;
    my $nameStart = shift;
    return undef unless ($node);
    if ($node eq "Easy_") {
      my ($package, $filename, $line) = caller;
      print STDERR "Warning: error in getClosestAncestor, \$node = $node ; \$nameStart = $nameStart ; caller = $filename:$line\n";
      return undef;
    }
    return ($node->ancestors("\@name=~/^$nameStart/"))[0];
    return undef;
}

my %print_relation_cache;

sub print_relation {
    my $type = shift;
    my $name1 = shift;
    my $name2 = shift;
    my $href1 = shift;
    my $href2 = shift;
    my $flag = shift;
    return if ($href1 eq "" || $href2 eq "");
    if (!defined($print_relation_cache{$type}{$href1."\t".$href2."\t".$flag})) {
	$rid++;
	print "  <relation xlink:type=\"extended\" type=\"$type\" id=\"E${eid}R$rid\">\n";
	print "    <$name1 xlink:type=\"locator\" xlink:href=\"$href1\"/>\n";
	print "    <$name2 xlink:type=\"locator\" xlink:href=\"$href2\"/>\n";
	if ($flag) {
	    print $flag."\n";
	}
	print "  </relation>\n";
    }
}

my %print_relation_3_cache;

sub print_relation_3 {
    my $type = shift;
    my $name1 = shift;
    my $name2 = shift;
    my $name3 = shift;
    my $href1 = shift;
    my $href2 = shift;
    my $href3 = shift;
    my $flag = shift;
    return if ($href1 eq "" || $href2 eq "" || $href3 eq "");
    if (!defined($print_relation_3_cache{$type}{$href1."\t".$href2."\t".$href3."\t".$flag})) {
	$rid++;
	print "  <relation xlink:type=\"extended\" type=\"$type\" id=\"E${eid}R$rid\">\n";
	print "    <$name1 xlink:type=\"locator\" xlink:href=\"$href1\"/>\n";
	print "    <$name2 xlink:type=\"locator\" xlink:href=\"$href2\"/>\n";
	print "    <$name3 xlink:type=\"locator\" xlink:href=\"$href3\"/>\n";
	if ($flag) {
	    print $flag."\n";
	}
	print "  </relation>\n";
    }
}

sub node2fs {
  my $node = shift;
  $node = $node->{'att'}->{"xml:id"};
  my ($ret_val, $fs);
  $fs = undef;
 fsloop: for $fs ($fstructures->descendants("f_structure")) {
    $ret_val=$fs;
    next if !hasPred($fs);
    for ($fs->children("attribute")) {
      if ($_->{'att'}->{"name"} eq "aij") {
	for ($_->children("value")) {
	  if ($_->{'att'}->{"xml:idref"} eq $node) {
	    last fsloop;
	  }
	}
      }
    }
  }
  return $ret_val;
}
