#!/usr/bin/perl

use strict;

my @mp_prod_table;
my @mp_t_table;
my @mp_t_rev_table;
my @mp_nt_table;

while (<>) {
    next if (!/^\*\[([PTN])([0-9]+) ([0-9]+)\]/);
    if ($1 eq "P") {
	$mp_prod_table[$2]=$3;
    } elsif ($1 eq "T") {
	$mp_t_table[$2]=$3;
	$mp_t_rev_table[$3]=$2; # big -> normal (=huge)
    } elsif ($1 eq "N") {
	$mp_nt_table[$2]=$3;
    }
}

&output_table(\@mp_t_table,"mp_t_table");
&output_table(\@mp_t_rev_table,"mp_t_rev_table");
&output_table(\@mp_nt_table,"mp_nt_table");
&output_table(\@mp_prod_table,"mp_prod_table");

sub output_table {
    my $table = shift;
    my $output_name = shift;
    print "#ifdef def_$output_name\n";
    print "static int $output_name [".($#{$table}+1)."] = {";
    for my $i (0..$#{$table}) {
	if ($i % 10 == 0) {
	    print "\n/* $i */";
	}
	if ($table->[$i] == 0) {
	    print " 0,";
	} else {
	    print " $table->[$i],";
	}
    }
    print "\n};\n";
    print "#endif /* def_$output_name */\n";
}
