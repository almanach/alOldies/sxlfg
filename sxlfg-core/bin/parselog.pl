#!/usr/bin/perl

#$us_is_to=(shift || 0);

$output=0;
$doparselog=1;
$TO=20;
$t0=0;

while (1) {
    $_=shift;
    $parse_options.=" ".$_;
    if (/^$/) {last}
    elsif (/^-o$/ || /^--output$/) {$output=1;}
    elsif (/^-rn=(.*)$/) {$runname=$1; $runname=~s/^run_//; $runname="run_".$runname}
    elsif (/^-c=(.*)$/) {$corpusname=$1}
    elsif (/^-cc=(.*)$/) {$corpusclass=$1}
    elsif (/^-tu$/) {$tolerate_unknown_status=1}
    elsif (/^-eat$/) {$exited_abnormally_means_timeout=1}
    elsif (/^-no_pl$/) {$doparselog=0}
    elsif (/^-to?=([0-9\.]+)$/ || /^--timeout=([0-9]+)$/) {$TO=$1;}
    elsif (/^-ts(?:hift)?=([0-9\.]+)$/) {$t0=$1;}
    elsif (/^-m(?:ax|l)=([0-9]+)$/ || /^--maxline=([0-9]+)$/) {$maxline=$1;}
    elsif (/^-h$/ || /^--help/) {
	print STDERR "Usage:        esxanalyse.pl\n";
	print STDERR "Options:\n";
	print STDERR "  -to=[number] --timeout=[number]                 timeout used while parsing (default=20, no timeout=0)\n";
	print STDERR "  -ts(hift)=                                      time to be atted to all logged times\n";
	print STDERR "                                                    (typically half of the time measurement granularity)\n";
	print STDERR "  -max=[number] -ml=[number] --maxline=[number]   take into account only the [number] first parsed sentences\n";
	print STDERR "  -c=corpusname                                   consider only sentences from corpus [corpusname]\n";
	print STDERR "  -cc=corpusclass                                 consider only sentences from corpus  with a name beginning by [corpusclass]_\n";
	print STDERR "  -rn=runname                                     name of the run (folder name is run_[runname])\n";
	print STDERR "  -tu                                             tolerate, and count, sentences with unknown status\n";
	print STDERR "  -ut                                             sentences with unknown status are counted as timeout\n";
	print STDERR "  -h --help                                       print this\n";
	exit(0);
    }
    else {die ("###### ERROR: Unrecognized or mistyped option: $_\n")}
}

$cfgonly=0;

if ($doparselog) {
  open(LOG,"$runname/global_log") || die("$runname/global_log could not be opened\n");
  while (<LOG>) {
    chomp;
    if (/^### CFG ONLY ###$/) {
      $cfgonly=1;
    } elsif (/^parsing sentence ([0-9-]+) of file ([^:]*) : (.*)/) {
      last if ($maxline>0 && $maxline<=$linenumber);
      $n=$2.":".$1;
      $sentence=$3;
      $parsed{$n}=1;
      $firstattemptfailed=0;
      $timeout{$n}=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
      $time{$n}=0;
      $nofs{$n}=0;
      $error{$n}=0;
      $robust{$n}=0;
      $filtered{$n}=0;
      $CFGerror{$n}=0;
      $caughterror{$n}=0;
      $uncaughterror{$n}=0;
      $temp=$sentence;
      $size{$n}=0;
      while ($temp=~s/^[^\{]*\{//) {
	$size{$n}++;
      }
      $linenumber++;
    } elsif (/filter mode/) {
      $firstattemptfailed=0;
      $timeout{$n}=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
      $nofs{$n}=0;
      $error{$n}=0;
      $CFGerror{$n}=0;
      $caughterror{$n}=0;
      $uncaughterror{$n}=0;
      $filter{$n}=1;
      $robust{$n}=0;
   } elsif (/robust mode/) {
      $firstattemptfailed=0;
      $timeout{$n}=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
      $nofs{$n}=0;
      $error{$n}=0;
      $CFGerror{$n}=0;
      $caughterror{$n}=0;
      $uncaughterror{$n}=0;
      $filter{$n}=0;
      $robust{$n}=1;
   } elsif (/standard mode/) {
      $firstattemptfailed=0;
      $timeout{$n}=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
      $nofs{$n}=0;
      $error{$n}=0;
      $CFGerror{$n}=0;
      $caughterror{$n}=0;
      $uncaughterror{$n}=0;
      $filter{$n}=0;
      $robust{$n}=0;
    } elsif (/number of trees in the shared forest :\t([0-9e\+\.]+)/) {
      $trees{$n}=$1;
    } elsif (/CFG data \(N T P outpuG TreeCount\) :\t([0-9e\+\.]+)\t([0-9e\+\.]+)\t([0-9e\+\.]+)\t([0-9e\+\.]+)\t([0-9e\+\.]+|inf)/) {
      $outpuG{$n}=$4;
      $trees{$n}=$5;
      if ($trees{$n} eq "inf") {
	$trees{$n}=1e999;
      }
      if ($outpuG{$n}==0 || $outpuG{$n} eq "") {
	print STDERR "For�t vide!? $n\n";
      }
    } elsif (/CFG data \(N T P outpuG TreeCount\) :/) {
      print STDERR "could not parse CFG data for sentence $n\n";
    } elsif (/parsing timed out/) {
      $timeout{$n}=1;
      $cons{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
      $time{$n}+=1000*$TO;
    } elsif (/parsing attempt timed out/) {
      $time{$n}+=1000*$TO;
    } elsif (/parsing time \(s\) :\t([0-9\.]+)/) {
      if ($TO==0 || $1 <= $TO) {
	$time{$n}+=1000*$1+$t0;
      } else {
	$timeout{$n}=1;
	$cons{$n}=0;
	$uncons_r{$n}=0;
	$uncons_g{$n}=0;
	$partial{$n}=0;
	$time{$n}+=1000*$TO;
      }
    } elsif (/parsing attempt time \(s\) :\t([0-9\.]+)/) {
      if ($TO==0 || $1 <= $TO) {
	$time{$n}+=1000*$1+$t0;
      } else {
	$time{$n}+=1000*$TO;
      }
    } elsif (/no first attempt f_structure \(step [2468]\)/) {
      $firstattemptfailed=1;
    } elsif (/[1-9][0-9]* consistent f_structure \(step [2468]\)/) {
      $firstattemptfailed=0;
      $CFGrec{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
      $nofs{$n}=0;
      $cons{$n}=1;
    } elsif (/[0-9]+ unconsistent f_structures? \(step [2468]\)/) {
      $firstattemptfailed=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $partial{$n}=0;
      $nofs{$n}=0;
      if ($firstattemptfailed) {
	$uncons_g{$n}=1;
	$uncons_r{$n}=0;
      } else {
	$uncons_r{$n}=1;
	$uncons_g{$n}=0;
      }
    } elsif (/0 partial f_structures/) {
      $firstattemptfailed=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $partial{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $nofs{$n}=1;
    } elsif (/\t([0-9]+) partial f_structures? \(step [2468]\)/) {
      $firstattemptfailed=0;
      $cons{$n}=0;
      $CFGrec{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $nofs{$n}=0;
      $partial{$n}=$1;
    } elsif (/parsing needed CFG error recovery/) {
      $CFGrec{$n}=1;
    } elsif (/parsing failed \(probably while processing CFG error recovery\)/) {
      $error{$n}=1;
      $CFGerror{$n}=1;
      $cons{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
    } elsif (/SXLFG CAUGHT ERROR/ && $_!~/non-zero status [25]\"/) {
      $error{$n}=1;
      $caughterror{$n}=1;
      $cons{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
    } elsif (/SXLFG UNCAUGHT ERROR/ || (/parsing exited abnormally/ && !$exited_abnormally_means_timeout)) {
      $error{$n}=1;
      $uncaughterror{$n}=1;
      $cons{$n}=0;
      $uncons_r{$n}=0;
      $uncons_g{$n}=0;
      $partial{$n}=0;
    } elsif (!$timeout{$n} && $exited_abnormally_means_timeout && /parsing exited abnormally/) {
	$timeout{$n}=1;
	$cons{$n}=0;
	$uncons_r{$n}=0;
	$uncons_g{$n}=0;
	$partial{$n}=0;
	$time{$n}+=1000*$TO;
    }
  }

  for $n (keys %parsed) {
      if (($corpusname eq "" && $corpusclass eq "") || $n=~/^$corpusname\:/ || $n=~/^$corpusclass\_/) {
	  $time[0]+=$time{$n};
	  $CFGrec[0]+=$CFGrec{$n};
	  $CFGrec_cons+=$CFGrec{$n}*$cons{$n};
	  $CFGerror[0]+=$CFGerror{$n};
	  $caughterror[0]+=$caughterror{$n};
	  $uncaughterror[0]+=$uncaughterror{$n};
	  $timeoutnumber+=$timeout{$n};
	  $sentencenumber++;
	  if (!$cfgonly) {
	      $cons[0]+=$cons{$n};
	      $uncons_r[0]+=$uncons_r{$n};
	      $uncons_g[0]+=$uncons_g{$n};
	      $partial[0]+=($partial{$n}>0);
	      $nofs[0]+=$nofs{$n};
	      $dummy{$n}=$cons{$n}+$uncons_r{$n}+$uncons_g{$n}+($partial{$n}>0)+$CFGerror{$n}+$caughterror{$n}+$uncaughterror{$n}+$timeout{$n}+$nofs{$n};
	      if ($dummy{$n}==0) {
		  if ($unknown_status_means_timeout) {
		      $timeoutnumber+=1;
		      $timeout{$n}=1;
		      $time{$n}=1000*$TO;			
		      $time[0]+=$time{$n};
		  } elsif ($tolerate_unknown_status) {
		      $unknown[0]++;
		  } else {
		      print STDERR "unknown status for sentence $n\n";
		  }
	      }
	  }
      }
  }
}

  print STDERR "TOTAL\t$sentencenumber sent.\n";
  print STDERR "TIMEOUT\t$TO s\n";
  print STDERR "\n";
  print STDERR "CFG ok\t".($sentencenumber-$CFGrec[0]-$CFGerror[0])."\t".(100*($sentencenumber-$CFGrec[0]-$CFGerror[0])/$sentencenumber)." %\n";
  print STDERR "CFG rec\t".($CFGrec[0])."\n";
  print STDERR "CFG err\t".($CFGerror[0])."\n";
  print STDERR "\n";
  if (!$cfgonly) {
      print STDERR "cons\t".($cons[0])."\t".(100*$cons[0]/$sentencenumber)." %\n";
      print STDERR "\tCFG rec\t".($CFGrec_cons)."\t".(100*$CFGrec_cons/$sentencenumber)." %";
      if ($CFGrec[0]) {
	  "\t(".(100*$CFGrec_cons/$CFGrec[0])."% of CFG-rcvr)";
      }
      print STDERR "\n";
      if (!$cfgonly) {
	  print STDERR "inconsR\t".($uncons_r[0])."\n";
	  print STDERR "inconsG\t".($uncons_g[0])."\n";
	  print STDERR "partial\t".($partial[0])."\n";
	  print STDERR "no fs\t".($nofs[0])."\n";
	  if ($tolerate_unknown_status) {
	      print STDERR "unknown\t".($unknown[0])."\n";
	  }
	  print STDERR "\n";
	  print STDERR "filter\t".($filter[0])."\t".(100*$filter[0]/$sentencenumber)." %\n";
	  print STDERR "standard\t".($sentencenumber-$robust[0]-$filter[0])."\t".(100-100*$robust[0]/$sentencenumber-100*$filter[0]/$sentencenumber)." %\n";
	  print STDERR "robust\t".($robust[0])."\t".(100*$robust[0]/$sentencenumber)." %\n";
	  print STDERR "\n";
      }
      print STDERR "timeout\t".($timeoutnumber)."\t".(100*$timeoutnumber/$sentencenumber)." %\n";
      if (!$cfgonly) {
	  print STDERR "CFG err\t".($CFGerror[0])."\n";
	  print STDERR "fserr c\t".($caughterror[0])."\n";
	  print STDERR "fserr u\t".($uncaughterror[0])."\n";
      }
      print STDERR "\n";
      print STDERR "total time\t".($time[0]/1000)."\n";
      close(LOG);
  }
  
  if ($output) {
      if ($doparselog) {
	  open(CFGSIZEAMB,">$runname/cfg-sizeamb") || die("$runname/cfg-sizeamb could not be opened\n");
	  if ($cfgonly) {
	      open(CFGSIZETIME,">$runname/cfg-sizetime") || die("$runname/cfg-sizetime could not be opened\n");
	  } else {
	      open(FSTREESTIME,">$runname/fs-treestime") || die("$runname/fs-treestime could not be opened\n");
	      open(FSOUTPUGTIME,">$runname/fs-outpuGtime") || die("$runname/fs-outpuGtime could not be opened\n");
	      open(FSSIZETIME,">$runname/fs-sizetime") || die("$runname/fs-sizetime could not be opened\n");
	  }
	  $i=0;
	  for $n (keys %parsed) {
	      next if ($time{$n} == 0 && $t0>0);
	      if ($corpusname eq "" || $n=~/^$corpusname\:/) {
		  $i++;
		  if ($i % 100 == 0) {
		      print STDERR "$i\r";
		  }
		  if (!$error{$n}) {
		      #	print  "$outpuG{$n}\t".($time{$n}+5)."\n";
		      if ($trees{$n}>0) {
			  print CFGSIZEAMB "$size{$n}\t$trees{$n}\n";
		      }
		      if ($cfgonly) {
			  print CFGSIZETIME "$size{$n}\t".($time{$n})."\n";
		      } else {
			  print FSTREESTIME "$trees{$n}\t".($time{$n})."\n";
			  print FSOUTPUGTIME "$outpuG{$n}\t".($time{$n})."\n";
			  print FSSIZETIME "$size{$n}\t".($time{$n})."\n";
		      }
		      #	print "$trees{$n}\t".($time{$n})."\n";
		      #	print "$size{$n}\t$trees{$n}\n";
		      #	print "$size{$n}\t".($time{$n})."\n";
		      #	print "$outpuG{$n}\t".($time{$n})."\t$trees{$n}\t$size{$n}\t".($outpuG{$n})."\t".($trees{$n}**(1/$size{$n}))."\n";
		      #	if ($cons{$n}) {
		      #	    print "./dags/general_lemonde.txt.dag:$n\n";
		      #	}
		  }
	      }
	  }
	  close(CFGSIZEAMB);
	  if ($cfgonly) {
	      close(CFGSIZETIME);
	  } else {
	      close(FSTREESTIME);
	      close(FSOUTPUGTIME);
	      close(FSSIZETIME);
	  }
      }
      system("cut -f1 $runname/cfg-sizeamb | sort | uniq -c | perl -pe 's/^\\s*([0-9]+)\\s+([0-9]+)/\\2\\t\\1/' | sort -n > $runname/lengths");
      system("rm -f $runname/plot_*");
      system("cd $runname && ln -s ../stats/plot_lengths plot_lengths && gnuplot plot_lengths > plot_lengths.ps");
      system("cd $runname && ln -s ../stats/plot_lengths-en plot_lengths-en && gnuplot plot_lengths-en > plot_lengths-en.ps");
      system("cat $runname/cfg-sizeamb | perl data2med.pl 5 > $runname/cfg-amb");
      system("cd $runname && ln -s ../stats/plot_cfg-amb plot_cfg-amb && gnuplot plot_cfg-amb > plot_cfg-amb.ps");
      system("cd $runname && ln -s ../stats/plot_cfg-amb-en plot_cfg-amb-en && gnuplot plot_cfg-amb-en > plot_cfg-amb-en.ps");
      if ($cfgonly) {
	  system("cat $runname/cfg-sizetime | perl data2med.pl 5 > $runname/cfg-time");
	  system("ln -s ../stats/plot_cfg-time $runname/plot_cfg-time");
	  system("ln -s ../stats/plot_cfg-time-en $runname/plot_cfg-time-en");
	  system("cd $runname && gnuplot plot_cfg-time > plot_cfg-time.ps");
	  system("cd $runname && gnuplot plot_cfg-time-en > plot_cfg-time-en.ps");
      } else {
	  system("cat $runname/fs-treestime | perl data2med.pl -1 > $runname/fs-time");
	  system("cat $runname/fs-outpuGtime | perl data2med.pl -0.1  > $runname/fs-time2");
	  system("cat $runname/fs-sizetime | perl data2med.pl -0.05  > $runname/fs-time3");
	  system("cd $runname && ln -s ../stats/plot_fs-time plot_fs-time && gnuplot plot_fs-time >  plot_fs-time.ps");
	  system("cd $runname && ln -s ../stats/plot_fs-time2 plot_fs-time2 && gnuplot plot_fs-time2 > plot_fs-time2.ps");
	  system("cd $runname && ln -s ../stats/plot_fs-time3 plot_fs-time3 && gnuplot plot_fs-time3 > plot_fs-time3.ps");
	  system("cd $runname && ln -s ../stats/plot_fs-time-en plot_fs-time-en && gnuplot plot_fs-time-en >  plot_fs-time-en.ps");
	  system("cd $runname && ln -s ../stats/plot_fs-time2-en plot_fs-time2-en && gnuplot plot_fs-time2-en > plot_fs-time2-en.ps");
	  system("cd $runname && ln -s ../stats/plot_fs-time3-en plot_fs-time3-en && gnuplot plot_fs-time3-en > plot_fs-time3-en.ps");
      }
  }
