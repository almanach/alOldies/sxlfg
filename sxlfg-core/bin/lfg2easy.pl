#!/usr/bin/perl

use XML::LibXML;

use strict;

my $xmlparsedata;
my %ids;
my $close_DOCUMENT = 0;
while (<STDIN>) {
  if ($xmlparsedata eq "" && $_ !~ /^<\?xml/) {
    $close_DOCUMENT = 1;
    $xmlparsedata = '<?xml version="1.0" encoding="ISO-8859-1"?>';
    $xmlparsedata .= "<DOCUMENT>";
  }
    for (split(/(?<=>)\s*(?=<)/,$_)) {
	if (/ id=\"([^\"]+)\"/) {
	    if ($ids{$1} == 0) {
		$ids{$1}=1;
		s/ id=/ xml:id=/g;
	    } else {
		s/ id=/ xml:idref=/g;
	    }
	} elsif (/ idref=\"([^\"]+)\"/) {
	    s/ idref=/ xml:idref=/g;
	}
	if (/^<E /) {
	    while (s/(input=\".*?)\"(.*\">)$/\1&quot;\2/) {}
	    while (s/(input=\".*?)<(.*\">)$/\1&lt;\2/) {}
	    while (s/(input=\".*?)>(.*\">)$/\1&gt;\2/) {}
	}
	$xmlparsedata .= $_;
    }
}
if ($close_DOCUMENT) {$xmlparsedata.="</DOCUMENT>";}

my $parser = XML::LibXML->new();
my $xmlparse = $parser->parse_string ($xmlparsedata) || die "Non-XML Parse";

my $parseroot = $xmlparse->getFirstChild; # DOCUMENT
my $parseroot = $parseroot->getFirstChild; # E
while ($parseroot && $parseroot->getName ne "E") {$parseroot = $parseroot->getNextSibling();}
die "\nEmpty Parse" if ($parseroot == undef || $parseroot->nodeName ne "E");

my ($eid,%tid2fid,$gid,$tid,$rid,$fstructures,$cstructure);

#fichier="general_elda.xml" id="reference" date="05/09/2006" 
print <<END;
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DOCUMENT SYSTEM "easy.dtd">
<DOCUMENT xmlns:xlink="http://www.w3.org/1999/xlink">
END

while ($parseroot) {
  $eid = $parseroot->getAttribute("sid");
  $eid =~ s/^E//;
  %ids = ();
  $cstructure = ($parseroot->getChildrenByTagName("c_structure"))[0];
  if ($cstructure == undef) {
    print STDERR "WARNING: No c-structures for sentence E$eid\n";
    $parseroot = $parseroot->getNextSibling();
  } else {
    $fstructures = ($parseroot->getChildrenByTagName("f_structures"))[0];
    print STDERR "WARNING: No f-structures for sentence E$eid\n" if ($fstructures == undef);
    
    $gid = 0;
    $tid = "";
    %tid2fid = ();
    $rid = 0;
    print "<E id=\"E$eid\">\n";
    print "<constituants>\n";
    &print_easy_chunks($cstructure);
    print "</constituants>\n";
    print "<relations>\n";
    if ($fstructures) {
      &print_easy_relations();
    }
    print "</relations>\n";
    print "</E>\n";
    $parseroot = $parseroot->getNextSibling();
  }
  while ($parseroot && $parseroot->getName ne "E") {$parseroot = $parseroot->getNextSibling();}
}

print <<END;
</DOCUMENT>
END

sub print_easy_chunks {
    my $node=shift;
    my $easy_chunk=0;
    my $gtype;
    if ($node->nodeName eq "F") {
	$node->setAttribute("gid","E${eid}G$gid");
	if ($node->hasAttribute("xml:id")) {
	    $tid2fid{$tid} = $node->getAttribute("xml:id");
	    print "    <F id=\"".$node->getAttribute("xml:id")."\">".$node->getFirstChild->data."</F>\n";
	} else {
	    $tid2fid{$tid} = $node->getAttribute("xml:idref");
	}
    } elsif ($node->nodeName eq "T") {
	$tid = $node->getAttribute("xml:id");
	for ($node->getChildNodes) {
	    print_easy_chunks ($_);
	}
    } else {
	if ($node->nodeName eq "node"
	    && $node->getAttribute("name") =~ /^Easy_(G[ANPR]|[NP]V)/) {
	    $gid++;
	    $easy_chunk = 1;
	    $gtype = $1;
	    $node->setAttribute("gid","E${eid}G$gid");
	    $node->setAttribute("gtype","$gtype");
	}
	if ($easy_chunk) {
	    print "  <Groupe type=\"$gtype\" id=\"E${eid}G$gid\">\n";
	}
	for ($node->getChildNodes) {
	    print_easy_chunks ($_);
	}
	if ($easy_chunk) {
	    print "  </Groupe>\n";
	}
    }
}

sub print_easy_relations {
  &print_SUJ_V();
  &print_AUX_V();
  &print_COD_V();
  &print_CPL_V();
  &print_MOD_V();
  &print_MOD_N();
  &print_MOD_A();
  &print_MOD_R();
  &print_MOD_P();
  &print_COMP();
  &print_ATB_SO();
  &print_COORD();
  &print_APPOS();
  &print_JUXT();
}

sub print_SUJ_V {
    my $suj;
    my ($v,$origv,$cop,$copnt,$copntid,$fs);
    my $sujgroup;
    for $v ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
	$origv=$v;
	for my $type ("Suj","SujC") {
	    $v=$origv;
	    $suj = getSubStruct($v,$type);
	    if ($suj && hasPred($suj)) {
		$v = getAnchorTerm($v);
		next if (!$v);
		$v = getClosestAncestor($v,"NV");
		next if (!$v);
		for ($v->getChildrenByTagName("node")) {
		    if ($_->getAttribute("name") =~ /^Easy_NV/) {
			$v = $_->getAttribute("gid");
			last;
		    }
		}
		$sujgroup = getClosestAncestor(getAnchorTerm($suj),"Easy_");
		if ($sujgroup && $sujgroup->getAttribute("name") !~ /^Easy_(00|NV)/) {
		    $suj = $sujgroup->getAttribute("gid");
		} else {
		    $suj = getAnchor($suj);
		}
		if ($suj && $v) {
		    print_relation("SUJ-V","sujet","verbe",$suj,$v);
		}
	    }
	    $suj = undef;
	}
    }
    # constructions � copule: on r�cup�re la relation sujet-copule
    for $copnt ($cstructure->getElementsByTagName("node")) {
	next if $copnt->getAttribute("name") !~ /Easy_NVA[1U]copule/;
	$copntid = $copnt->getAttribute("xml:id");
	$copnt = $copnt->getAttribute("gid");
	fsloop: for $fs ($fstructures->getElementsByTagName("f_structure")) {
	    $v=$fs;
	    next if !hasPred($fs);
	    for ($fs->getChildrenByTagName("attribute")) {
		if ($_->getAttribute("name") eq "aij") {
		    for ($_->getChildrenByTagName("value")) {
			if ($_->getAttribute("xml:idref") eq $copntid) {
			  last fsloop;
			}
		    }
		}
	    }
	}
	if ($v) {
	  for my $type ("Suj","SujC") {
	    $suj = getSubStruct($v,$type);
	    if ($suj && hasPred($suj)) {
	      $sujgroup = getClosestAncestor(getAnchorTerm($suj),"Easy_");
	      if ($sujgroup && $sujgroup->getAttribute("name") !~ /^Easy_(00|NV)/) {
		$suj = $sujgroup->getAttribute("gid");
	      } else {
		$suj = getAnchor($suj);
	      }
	      if ($suj && $copnt) {
		print_relation("SUJ-V","sujet","verbe",$suj,$copnt);
	      }
	    }
	    $suj = undef;
	  }
	}
    }
}

sub print_AUX_V {
    my $aux1 = undef;
    my $aux2 = undef;
    my $v = undef;
    my $nv;
    for $aux1 ($cstructure->getElementsByTagName("node")) {
	next if $aux1->getAttribute("name") !~ /Easy_NVA[1U]/;
	$nv = $aux1->parentNode;
	for ($nv->getElementsByTagName("node")) {
	    if ($_->getAttribute("name") =~ /^Easy_NVA[1U]/) {
		$aux1 = $_;
	    } elsif ($_->getAttribute("name") =~ /^Easy_NVAn/) {
		$aux2 = $_;
	    } elsif ($_->getAttribute("name") =~ /^Easy_NV/) {
		$v = $_;
	    }
	}
	if ($v) {
	    if ($aux2) {
		print_relation("AUX-V","auxiliaire","verbe",getLastToken($aux1),getLastToken($aux2));
		print_relation("AUX-V","auxiliaire","verbe",getLastToken($aux2),getLastToken($v));
	    } else {
		print_relation("AUX-V","auxiliaire","verbe",getLastToken($aux1),getLastToken($v));
	    }
	}
    }
}

sub print_COD_V {
    my $obj;
    my $v;
    my $objgroup;
    for $v ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
	$obj = getSubStruct($v,"Obj");
	if ($obj && hasPred($obj)) {
	    $v = getAnchorTerm($v);
	    next if (!$v);
	    $v = getClosestAncestor($v,"Easy_");
	    next if (!$v);
	    $v = $v->getAttribute("gid");
	    next if (!$v);
	    $objgroup = getClosestAncestor(getAnchorTerm($obj),"Easy_");
	    if ($objgroup && $objgroup->getAttribute("name") !~ /^Easy_(00|NV)/) {
		$obj = $objgroup->getAttribute("gid");
	    } else {
		$obj = getAnchor($obj);
	    }
	    if ($obj && $v) {
	      if ($objgroup->getAttribute("name") =~ /^Easy_PV/
		 && $objgroup->getAttribute("name") !~ /^Easy_PV.*INF_DE_/) {
		print_relation("CPL-V","complement","verbe",$obj,$v);		
	      } else {
		print_relation("COD-V","cod","verbe",$obj,$v);
	      }
	    }
	}
	$obj = undef;
    }
}

sub print_CPL_V {
  my $obj;
  my ($v, $origv);
  my $objgroup;
  for $v ($fstructures->getElementsByTagName("f_structure")) {
    next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
    my $v_not_computed=1;
    $origv = $v;
    for my $type ("Obj�","Objde","Loc","Dloc","Obl","Obl2") {
      $obj = getSubStruct($origv,$type);
      if ($obj && hasPred($obj)) {
	if ($v_not_computed) {
	  $v = getAnchorTerm($v);
	  next if (!$v);
	  $v = getClosestAncestor($v,"Easy_");
	  next if (!$v);
	  $v = $v->getAttribute("gid");
	  $v_not_computed=0;
	}
	next if (!$v);
	$objgroup = getClosestAncestor(getAnchorTerm($obj),"Easy_");
	if ($objgroup && $objgroup->getAttribute("name") !~ /^Easy_NV/) {
	  $obj = $objgroup->getAttribute("gid");
	} else {
	  $obj = getAnchor($obj);
	}
	if ($obj && $v) {
	  print_relation("CPL-V","complement","verbe",$obj,$v);
	}
      }
      $obj = undef;
    }
  }
}

sub print_MOD_V {
    my $mod;
    my ($v,$origv);
    my $modgroup;
    for $v ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
	$origv=$v;
	for $mod (getSubStructsList($v,"adjunct")) {
	    $v=$origv;
	    if ($mod && hasPred($mod)) {
		$v = getAnchorTerm($v);
		next if (!$v);
		$v = getClosestAncestor($v,"Easy_");
		next if (!$v);
		$v = $v->getAttribute("gid");
		next if (!$v);
		$modgroup = getClosestAncestor(getAnchorTerm($mod),"Easy_");
		if ($modgroup && $modgroup->getAttribute("name") !~ /^Easy_(NV|00)/) {
		    $mod = $modgroup->getAttribute("gid");
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $v) {
		    if ($modgroup && ($modgroup->getAttribute("gtype") eq "GP" || $modgroup->getAttribute("gtype") eq "GP")) {
			print_relation("CPL-V","complement","verbe",$mod,$v);
		    } else {
			print_relation("MOD-V","modifieur","verbe",$mod,$v);
		    }
		}
	    }
	}
	$mod = undef;
    }
}

sub print_MOD_N {
  my $mod;
  my ($nom,$orignom);
  my $modgroup;
  for $nom ($fstructures->getElementsByTagName("f_structure")) {
     next if !hasPred($nom) || getAtomicVal($nom,"cat") !~ /^n/;
     $orignom=$nom;
     for $mod (getSubStructsList($nom,"adjunct")) {
       next unless $mod;
       next unless getAnchorTerm($mod);
       next if getAnchorTerm($mod)->getAttribute("name") =~ /^pro/;
      $nom=$orignom;
      if ($mod && hasPred($mod)) {
	$nom = getAnchorTerm($nom);
	next if (!$nom);
	$nom = getClosestAncestor($nom,"Easy_");
	next if (!$nom);
	$nom = $nom->getAttribute("gid");
	next if (!$nom);
	$modgroup = getClosestAncestor(getAnchorTerm($mod),"Easy_");
	if ($modgroup && $nom ne $modgroup->getAttribute("gid")) {
	  $mod = $modgroup->getAttribute("gid");
	} else {
	  $mod = getAnchor($mod);
	}
	if ($mod && $nom) {
	  print_relation("MOD-N","modifieur","nom",$mod,$nom);
	}
      }
     }
     $mod = undef;
  }
}

sub print_MOD_A {
    my $mod;
    my ($adj,$origadj);
    my $modgroup;
    for $adj ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($adj) || getAtomicVal($adj,"cat") !~ /^adj/;
	$origadj=$adj;
	for $mod (getSubStructsList($adj,"adjunct")) {
	    $adj=$origadj;
	    if ($mod && hasPred($mod)) {
		$adj = getAnchorTerm($adj);
		next if (!$adj);
		$adj = getClosestAncestor($adj,"Easy_");
		next if (!$adj);
		$adj = $adj->getAttribute("gid");
		next if (!$adj);
		$modgroup = getClosestAncestor(getAnchorTerm($mod),"Easy_");
		if ($modgroup && $adj ne $modgroup->getAttribute("gid")) {
		    $mod = $modgroup->getAttribute("gid");
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $adj) {
		    print_relation("MOD-A","modifieur","adjectif",$mod,$adj);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_MOD_R {
    my $mod;
    my ($adv,$origadv);
    my $modgroup;
    for $adv ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($adv) || getAtomicVal($adv,"cat") !~ /^adv/;
	$origadv=$adv;
	for $mod (getSubStructsList($adv,"adjunct")) {
	    $adv=$origadv;
	    if ($mod && hasPred($mod)) {
		$adv = getAnchorTerm($adv);
		next if (!$adv);
		$adv = getClosestAncestor($adv,"Easy_")->getAttribute("gid");
		next if (!$adv);
		$modgroup = getClosestAncestor(getAnchorTerm($mod),"Easy_");
		if ($modgroup) {
		    $mod = $modgroup->getAttribute("gid");
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $adv) {
		    print_relation("MOD-R","modifieur","adverbe",$mod,$adv);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_MOD_P {
    my $mod;
    my ($prep,$origprep);
    my $modgroup;
    for $prep ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($prep) || getAtomicVal($prep,"cat") !~ /^prep/;
	$origprep=$prep;
	for $mod (getSubStructsList($prep,"adjunct")) {
	    $prep=$origprep;
	    if ($mod && hasPred($mod)) {
		$prep = getAnchorTerm($prep);
		next if (!$prep);
		$prep = getClosestAncestor($prep,"Easy_")->getAttribute("gid");
		next if (!$prep);
		$modgroup = getClosestAncestor(getAnchorTerm($mod),"Easy_");
		if ($modgroup) {
		    $mod = $modgroup->getAttribute("gid");
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $prep) {
		    print_relation("MOD-P","modifieur","preposition",$mod,$prep);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_COMP {
    my ($prop,$csuform);
    my$prophead = undef;
    for $prop ($cstructure->getElementsByTagName("node")) {
	next unless $prop->getAttribute("name") =~ /^Easy_00(que|csu)/;
	$csuform = $prop;
	$csuform = ($csuform->getElementsByTagName("F"))[-1];
	last unless ($csuform);
	$csuform = $csuform->getAttribute("xml:id");
	$prop = $prop->parentNode->getAttribute("xml:id");
	for my $v ($fstructures->getElementsByTagName("f_structure")) {
	    next if !hasPred($v);
	    for ($v->getChildrenByTagName("attribute")) {
		if ($_->getAttribute("name") eq "aij") {
		    for ($_->getChildrenByTagName("value")) {
			if ($_->getAttribute("xml:idref") eq $prop) {
			    $prophead = getAnchorTerm($v);
			    next if (!$prophead);
			    $prophead = getClosestAncestor($prophead,"Easy_")->getAttribute("gid");
			    last;
			}
		    }
		}
	    }
	}
	if ($csuform && $prophead) {
	    print_relation("COMP","complementeur","verbe",$csuform,$prophead);
	}
    }
}

sub print_ATB_SO {
  my $att;
  my $v;
  for $v ($fstructures->getElementsByTagName("f_structure")) {
    next if !hasPred($v) || getAtomicVal($v,"cat") ne "v";
    $att = getSubStruct($v,"Att");
    if ($att && hasPred($att)) {
      $v = getAnchorTerm($v);
      next unless $v;
      $v = getClosestAncestor($v,"Easy_");
      next unless $v;
      $v = $v->getAttribute("gid");
      next unless $v;
      $att = getClosestAncestor(getAnchorTerm($att),"Easy_");
      next unless $att;
      if ($att !~ /^Easy_00/) {
	$att = $att->getAttribute("gid");
      } else {
	$att = getAnchor($att);
      }
      if ($att && $v) {
	print_relation("ATB-SO","attribut","verbe",$att,$v,"    <s-o valeur=\"sujet\"/>");
      }
    }
    $att = undef;
  }
  # constructions � copule: on r�cup�re la relation sujet-copule
  my ($att, $attgroup, $copnt, $copntid, $fs);
  for $copnt ($cstructure->getElementsByTagName("node")) {
    next if $copnt->getAttribute("name") !~ /Easy_NVA[1U]copule/;
    $copnt = $copnt->getAttribute("gid");
    $att = node2fs($copnt);
    if ($att && hasPred($att)) {
      $attgroup = getClosestAncestor(getAnchorTerm($att),"Easy_");
      if ($attgroup && $attgroup->getAttribute("name") !~ /^Easy_NV/) {
	$att = $attgroup->getAttribute("gid");
      } else {
	$att = getAnchor($att);
      }
      if ($att && $copnt) {
	print_relation("ATB-SO","attribut","verbe",$att,$copnt,"    <s-o valeur=\"sujet\"/>");
      }
    }
    $att = undef;
  }
}

sub print_COORD {
    my @arg;
    my $coo;
    for $coo ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($coo) || getAtomicVal($coo,"cat") ne "coo";
	$arg[1]=$arg[2]=undef;
	for my $i (1,2) {
	    $arg[$i] = getSubStruct($coo,"arg$i");
	    if ($arg[$i] && hasPred($arg[$i])) {
		$arg[$i] = getClosestAncestor(getAnchorTerm($arg[$i]),"Easy_");
		if ($arg[$i]) {
		  $arg[$i] = $arg[$i]->getAttribute("gid");
		}
	    }
	}
	if ($arg[2]) {
#	    print "coo=$coo\n";
	    $coo = getAnchor($coo);
	    next if (!$coo);
	    if ($arg[1]) {
		print_relation_3("COORD","coordonnant","coord-g","coord-d",$coo,$arg[1],$arg[2]);
	    } else {
		print_relation("COORD","coordonnant","coord-d",$coo,$arg[2]);
	    }
	}
    }
}

sub print_APPOS {
    my $nom = undef;
    my $nomAppos = undef;
    for $nomAppos ($cstructure->getElementsByTagName("node")) {
	next if $nomAppos->getAttribute("name") ne "SN_appos";
	$nom = $nomAppos->parentNode;
	while ($nom->getAttribute("name") =~ /appos/) {
	  $nom = $nom->parentNode;
	}
	next unless ($nom);
	$nom = node2fs($nom);
	next unless ($nom);
	$nom = getAnchorTerm($nom);
	next unless ($nom);
	$nom = getClosestAncestor($nom,"Easy_")->getAttribute("gid");
	next unless ($nom);
	$nomAppos = node2fs($nomAppos);
	next unless ($nomAppos);
	$nomAppos = getAnchorTerm($nomAppos);
	next unless ($nomAppos);
	$nomAppos = getClosestAncestor($nomAppos,"Easy_")->getAttribute("gid");
	next unless ($nomAppos);	
	print_relation("APPOS","premier","appose",$nom,$nomAppos);
    }
    my $mod;
    my $orignom;
    my $modgroup;
    for $nom ($fstructures->getElementsByTagName("f_structure")) {
	next if !hasPred($nom) || getAtomicVal($nom,"cat") !~ /^n/;
	$orignom=$nom;
	for $mod (getSubStructsList($nom,"adjunct")) {
	  next unless $mod;
	  next unless getAnchorTerm($mod);
	  next if getAnchorTerm($mod)->getAttribute("name") !~ /^pro/;
	    $nom=$orignom;
	    if ($mod && hasPred($mod)) {
		$nom = getAnchorTerm($nom);
		next if (!$nom);
		$nom = getClosestAncestor($nom,"Easy_");
		next if (!$nom);
		$nom = $nom->getAttribute("gid");
		next if (!$nom);
		$modgroup = getClosestAncestor(getAnchorTerm($mod),"Easy_");
		if ($modgroup && $nom ne $modgroup->getAttribute("gid")) {
		    $mod = $modgroup->getAttribute("gid");
		} else {
		    $mod = getAnchor($mod);
		}
		if ($mod && $nom) {
		    print_relation("APPOS","premier","appose",$nom,$mod);
		}
	    }
	}
	$mod = undef;
    }
}

sub print_JUXT {
    my @arg;
    my $coo;
    # Easy_JUXT2
    for $coo ($cstructure->getElementsByTagName("node")) {
	next if $coo->getAttribute("name") ne "Easy_JUXT2";
	$arg[1]=undef;
	$arg[2]=undef;
	for ($coo->getChildrenByTagName("node")) {
	    if ($_->getAttribute("name") eq "PHRASE") {
		$arg[2]=$_;
	    } elsif ($_->getAttribute("name") eq "ENONCES") {
		$arg[1]=($_->getChildrenByTagName("node"))[0];
		if ($arg[1] && $arg[1]->getAttribute("name") eq "Easy_JUXT2b") {
		    for ($arg[1]->getChildrenByTagName("node")) {
			if ($_->getAttribute("name") eq "PHRASE") {
			    $arg[1]=$_;
			    last;
			}
		    }	    
		}
	    }
	}
	if ($arg[1] && $arg[2]) {
	    for my $i (1,2) {
		$arg[$i]=$arg[$i]->getAttribute("xml:id");
		for my $v ($fstructures->getElementsByTagName("f_structure")) {
		    next if !hasPred($v);
		    for ($v->getChildrenByTagName("attribute")) {
			if ($_->getAttribute("name") eq "aij") {
			    for ($_->getChildrenByTagName("value")) {
				if ($_->getAttribute("xml:idref") eq $arg[$i]) {
				    $arg[$i] = getAnchorTerm($v);
				    next if (!$arg[$i]);
				    $arg[$i] = getClosestAncestor($arg[$i],"Easy_");
				    if ($arg[$i] && $arg[$i] !~ /^Easy_00/) {
				      $arg[$i] = $arg[$i]->getAttribute("gid");
				    } else {
				      $arg[$i] = getAnchor($arg[$i]);
				    }
				    last;
				}
			    }
			}
		    }
		}
	    }
	}
	if ($arg[1] && $arg[2]) {
	    print_relation("JUXT","premier","suivant",$arg[1],$arg[2]);
	}
    }
    # Easy_JUXT2b
    juxt2: for $coo ($cstructure->getElementsByTagName("node")) {
	next if $coo->getAttribute("name") ne "Easy_JUXT2b";
	$arg[1]=undef;
	$arg[2]=undef;
	for ($coo->getChildrenByTagName("node")) {
	    if ($_->getAttribute("name") eq "S3") {
		$arg[2]=$_;
	    } elsif ($_->getAttribute("name") eq "S4") {
		$arg[1]=$_;
		my $tmp = ($_->getChildrenByTagName("node"))[0];
		if ($tmp && $tmp->getAttribute("name") eq "Easy_JUXT2b") {
		    $arg[1]=undef;
		    for ($tmp->getChildrenByTagName("node")) {
			if ($_->getAttribute("name") eq "S3") {
			    $arg[1]=$_;
			    last;
			}
		    }	    
		}
	    }
	}
	if ($arg[1] && $arg[2]) {
	    for my $i (1,2) {
		$arg[$i]=$arg[$i]->getAttribute("xml:id");
		arg: for my $v ($fstructures->getElementsByTagName("f_structure")) {
		    next if !hasPred($v);
		    for ($v->getChildrenByTagName("attribute")) {
			if ($_->getAttribute("name") eq "aij") {
			    for ($_->getChildrenByTagName("value")) {
				if ($_->getAttribute("xml:idref") eq $arg[$i]) {
				    $arg[$i] = getAnchorTerm($v);
				    next juxt2 if (!$arg[$i]);
				    if (getClosestAncestor($arg[$i],"Easy_")) {
				      $arg[$i] = getClosestAncestor($arg[$i],"Easy_")->getAttribute("gid");
				    } else {
				      $arg[$i] = getAnchor($arg[$i]);
				    }
				    last arg;
				}
			    }
			}
		    }
		}
		next juxt2 if ($arg[$i] =~ /^R/);
	    }
	}
	if ($arg[1] && $arg[2]) {
	    if ($arg[1]!~/^R/ && $arg[2]!~/^R/) {
		print_relation("JUXT","premier","suivant",$arg[1],$arg[2]);
	    } else {
		print STDERR join("\n\t","JUXT","premier","suivant",$arg[1],$arg[2])."\n";
	    }
	}
    }
}



sub getSubStruct {
    my $fs = shift;
    my $sfs_name = shift;
    my $ptr;
    for $ptr ($fs->getElementsByTagName("attribute")) {
	if ($ptr->getAttribute("type") eq "struct" && $ptr->getAttribute("name") eq $sfs_name) {
	    $ptr = ($ptr->getElementsByTagName("value"))[0];
	    $ptr = $ptr->getAttribute("xml:idref");
	    return $xmlparse->getElementsById($ptr);
	}
    }
    return undef;
}

sub getSubStructsList {
    my $fs = shift;
    my $sfs_name = shift;
    my $ptr;
    my @answer=();
    for $ptr ($fs->getElementsByTagName("attribute")) {
	if ($ptr->getAttribute("type") eq "set" && $ptr->getAttribute("name") eq $sfs_name) {
	    for my $ptr2 ($ptr->getElementsByTagName("value")) {
		$ptr2 = $ptr2->getAttribute("xml:idref");
		push(@answer,$xmlparse->getElementsById($ptr2));
	    }
	}
    }
    return @answer;
}

sub getAtomicVal {
    my $fs = shift;
    my $at_name = shift;
    my $ptr;
    for $ptr ($fs->getElementsByTagName("attribute")) {
	if ($ptr->getAttribute("type") eq "atom" && $ptr->getAttribute("name") eq $at_name) {
	    return undef if $fs == undef;
	    $ptr = ($ptr->getElementsByTagName("value"))[0];
	    return undef if $fs == undef;
	    $ptr = $ptr->getAttribute("atomic_val");
	    return $ptr;
	}
    }
    return undef;
}

sub getAnchor {
    my $fs = shift;
    $fs = ($fs->getElementsByTagName("pred"))[0];
    return undef if $fs == undef;
    $fs = ($fs->getElementsByTagName("anchor"))[0];
    return undef if $fs == undef;
    $fs = $fs->getAttribute("xml:idref");
    return $tid2fid{$fs};
}

sub getAnchorTerm {
    my $fs = shift;
    $fs = ($fs->getElementsByTagName("pred"))[0];
    return undef if $fs == undef;
    $fs = ($fs->getElementsByTagName("anchor"))[0];
    return undef if $fs == undef;
    $fs = $fs->getAttribute("xml:idref");
    return $xmlparse->getElementsById($fs);
}

sub getLastToken {
    my $node = shift;
    my $tid = "";
    for ($node->getElementsByTagName("F")) {
	if ($_->hasAttribute("xml:id")) {
	    $tid = $_->getAttribute("xml:id");
	} else {
	    $tid = $_->getAttribute("xml:idref");
	}
    }
    return $tid;
}

sub hasPred {
    my $fs = shift;
    my $ptr = ($fs->getElementsByTagName("pred"))[0];
    return !($ptr == undef);
}

sub getClosestAncestor {
    my $node = shift;
    my $nameStart = shift;
    if ($node) {
	while ($node->parentNode && $node->parentNode->nodeName eq "node" && $node->parentNode->getAttribute("name") !~ /^$nameStart/) {
	    $node = $node->parentNode;
	}
	if ($node->parentNode->getAttribute("name") =~ /^$nameStart/) {
	  return $node->parentNode;
	}
    }
    return undef;
}

my %print_relation_cache;

sub print_relation {
    my $type = shift;
    my $name1 = shift;
    my $name2 = shift;
    my $href1 = shift;
    my $href2 = shift;
    my $flag = shift;
    return if ($href1 eq "" || $href2 eq "");
    if (!defined($print_relation_cache{$type}{$href1."\t".$href2."\t".$flag})) {
	$rid++;
	print "  <relation xlink:type=\"extended\" type=\"$type\" id=\"E${eid}R$rid\">\n";
	print "    <$name1 xlink:type=\"locator\" xlink:href=\"$href1\"/>\n";
	print "    <$name2 xlink:type=\"locator\" xlink:href=\"$href2\"/>\n";
	if ($flag) {
	    print $flag."\n";
	}
	print "  </relation>\n";
    }
}

my %print_relation_3_cache;

sub print_relation_3 {
    my $type = shift;
    my $name1 = shift;
    my $name2 = shift;
    my $name3 = shift;
    my $href1 = shift;
    my $href2 = shift;
    my $href3 = shift;
    my $flag = shift;
    return if ($href1 eq "" || $href2 eq "" || $href3 eq "");
    if (!defined($print_relation_3_cache{$type}{$href1."\t".$href2."\t".$href3."\t".$flag})) {
	$rid++;
	print "  <relation xlink:type=\"extended\" type=\"$type\" id=\"E${eid}R$rid\">\n";
	print "    <$name1 xlink:type=\"locator\" xlink:href=\"$href1\"/>\n";
	print "    <$name2 xlink:type=\"locator\" xlink:href=\"$href2\"/>\n";
	print "    <$name3 xlink:type=\"locator\" xlink:href=\"$href3\"/>\n";
	if ($flag) {
	    print $flag."\n";
	}
	print "  </relation>\n";
    }
}

sub node2fs {
  my $node = shift;
  $node = $node->getAttribute("xml:id");
  my ($ret_val, $fs);
  $fs = undef;
 fsloop: for $fs ($fstructures->getElementsByTagName("f_structure")) {
    $ret_val=$fs;
    next if !hasPred($fs);
    for ($fs->getChildrenByTagName("attribute")) {
      if ($_->getAttribute("name") eq "aij") {
	for ($_->getChildrenByTagName("value")) {
	  if ($_->getAttribute("xml:idref") eq $node) {
	    last fsloop;
	  }
	}
      }
    }
  }
  return $ret_val;
}
