/* ********************************************************
   *                                                      *
   *                                                      *
   * Copyright (c) 2005 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *                                                      *
   *                                                      *
   ******************************************************** */

/* Actions semantiques permettant la sortie de diverses vues du r�sultat de l'analyse Earley et/ou LFG et/ou chunks et/ou nbest */

/* ********************************************************
   *                                                      *
   *          Produit de l'equipe ATOLL.                  *
   *                                                      *
   ******************************************************** */

/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* 26-10-05 11:30 (pb&an):	Ajout de cette rubrique "modifications"	*/
/************************************************************************/



static char	ME [] = "output_semact";

/* Les valeurs atomiques sont qcq et on peut faire de la disjonction sur les "..." */
#define ESSAI

#include "sxunix.h"
#include "earley.h"
#include "XH.h"
#include "varstr.h"
#include "sxstack.h"
#include "udag_scanner.h"
#include <math.h>
#include <float.h>

char WHAT_OUTPUT_SEMACT[] = "@(#)SYNTAX - $Id$" WHAT_DEBUG;

#ifdef ESSAI
/* Pour atom_id2local_atom_id_hd */
#include "X.h"
#endif /* ESSAI */


#include td_h

static char             *output_kind;
static BOOLEAN          is_print_headers;
static BOOLEAN          is_print_xml_headers;

static SXINT              *spf_count;
static SXINT              *Pij2out_sentence, *Aij2out_sentence;
static SXINT              rec_level, last_f;
#define SENTENCE_CONCAT 0X80000000
#define SENTENCE_OR     0X40000000
static XH_header        out_sentence_hd;
static SXINT              maxxt; /* nb des terminaux instancies Tij, y compris ceux couverts par les nt de la rcvr == -spf.outputG.maxt */
static BOOLEAN          nl_done;

static FILE    *weights_file;
static char    *weights_file_name;
FILE    *xml_file;
static char    *xml_file_name;
static FILE    *odag_file;
static char    *odag_file_name;

static char    compact_infos[32];

#include "sxword.h"

static SXINT            Ei_lgth;


#define Tpq2str(Tpq)  (Tpq ? (spf.inputG.tstring [-Tij2T (Tpq-spf.outputG.maxxnt)]) : NULL)

extern SXINT            sentence_id; /* nbest peut en avoir besoin */
/* extern char           *sntid2string[MAX_SNT_ID+1]; */
extern char             output_sem_pass_arg;

/* valeur par defaut */

static char	Usage [] = "\
\t\t-xml pathname,\n\
\t\t-odag pathname,\n\
\t\t-wf pathname, -weights_file pathname,\n\
\t\t-ok kind, -output_kind kind,\n\
";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 	   0
#define XML                1
#define WEIGHTS            2
#define OUTPUT_KIND        3
#define PRINT_HEADERS      4
#define PRINT_XML_HEADER   5
#define ODAG               6

static char	*option_tbl [] = {
    "",
    "xml",
    "wf", "weights_file",
    "ok","output_kind",
    "ph","print_headers",
    "pxh","print_xml_header",
    "odag",
};

static SXINT	option_kind [] = {
    UNKNOWN_ARG,
    XML,
    WEIGHTS, WEIGHTS,
    OUTPUT_KIND,OUTPUT_KIND,
    PRINT_HEADERS, PRINT_HEADERS,
    PRINT_XML_HEADER, PRINT_XML_HEADER,
    ODAG,
};

static SXINT
option_get_kind (char *arg)
{
  char	**opt;

  if (*arg++ != '-')
    return UNKNOWN_ARG;

  if (*arg == NUL)
    return UNKNOWN_ARG;

  for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
    if (strcmp (*opt, arg) == 0 /* egalite */ )
      break;
  }

  return option_kind [opt - option_tbl];
}



static char *
option_get_text (SXINT kind)
{
  register SXINT	i;

  for (i = OPT_NB; i > 0; i--) {
    if (option_kind [i] == kind)
      break;
  }

  return option_tbl [i];
}


/* retourne le ME */
static char*
output_ME ()
{
  return ME;
}

/* retourne la chaine des arguments possibles propres a output */
static char*
output_args_usage ()
{
  return Usage;
}

/* decode les arguments specifiques a output */
/* l'option argv [*parg_num] est inconnue du parseur earley */
static BOOLEAN
output_args_decode (int *pargnum, int argc, char *argv [])
{
  switch (option_get_kind (argv [*pargnum])) {

  case XML:
    if (++*pargnum >= argc) {
      fprintf (sxstderr, "%s: a pathname must follow the \"%s\" option;\n", ME, option_get_text (XML));
      return FALSE;
    }
    xml_file_name = argv [*pargnum];
    
    break;

  case ODAG:
    if (++*pargnum >= argc) {
      fprintf (sxstderr, "%s: a pathname must follow the \"%s\" option;\n", ME, option_get_text (ODAG));
      return FALSE;
    }
    odag_file_name = argv [*pargnum];
    
    break;

  case WEIGHTS:
    if (++*pargnum >= argc) {
      fprintf (sxstderr, "%s: a pathname must follow the \"%s\" option;\n", ME, option_get_text (WEIGHTS));
      return FALSE;
    }
	      
    weights_file_name = argv [*pargnum];

    break;

  case OUTPUT_KIND:
    if (++*pargnum >= argc) {
      fprintf (sxstderr, "%s: an output kind specification string must follow the \"%s\" option;\n", ME, option_get_text (OUTPUT_KIND));
      return FALSE;
    }

    output_kind = argv [*pargnum];
    break;

  case PRINT_HEADERS:
    is_print_headers = TRUE;

  case PRINT_XML_HEADER:
    is_print_xml_headers = TRUE;

  case UNKNOWN_ARG:
    return FALSE;
  }

  return TRUE;
}



static char*
ntstring2propname (char *ntstring)
{
  if (!strcmp(ntstring,"WS")) {
    return("completive");
  } else if (!strcmp(ntstring,"Prop_INTD")
	     || !strcmp(ntstring,"Prop_COMPLETIVE")
	     || !strcmp(ntstring,"Prop_COMPLETIVE_A")
	     || !strcmp(ntstring,"Prop_COMPLETIVE_DE")) {
    return("completive");
  } else if (!strcmp(ntstring,"Prop_CIRC")) {
    return("circonstancielle");
  } else if (!strncmp(ntstring,"Prop_INTI",9)) {
    return("interrogative_indirecte");
  } else if (!strcmp(ntstring,"Prop_RELATIVE")) {
    return("relative");
  }
  return NUL;
}

static char*
ntstring2phrasename (char *ntstring)
{
  if (!strcmp(ntstring,"NP")
      || !strcmp(ntstring,"NumP")
      || !strcmp(ntstring,"AP")
      || !strcmp(ntstring,"PP")
      || !strcmp(ntstring,"VP")
      || !strcmp(ntstring,"AdvP")
      )
    return(ntstring);
  return NUL;
}

static SXINT cur_snt_id;

/* parcours top-down y compris les terminaux: boucle r�cursive */
static void
spf_td_walk_incl_terminals (SXINT Aij, char* (*ntstring2name) (char *), char *ntkind, char *ntkindshort)
{
  SXINT            output_prod, output_item, Xpq, hook, i, k, tok_no;
  SXINT            Aij_val, local_f;
  char             *ntstring, *comment, *str1, *str2;
  struct sxtoken   *tok_ptr;

  if ((Aij_val = spf_count [Aij]) == -1) {
    /* 1ere visite de Aij */
    Aij_val = spf_count [Aij] = 0;
    ntstring = spf.inputG.ntstring[Aij2A (Aij)];

    ntstring=(*ntstring2name) (ntstring);

    if (ntstring != NUL && Aij2i (Aij) < Aij2j (Aij)) {
      if (!nl_done) {
	fprintf(xml_file,"\n");
	nl_done=TRUE;
      }
      for (i = 0; i <= rec_level; i++)
	fprintf(xml_file,"  ");
      fprintf(xml_file,"<%s type=\"%s\" id=\"E%i%s%i\">", ntkind, ntstring, sentence_id, ntkindshort, cur_snt_id++);
      rec_level++;
      nl_done=FALSE;
    }
    
    hook = spf.outputG.lhs [spf.outputG.maxxprod+Aij].prolon;
    
    while ((output_prod = spf.outputG.rhs [hook++].lispro) != 0) {
      /* On parcourt les productions dont la lhs est Aij */
      if (output_prod > 0) { /* et qui ne sont pas filtr�es */
	output_item = spf.outputG.lhs [output_prod].prolon;
	Aij_val = 1;
	
	while ((Xpq = spf.outputG.rhs [output_item++].lispro) != 0) {
	  /* On parcourt la RHS de la  prod instanciee output_prod */
	  if (Xpq <= spf.outputG.maxnt) {
	    //	    fprintf(xml_file,"(%i <? %i)\n",Xpq,spf.outputG.maxnt);
	    if (Xpq > 0) {
	      /* nt */
	      spf_td_walk_incl_terminals(Xpq, ntstring2name, ntkind, ntkindshort);
	    }
	    else {
	      /* terminal*/
	      if (!nl_done)
		fputs("\n",xml_file);
	      nl_done=TRUE;

	      tok_no = spf_get_Tij2tok_no_stack (-Xpq)[1]; /* le premier token de Tpq  (ils ont tous le meme commentaire) */
	      tok_ptr = &(SXGET_TOKEN (tok_no));
	      comment = tok_ptr->comment;

	      if (comment) {
		comment++;
		for (comment; *comment != NUL; comment++) {
		  str1 = strchr(comment,(int)'F');
		  str2 = strchr(str1+3,(int)'F');
		  str1 /* str3 */ = strchr(str2,(int)'"');
		  *str1=NUL;
		  local_f=atoi(str2+1);
		  *str1='"';
		  if (local_f > last_f) {
		    last_f=local_f;
		    if (!nl_done)
		      fputs("\n",xml_file);
		    nl_done=TRUE;
		    for (i = 0; i <= rec_level; i++)
		      fprintf(xml_file,"  ");
		    for (comment; *comment != '}'; comment++){
		      fprintf(xml_file,"%c",*comment);
		      nl_done=FALSE;
		    }
		  } else {
		    comment = strchr(comment,(int)'>');
		    comment = strchr(comment+1,(int)'>');
		    comment+=2;
		  }
		}
	      } else { // pas de commentaires: on sort la ff dans un commentaire XML
		fprintf(xml_file,"<!-- %s -->\n",sxstrget (tok_ptr->string_table_entry));
	      }
	    }
	  }
	}
      }
    }
    if (ntstring != NUL && Aij2i (Aij) < Aij2j (Aij)) {
      rec_level--;
      if (!nl_done)
	fprintf(xml_file,"\n");
      nl_done=TRUE;
      for (i = 0; i <= rec_level; i++)
	fprintf(xml_file,"  ");
      fprintf(xml_file,"</%s>\n",ntkind);
    }
    spf_count [Aij] = Aij_val;
  }
}

static void
spf_output_partial_forest (char* (*ntstring2name)(char *), char *ntkind, char *ntkindshort)
{
  SXINT i;

  if (spf.outputG.start_symbol != 0) {
    spf_count = (SXINT*) sxalloc (spf.outputG.maxxnt+1, sizeof (SXINT));
    for (i=0;i<=spf.outputG.maxxnt;i++)
      spf_count[i]=-1;

    cur_snt_id = 1;
    spf_td_walk_incl_terminals (spf.outputG.start_symbol, ntstring2name, ntkind, ntkindshort);

    sxfree (spf_count), spf_count = NULL;
  }
  if (!nl_done)
    fprintf(xml_file,"\n");

}







SXINT
easy_get_sentence_no ()
{
  SXINT  sentence_no = 0, tok_no;
  char *str1, *str2, *comment;

  tok_no = spf_get_Tij2tok_no_stack (1)[1]; /* le Tpq 1 est plus ou moins le premier, c�d ce qu'on veut */
  comment = SXGET_TOKEN (tok_no).comment;

  if (comment ) {
    str1 = strchr (comment+1, (int)'"');
	
    if (str1) {
      if (*++str1 == 'E') {
	str1++;
	str2 = strchr (str1, (int)'F');

	if (str2) {
	  /*
	    {<F id="E1F1">�</F> <F id="E1F2">moins</F> <F id="E1F3">que</F>} �_moins_que  
	    comment   ^
	    str1               ^
	    str2                ^
	  */
	  *str2 = NUL;
	  sentence_no = atoi (str1);
	  *str2 = 'F';
	}
      }
    }
  }

  return sentence_no;
}

/* On regarde si le nom du nt en lhs de Pij commence par Easy_A */
int
seek_prefix_Aij (char *Easy_A, SXINT Pij)
{
  SXINT  lgth, Aij;
  char *str;

  lgth = strlen (Easy_A);
  Aij = spf.outputG.lhs [Pij].lhs;
  str = spf.inputG.ntstring [Aij2A (Aij)];

  if (strlen (str) >= lgth && strncmp (str, Easy_A, lgth) == 0) {
    return Aij;
  }
  
  return 0;
}
/* On regarde si le nom du nt en lhs de Pij est A */
int
seek_Aij (char *A, SXINT Pij)
{
  SXINT  lgth, Aij;
  char *str;

  lgth = strlen (A);
  Aij = spf.outputG.lhs [Pij].lhs;
  str = spf.inputG.ntstring [Aij2A (Aij)];

  if (strlen (str) == lgth && strncmp (str, A, lgth) == 0) {
    return Aij;
  }
  
  return 0;
}
#endif /* no_lfg */

/* Attention retourne Pij si la lhs de cette Pij est "PP_tempij" */
static SXINT
seek_PP_tempij (SXINT Pij)
{
  return seek_Aij ("PP_temp", Pij) ? Pij : 0;
}

/* Attention retourne  Pij si la lhs de cette Pij est "NP_temp...ij" */
static SXINT
seek_Easy_GNij (SXINT Pij)
{
  return seek_prefix_Aij ("Easy_GN", Pij) ? Pij : 0;
}


/* Attention retourne  Pij si la lhs de cette Pij est "NP_temp...ij" */
static SXINT
seek_NP_tempij (SXINT Pij)
{
  return seek_prefix_Aij ("NP_temp", Pij) ? Pij : 0;
}


/* Attention retourne  Pij si la lhs de cette Pij est "Easy_JUXT2ij" */
static SXINT
seek_Easy_JUXT2ij (SXINT Pij)
{
  return seek_prefix_Aij ("Easy_JUXT2", Pij) ? Pij : 0;
}


/* Attention retourne  Pij si la lhs de cette Pij est "Easy_JUXTij" */
static SXINT
seek_Easy_JUXTij (SXINT Pij)
{
  return seek_Aij ("Easy_JUXT", Pij) ? Pij : 0;
}


static SXINT
seek_VERB_v (SXINT Pij)
{
  SXINT  Aij, href_verbe;
  
  if ((Aij = seek_prefix_Aij ("Easy_NVS", Pij)) || (Aij = seek_prefix_Aij ("Easy_NVP", Pij))) {
    if (href_verbe = seek_t (Aij, "v", 
			     TRUE /* Un terminal dont le nom commence par "v" */
			     ))
      return href_verbe;
  }

  return 0;
}


static SXINT
seek_N_nc (SXINT Pij)
{
  SXINT  Aij, href_verbe;

  if (Aij = seek_Aij ("N", Pij)) {
    if (href_verbe = seek_t (Aij, "nc", FALSE /* Le terminal "nc" */))
      return href_verbe;
  }
  
  return 0;
}


static SXINT
seek_WS_csu (SXINT Pij)
{
  SXINT  Aij, href;

  if (Aij = seek_Aij ("WS", Pij)) {
    if (href = seek_t (Aij, "csu", FALSE /* Le terminal "csu" */))
      return href;
  }
  
  return 0;
}


static SXINT
seek_Easy_PV_prep (SXINT Pij)
{
  SXINT  Aij, href;

  if (Aij = seek_prefix_Aij ("Easy_PV", Pij)) {
    if (href = seek_t (Aij, "prep", FALSE /* Le terminal "prep" */))
      return href;
  }
  
  return 0;
}


static SXINT
seek_Easy_GA_adj (SXINT Pij)
{
  SXINT  Aij, href;

  if (Aij = seek_prefix_Aij ("Easy_GA", Pij)) {
    if (href = seek_t (Aij, "adj", FALSE /* Le terminal "adj" */))
      return href;
  }
  
  return 0;
}


static SXINT
seek_Easy_GR_adv (SXINT Pij)
{
  SXINT  Aij, href;

  if (Aij = seek_prefix_Aij ("Easy_GR", Pij)) {
    if (href = seek_t (Aij, "adv", FALSE /* Le terminal "adv" */))
      return href;
  }
  
  return 0;
}


static SXINT
seek_Easy_NV (SXINT Pij)
{
  SXINT  Aij, href_gn;

  if (Aij = seek_prefix_Aij ("Easy_NV", Pij)) {
    if (href_gn = Xpq2easy_href [Aij])
      return href_gn;
  }
  
  return 0;
}

static SXINT
seek_Easy_NVij (SXINT Pij)
{
  SXINT  Aij, href_gn;

  if (Aij = seek_prefix_Aij ("Easy_NV", Pij)) {
    return Aij;
  }
  
  return 0;
}

static SXINT
seek_Easy_GA_or_adj (SXINT Pij)
{
  SXINT  Aij, href_gn;

  if (Aij = seek_prefix_Aij ("Easy_GA", Pij)) {
    if (href_gn = Xpq2easy_href [Aij])
      return href_gn;

    return seek_t (Aij, "adj", FALSE /* Le terminal "adj" */);
  }
  
  return 0;
}

static SXINT
seek_Easy_GA (SXINT Pij)
{
  SXINT  Aij, href_gn;

  if (Aij = seek_prefix_Aij ("Easy_GA", Pij)) {
    if (href_gn = Xpq2easy_href [Aij])
      return href_gn;
  }
  
  return 0;
}

static SXINT
seek_Easy_GP (SXINT Pij)
{
  SXINT  Aij, href_gn;

  if (Aij = seek_prefix_Aij ("Easy_GP", Pij)) {
    if (href_gn = Xpq2easy_href [Aij])
      return href_gn;
  }
  
  return 0;
}


static SXINT
seek_Easy_PV (SXINT Pij)
{
  SXINT  Aij, href_gn;

  if (Aij = seek_prefix_Aij ("Easy_PV", Pij)) {
    if (href_gn = Xpq2easy_href [Aij])
      return href_gn;
  }
  
  return 0;
}


static SXINT *NV_stack;



#if 1
/* Il faut reconstituer les AMALGAM et les COMPOUND */
static SXINT
easy_print_Tij_stack (SXINT F)
{
  SXINT  top, x, y, z, if_id, am_id, ste, href_id, Tij, Tij2, tok_no;
  char *str1, *str2, easy_int [8], easy_string [16];
  char *comment;
  struct sxtoken *tok_ptr;

  top = easy_Tij_stack [0];

  for (x = 1; x <= top; x++) {
    Tij = easy_Tij_stack [x];
    tok_no = spf_get_Tij2tok_no_stack (Tij)[1]; /* on ne prend que le premier token :( */
    if_id = SXGET_TOKEN (tok_no).lahead; /* L'if_id a ete stocke en lahead par store_dag() ds dag_scanner.c */

#if 0
    if (X_is_set (&X_amalgam_if_id_hd, if_id))
      /* C'est un element d'un amalgam */
      easy_Tij_stack [x] = -Tij;
#endif /* 0 */
  }

  for (x = 1; x <= top; x++) {
    Tij = easy_Tij_stack [x];
    am_id = 0;

#if 0
    if (Tij < 0) {
      /* On cherche l'amalgam le + long!! */
      Tij = -Tij;
      easy_Tij_stack [x] = Tij; /* Prudence */
      tok_ptr = Tij2next_tok (Tij, 0); /* le premier token de Tij... */
      comment = tok_ptr->comment;
      z = top;

      while (x <= z) {
	XH_push (amalgam_list, tok_ptr->lahead);

	for (y = x+1; y <= z; y++) {
	  if ((Tij2 = easy_Tij_stack [y]) > 0)
	    break;
	
	  Tij2 = -Tij2;
	  easy_Tij_stack [y] = Tij2; /* Prudence */
	  tok_ptr = Tij2next_tok (Tij2, 0); /* le premier token de Tij2... */
	  XH_push (amalgam_list, tok_ptr->lahead);
	}

	if (am_id = XH_is_set (&amalgam_list)) {
	  /* Ici, l'expansion d'un amalgame se trouve dans le meme composant */

	  /* il existe */
	  if (is_print_f_structure) printf ("F%i = \"%s\"", F, amalgam_names [am_id]);

	  if (xml_file) {
	    /* Pour chaque Xpq, on sauve le "href" correspondant */
	    sprintf (easy_string, "E%iF%i", sentence_id, F);

	    if (comment == NULL) {
	      /* Pour "simuler" un source a la easy, sans commentaires */
	      href_id = sxword_save (&easy_hrefs, easy_string);
	      /* On force tous les constituants de l'amalgame Tij a referencer href_id */
	      Xpq2easy_href [spf.outputG.maxxnt + Tij] = href_id;

	      while (++x < y) {
		Tij2 = easy_Tij_stack [x];
		Xpq2easy_href [spf.outputG.maxxnt + Tij2] = href_id;
		tok_ptr = Tij2next_tok (Tij2, 0); /* le premier token de Tij2... */
	      }

#if EBUG
	    }
#endif /* EBUG */
	    fprintf (xml_file,
		     "  <F debug_id=\"%s\">%s</F>\n",
		     easy_string,
		     amalgam_names [am_id]
		     );

#if EBUG
	    if (comment == NULL) {
#endif /* EBUG */
	      sprintf (easy_int, "%i", F);
	      easy_vstr = varstr_catenate (varstr_catenate (varstr_catenate (easy_vstr, "F"), easy_int), " ");
	    }
	  }

	  x = y-1;
	  break;
	}

	z = y-2;
      }
    }
    else
#endif /* 0 */
      {
	tok_no = spf_get_Tij2tok_no_stack (Tij)[1]; /* on ne prend que le premier token :( */
	comment = SXGET_TOKEN (tok_no).comment;
    }
    
    if (am_id == 0) {
      str1 = sxstrget (tok_ptr->string_table_entry);

      /* Les composants d'un COMPOUND sont separes par des '_' */
      /* On ne tient pas compte de ceux commencant par = '_'
	 ni de ceux qui contiennent  '__' */
      if (*str1 != '_') {

	while (str2 = strchr (str1, '_')) {
	  if (str2 [1] == '_')
	    break;

	  *str2 = NUL;
	  if (is_print_f_structure) printf ("F%i = \"%s\", ", F, str1);

	  if (xml_file) {
#if !EBUG
	    if (comment == NULL) {
#endif /* EBUG */
	      fprintf (xml_file,
		       "  <F debug_id=\"E%iF%i\">%s</F>\n",
		       sentence_id,
		       F,
		       str1
		       );

#if EBUG
	      if (comment == NULL) {
#endif /* EBUG */
		sprintf (easy_int, "%i", F);
		easy_vstr = varstr_catenate (varstr_catenate (varstr_catenate (easy_vstr, "F"), easy_int), " ");
	      }
	    }

	    *str2 = '_';
	    str1 = str2+1;
	    F++;
	  }
	}

	if (str2 = strchr (str1, '/'))
	  /* On supprime le suffixe introduit par un '/' Ex le/det/ */
	  *str2 = NUL;

	if (is_print_f_structure) printf ("F%i = \"%s\"", F, str1);
	  
	if (xml_file) {
	  /* on sauve le "href" correspondant au dernier composant d'un mot compose' */
	  sprintf (easy_string, "E%iF%i", sentence_id, F);

	  if (comment == NULL) {
	    Xpq2easy_href [spf.outputG.maxxnt + Tij] = sxword_save (&easy_hrefs, easy_string);
#if EBUG
	  }
#endif /* EBUG */

	  fprintf (xml_file,
		   "  <F debug_id=\"%s\">%s</F>\n",
		   easy_string,
		   str1
		   );

#if EBUG
	  if (comment == NULL) {
#endif /* EBUG */
	    sprintf (easy_int, "%i", F);
	    easy_vstr = varstr_catenate (varstr_catenate (varstr_catenate (easy_vstr, "F"), easy_int), " ");
	  }
	}

	if (str2)
	  *str2 = '/';
      }

      F++;

      if (x < top)
	if (is_print_f_structure) fputs (", ", stdout);
    }

  return F;
}
#endif /* 1 */

/* On est ds le cas if (xml_file) */
/* Version de easy_print_Tij_stack ds laquelle ce sont les commentaires associes aux tokens qui contiennent
   les TAG  xml pour easy */
/* 3 cas principaux :
   1 : un mot donne un mot ; il => {<F id="E1F1">il</F>} il
   2 : un amalgamme ; au => {<F id="E1F1">au</F>} � {<F id="E1F1">au</F>} le__det
   3 : un mot_compose ; � moins que => {<F id="E1F1">�</F> <F id="E1F2">moins</F> <F id="E1F3">que</F>} �_moins_que     
*/
static void
easy_print_Tij_stack_from_comments ()
{
  SXINT  top, x, Tij, href_id, comment_lgth, tok_no;
  char *str1, *str2, *comment;

  top = easy_Tij_stack [0];

  for (x = 1; x <= top; x++) {
    Tij = easy_Tij_stack [x];

    if (Tij <= -spf.outputG.maxt) {
      /* On ne s'occupe pas des "terminaux" de la rcvr */
      tok_no = spf_get_Tij2tok_no_stack (Tij)[1]; /* on ne prend que le premier token :( */
      comment = SXGET_TOKEN (tok_no).comment;

      if (comment) {
	/* Ca permet de tester du texte en mode easy meme sans TAG xml ... */
	comment_lgth = strlen (comment)-1;
	str1 = comment+comment_lgth;
	*str1 = NUL;
      
	fprintf (xml_file, "%s\n", comment+1);

	*str1 = '}';
  
	/* On extrait le "href" */
	/* On prend le + a gauche */
	/* Prudence, on y accede par les ">" et "<"  a cause d'un >"< eventuel ... */
	str1 = strchr (comment+1, (int)'"');
	if (str1) str2 = strchr (str1+1, (int)'"');

	if (str1 && str2) {
	  /*
	              {<F id="E1F1">�</F> <F id="E1F2">moins</F> <F id="E1F3">que</F>} �_moins_que  
	    comment   ^
	    str1             ^
	    str2                  ^
	  */
	  *str2 = NUL;
	  href_id = sxword_save (&easy_hrefs, str1+1);
	  *str2 = '"';
	  Xpq2easy_href [spf.outputG.maxxnt + Tij] = href_id;
	}
      }
    }
  }
}


static SXINT
easy_print_ste_path (SXINT F, SXINT i, SXINT j, SXINT repair_i)
{
  SXINT     triple, k, Tik, new_F;
  BOOLEAN is_crooked;

  if (i == j) {
#if 1
    /* Pour l'instant pour verif ... */
    F = easy_print_Tij_stack (F);
#endif /* 1 */

    if (xml_file)
      easy_print_Tij_stack_from_comments ();
  }
  else {
    if (spf.outputG.has_repair) {
      /* easy_Tij_hd peut etre cyclique ... */
      /* ... mais il y a forcement un chemin de i a j */ 
      is_crooked = FALSE;

      XxYxZ_Xforeach (easy_Tij_hd, i, triple) {
	Tik = XxYxZ_Y (easy_Tij_hd, triple);
	k = XxYxZ_Z (easy_Tij_hd, triple);

	/* Car easy_Tij_hd peut etre cyclique (pas seulement avec des boucles) */
	if (i == k && Tpq2attr [Tik].i >= repair_i) {
	  /* ... oui */
	  /* On ne considere que le 1er */
	  is_crooked = TRUE;
	  PUSH (easy_Tij_stack, Tik);
	  break;
	}
      }

      XxYxZ_Xforeach (easy_Tij_hd, i, triple) {
	Tik = XxYxZ_Y (easy_Tij_hd, triple);
	k = XxYxZ_Z (easy_Tij_hd, triple);

	if (i != k && Tpq2attr [Tik].i >= repair_i) {
	  PUSH (easy_Tij_stack, Tik);
	  new_F = easy_print_ste_path (F, k, j, Tpq2attr [Tik].j);
	  POP (easy_Tij_stack);

	  if (new_F > F) {
	    /* un seul chemin !! */
	    F = new_F;
	    break;
	  }
	}
      }

      if (is_crooked)
	POP (easy_Tij_stack);
    }
    else {
      XxYxZ_Xforeach (easy_Tij_hd, i, triple) {
	Tik = XxYxZ_Y (easy_Tij_hd, triple);

	k = XxYxZ_Z (easy_Tij_hd, triple);
	
	if (k <= j) {
	  /* transition possible */
	  PUSH (easy_Tij_stack, Tik);
	  new_F = easy_print_ste_path (F, k, j, 0);
	  POP (easy_Tij_stack);

	  if (new_F > F) {
	    /* un seul chemin !! */
	    F = new_F;
	    break;
	  }
	}
      }
    }
  }
    
  return F;
}


static SXINT easy_path_nb;

/* easy_constituent_stack contient un chemin complet constituants+terminaux de liaison */
static void
easy_print_constituent_path ()
{
  SXINT  Aij, top, x, A, i, j, F, G;
  char easy_name [4], *str, easy_int [8], easy_string [16];

  if (is_print_f_structure) fputs ("\n", stdout);

  easy_name [2] = NUL;

  top = easy_constituent_stack [0];
  F = G = 1;

  for (x = 1; x <= top; x++) {
    Aij = easy_constituent_stack [x];

    if (Aij <= spf.outputG.maxxnt) {
      if (Aij < 0) {
	/* dummy */
	A = dummy_easy_stack [-Aij];

	if (A == Easy_PV_code)
	  strncpy (easy_name, "PV", 2);
	else
	  strncpy (easy_name, "NV", 2);

	i = dummy_easy_stack [-Aij+2];
	j = dummy_easy_stack [-Aij+3];
      }
      else {
	A = Aij2A (Aij);
	strncpy (easy_name, spf.inputG.ntstring [A]+5, 2);
	i = Aij2i (Aij);
	j = Aij2j (Aij);
      }

      if (i >= 0 && j >= 0) {
	/* Pas des rcvr ou repair */
	/* On cherche un chemin terminal allant de i a j */
	/* dont les formes flechies sont reperees a partir de F */
	if (is_print_f_structure) printf ("G%i : %s%i = [", G, easy_name, G);

	if (xml_file) {
	  sprintf (easy_string, "E%iG%i", sentence_id, G);

	  /* Pour chaque Xpq, on sauve le "href" correspondant */
	  if (Aij > 0)
	    /* A FAIRE ds le cas des dummy !! */
	    Xpq2easy_href [Aij] = sxword_save (&easy_hrefs, easy_string);

	  fprintf (xml_file,
		   "<Groupe type=\"%s\" id=\"%s\">\n",
		   easy_name,
		   easy_string
		   );
	}

	G++; 
	
	F = easy_print_ste_path (F, i, j, 0);
      
	if (is_print_f_structure) fputs ("]\n", stdout);

	if (xml_file)
	  fputs ("</Groupe>\n", xml_file);
      } 
    }
    else {
      PUSH (easy_Tij_stack, Aij - spf.outputG.maxxnt);
#if 1
      /* Il faut traiter les amalgam et compound */
      F = easy_print_Tij_stack (F);
#endif /* 1 */

      if (is_print_f_structure) fputs ("\n", stdout);

      if (xml_file)
	easy_print_Tij_stack_from_comments ();

      POP (easy_Tij_stack);
    }
  }
}

/* On parcourt les constituants consecutifs */
static BOOLEAN
easy_walk (SXINT i, SXINT repair_i)
{
  SXINT     triple, Aij, j, Tij;
  BOOLEAN ret_val, is_crooked;

  if (i == idag.final_state) {
    /* easy_constituent_stack contient un chemin complet (constituants et terminaux de liaison) */
    if (++easy_path_nb == 1) {
      /* Je n'imprime que le 1er chemin ... */
      if (xml_file) {
	fputs ("<constituants>\n", xml_file);
      }

      easy_print_constituent_path ();

      if (xml_file) {
	fputs ("</constituants>\n", xml_file);


	xml_close_tag = TRUE;
      }
    }

    return TRUE;
  } 

  ret_val = FALSE;

  XxYxZ_Xforeach (easy_hd, i, triple) {
    Aij =  XxYxZ_Y (easy_hd, triple);
    /* Attention Aij peut etre negatif ds le cas de dummy constituents */

    /* Attention, easy_hd peut etre cyclique */
    if (Aij < 0 || SXBA_bit_is_reset_set (easy_Aij_constituent_set, Aij)) {
      PUSH (easy_constituent_stack, Aij);
      j = XxYxZ_Z (easy_hd, triple);

      if (easy_walk (j, 0))
	ret_val = TRUE;

      POP (easy_constituent_stack);

      if (Aij > 0)
	SXBA_0_bit (easy_Aij_constituent_set, Aij);

      if (ret_val)
	break;
    }

#if 0
    /* Le nombre de chemins peut etre gigantesque ... */
    /* ... le 22/12/05 abandon, on ne les compte pas */
#if !LOG
    if (ret_val)
      break;
#endif /* !LOG */
#endif /* 0 */
  }

  if (!ret_val) {
    /* On fait une transition terminale ... */
    if (!spf.outputG.has_repair) {
      XxYxZ_Xforeach (easy_Tij_hd, i, triple) {
	Tij =  XxYxZ_Y (easy_Tij_hd, triple);
	PUSH (easy_constituent_stack, spf.outputG.maxxnt+Tij);
	j = XxYxZ_Z (easy_Tij_hd, triple);

	if (easy_walk (j, 0))
	  ret_val = TRUE;

	POP (easy_constituent_stack);

	if (ret_val)
	  /* Le nombre de chemins peut etre gigantesque ... */
	  /* ... le 22/12/05 abandon, on ne les compte pas */
	  break;
      }
    }
    else {
      /* repair ... on regarde s'il y a des Tii ... */
      is_crooked = FALSE;

      XxYxZ_Xforeach (easy_Tij_hd, i, triple) {
	Tij =  XxYxZ_Y (easy_Tij_hd, triple);

	/* Car easy_Tij_hd peut etre cyclique (pas seulement avec des boucles) */
	if (i == XxYxZ_Z (easy_Tij_hd, triple) && Tpq2attr [Tij].i >= repair_i) {
	  /* ... oui */
	  /* On ne considere que le 1er */
	  is_crooked = TRUE;
	  PUSH (easy_constituent_stack, spf.outputG.maxxnt+Tij);
	  break;
	}
      }

      XxYxZ_Xforeach (easy_Tij_hd, i, triple) {
	j = XxYxZ_Z (easy_Tij_hd, triple);
	Tij =  XxYxZ_Y (easy_Tij_hd, triple);

	if (i != j && Tpq2attr [Tij].i >= repair_i) {
	  PUSH (easy_constituent_stack, spf.outputG.maxxnt+Tij);

	  if (easy_walk (j, Tpq2attr [Tij].j))
	    ret_val = TRUE;

	  POP (easy_constituent_stack);

	  if (ret_val)
	    /* Le nombre de chemins peut etre gigantesque ... */
	    /* ... le 22/12/05 abandon, on ne les compte pas */
	    break;
	}
      }

      if (is_crooked)
	POP (easy_constituent_stack);
    }

    /* Faut-il faire Tij_iforeach aussi ? */
    /* Oui le 17/02/06 car on peut avoir du rcvr sur la super_regle non accedee par les tris topologiques. Ex :
       <0> -> $ S [n-1] [n] $ (suppression du dernier symbole), donc pas ds easy_Tij_hd
       or il faut atteindre l'etat final */
    /* Essai le 17/11/04 */
    if (!ret_val) {
      Tij_iforeach (i, Tij) {
	j = Tij2j (Tij); /* j > i */
	PUSH (easy_constituent_stack, spf.outputG.maxxnt+Tij);

	if (easy_walk (j, 0))
	  ret_val = TRUE;

	POP (easy_constituent_stack);

	if (ret_val)
	  break;

#if 0
	/* Le nombre de chemins peut etre gigantesque ... */
	/* ... le 22/12/05 abandon, on ne les compte pas */
#if !LOG
	if (ret_val)
	  break;
#endif /* !LOG */
#endif /* 0 */
      }
    }
  }

  return ret_val;
}


/* On construit Tpq2attr */
static SXINT
build_Tpq2attr ()
{
  SXINT              i, cur_bound, Tpq, Tqr, j, map, ib, p, pb, q, trans2, r, qb, trans, max_bound;
  BOOLEAN          has_cyclic_repair, has_repair;
  struct attr      *attr_ptr;

  max_bound = i2lb [1] = 1;

  for (i = 1; i <= idag.final_state; i++) {
    non_repair_i2lb [i] = cur_bound = i2lb [i];

    if (cur_bound) {
      /* Il se peut que des Tpq soient sautes */
      has_cyclic_repair = has_repair = FALSE;

      /* On regarde s'il y a des Tik qui proviennent de la reparation */
      XxYxZ_Xforeach (easy_Tij_hd, i, trans) {
	Tpq = XxYxZ_Y (easy_Tij_hd, trans);

	if (SXBA_bit_is_set (spf.outputG.Tpq_rcvr_set, Tpq)) {
	  /* oui */
	  j = XxYxZ_Z (easy_Tij_hd, trans);

	  if (i == j) {
	    if (!has_cyclic_repair) {
	      /* On ne prend que le 1er */
	      has_cyclic_repair = TRUE;

	      /* (i, i) => (cur_bound, cur_bound+1) */
	      attr_ptr = Tpq2attr+Tpq;
	      attr_ptr->i = cur_bound;
	      attr_ptr->j = ++cur_bound;
	      attr_ptr->size = 1;

	      max_bound = cur_bound;
	      break; /* 1 seul chemin */
	    }
	  }
	  else {
	    has_repair = TRUE;
	  }
	}
      }

      if (!has_cyclic_repair && has_repair) {
	/* On traite ces Tpq */
	ib = cur_bound;

	XxYxZ_Xforeach (easy_Tij_hd, i, trans) {
	  Tpq = XxYxZ_Y (easy_Tij_hd, trans);

	  if (SXBA_bit_is_set (spf.outputG.Tpq_rcvr_set, Tpq)) {
	    /* oui */
	    p = i;
	    pb = ib;
	    q = XxYxZ_Z (easy_Tij_hd, trans);

	    if (p != q) {
	      while (q > idag.final_state) {
		/* (p, q) => (pb, cur_bound+1) */
		attr_ptr = Tpq2attr+Tpq;
		attr_ptr->i = pb;
		attr_ptr->j = ++cur_bound;
		attr_ptr->size = 1;

		XxYxZ_Xforeach (easy_Tij_hd, q, trans2) {
		  /* trans unique */
		  break;
		}
	
		p = q;
		pb = cur_bound;

		Tpq = XxYxZ_Y (easy_Tij_hd, trans2);
		q = XxYxZ_Z (easy_Tij_hd, trans2);
	      }

	      /* On ne conserve qu'un chemin */
	      if ((qb = i2lb [q]) == 0 /* 1ere fois */ || qb < cur_bound+1 /* on choisit le + long */)
		max_bound = qb = i2lb [q] = cur_bound+1;

	      attr_ptr = Tpq2attr+Tpq;
	      attr_ptr->i = pb;
	      attr_ptr->j = qb;
	      attr_ptr->size = 1;
	    
	      break;
	    }
	  }
	}
      }

      /* on traite les normaux */
      ib = cur_bound;
    
      XxYxZ_Xforeach (easy_Tij_hd, i, trans) {
	Tpq = XxYxZ_Y (easy_Tij_hd, trans);

	if (!SXBA_bit_is_set (spf.outputG.Tpq_rcvr_set, Tpq)) {
	  /* normal */
	  q = XxYxZ_Z (easy_Tij_hd, trans);
	  qb = ib+1;

	  if (i2lb [q] < qb)
	    max_bound = i2lb [q] = qb;
	  else
	    qb = i2lb [q];

	  /* (i, q) => (ib, qb) */
	  attr_ptr = Tpq2attr+Tpq;
	  attr_ptr->i = ib;
	  attr_ptr->j = qb;
	  attr_ptr->size = 1;
	}
      }
    }
  }

  return max_bound;
}


static void
easy_header ()
{
  extern char	*ctime ();
  long		date_time;

#if 0
  /* Cette entete sera produite par un wrappeur */
  fputs ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", xml_file);
#endif /* 0 */
    
  if (is_print_headers) {
    date_time = time (0);
    
    fprintf (xml_file,
	     "\n\
<!-- ********************************************************************\n\
\tThe file \"%s\" contains the XML format of the input sentence #%i\n",
	     xml_file_name,
	     sentence_id
	     );
    
    if (input_sentence_string)
      fprintf (xml_file,
	       "\t\"%s\"\n",
	       input_sentence_string
	       );
    
    fprintf (xml_file,
	     "\twhich has been generated on %s\
\tby the SYNTAX(*) LFG processor SXLFG(*)\n\
     ********************************************************************\n\
\t(*) SYNTAX and SXLFG are trademarks of INRIA.\n\
     ******************************************************************** -->\n\n",
	     ctime (&date_time)
	     );
  }
#if 0
  /* Cette commande sera produite par un wrappeur */
  fprintf (xml_file,
	   "<DOCUMENT fichier=\"%s\">\n",
	   xml_file_name);
#endif /* 0 */
  
  fprintf (xml_file,
	   "<E id=\"E%i\">\n",
	   sentence_id);
}

/* On repere les constituants */
static void
easy_constituent ()
{
  static SXINT      easy_hd_foreach [6] = {1, 0, 0, 0, 0, 0};
  static SXINT      easy_Tij_hd_foreach [6] = {1, 0, 0, 0, 0, 0};
  static SXINT      dummy_easy_hd_foreach [6] = {0, 0, 0, 0, 0, 0};
  SXINT             nt, t, lgth, max_bound;
  char            *nt_string, *t_string;

  easy_constituent_nt_set = sxba_calloc (spf.inputG.maxnt+1);
  easy_Aij_constituent_set = sxba_calloc (spf.outputG.maxxnt+1);
  XxYxZ_alloc (&easy_hd, "easy_hd", idag.final_state+1, 1, easy_hd_foreach, NULL, NULL);
  XxYxZ_alloc (&easy_Tij_hd, "easy_Tij_hd", idag.final_state+1, 1, easy_Tij_hd_foreach, NULL, NULL);
  easy_constituent_stack = (SXINT*) sxalloc (spf.outputG.maxxnt-spf.outputG.maxt+1, sizeof (SXINT)), easy_constituent_stack [0] = 0;
  easy_Tij_stack = (SXINT*) sxalloc (-spf.outputG.maxt+1, sizeof (SXINT)), easy_Tij_stack [0] = 0;

  Aij2attr = (struct attr*) sxalloc (spf.outputG.maxxnt+1, sizeof (struct attr));
  dummy_Aij2attr = (struct attr*) sxalloc (spf.outputG.maxxnt+1, sizeof (struct attr));
  DALLOC_STACK (dummy_easy_stack, 10*6);

  for (nt = 1; nt <= spf.inputG.maxnt; nt++) {
    nt_string = spf.inputG.ntstring [nt];

    if (strlen (nt_string) > 5 && strncmp (nt_string, "Easy_", 5) == 0
      /* C'est un easy ... */
      /* ... mais est-ce un constituant ? */
	&& strlen (nt_string+5) >= 2 && strncmp (nt_string+5, "00", 2) != 0 && strncmp (nt_string+5, "ZZ", 2) != 0)
      SXBA_1_bit (easy_constituent_nt_set, nt);
  }
  
  if (spf.outputG.start_symbol) {
    if (xml_file)
      easy_header ();

    Easy_sorted_Aij = (SXINT*) sxalloc (spf.outputG.maxxnt+1, sizeof (SXINT)), Easy_sorted_Aij [0] = 0;

#if LOG
    fputs ("\npi = easy_td_walk\n", stdout);
#endif /* LOG */

    spf_topological_walk (spf.outputG.start_symbol, easy_td_walk, NULL);

    if (spf.outputG.has_repair) {
      Tpq2attr = (struct attr*) sxcalloc (-spf.outputG.maxt+1, sizeof (struct attr));
      i2lb = (SXINT*) sxcalloc (spf.outputG.repair_mlstn_top+1, sizeof (SXINT));
      non_repair_i2lb = (SXINT*) sxcalloc (spf.outputG.repair_mlstn_top+1, sizeof (SXINT));

      build_Tpq2attr ();

      max_bound = spf.outputG.repair_mlstn_top;

      sxfree (i2lb), i2lb = NULL;
    }
    else
      max_bound = idag.final_state;

    easy_range_set = sxba_calloc (max_bound+1);

    /* Nelle version du 10/02/06, on propage les tailles ds tous les cas */
    easy_upper_bound = (SXINT *) sxcalloc (spf.outputG.maxxnt+1, sizeof (SXINT));

#if LOG
    fputs ("\npd = dummy_easy_bu_walk\n", stdout);
#endif /* LOG */

    spf_topological_walk (spf.outputG.start_symbol, NULL, dummy_easy_bu_walk);

    sxfree (easy_upper_bound), easy_upper_bound = NULL;

    Easy_maximal_constituent ();

    sxfree (Easy_sorted_Aij), Easy_sorted_Aij = NULL;

    /* Il faut chercher les compositions sequentielles */
    easy_walk (1, 0);

#if EBUG
    if (xml_file && !xml_close_tag)
      sxtrap (ME, "easy_constituent");
#endif /* EBUG */
    fputs ("</E>\n", xml_file);
  }

  sxfree (easy_constituent_nt_set), easy_constituent_nt_set = NULL;
  sxfree (easy_range_set), easy_range_set = NULL;
  sxfree (easy_Aij_constituent_set), easy_Aij_constituent_set = NULL;
  XxYxZ_free (&easy_hd);
  XxYxZ_free (&easy_Tij_hd);
  sxfree (easy_constituent_stack), easy_constituent_stack = NULL;
  sxfree (easy_Tij_stack), easy_Tij_stack = NULL;

  sxfree (Aij2attr), Aij2attr = NULL;
  sxfree (dummy_Aij2attr), dummy_Aij2attr = NULL;
  DFREE_STACK (dummy_easy_stack);

  if (spf.outputG.has_repair) {
    sxfree (Tpq2attr), Tpq2attr = NULL;
    sxfree (non_repair_i2lb), non_repair_i2lb = NULL;
  }
}






static void
easy_campaign ()
{
  SXINT  sentence_no;
  char string [16];

#if 0
  X_reuse (&X_amalgam_if_id_hd, "X_amalgam_if_id_hd", NULL, NULL);
  XH_unlock (amalgam_list);
#endif /* 0 */

  easy_vstr = varstr_alloc (128);
  sxword_alloc (&easy_hrefs, "easy_hrefs", Xpq_top+2, 1, 8,
		sxcont_malloc, sxcont_alloc, sxcont_realloc, NULL, sxcont_free, NULL, NULL);
  Xpq2easy_href = (SXINT*) sxcalloc (Xpq_top+2, sizeof (SXINT));
  NV_stack = (SXINT*) sxalloc (2*spf.inputG.maxrhs+1, sizeof (SXINT)); 

  if (is_print_f_structure) {
    /*    sentence_no = easy_get_sentence_no ();

    if (sentence_no != 0 && sentence_no != sentence_id)
      sentence_id = sentence_no;
      */

    sprintf (string, "E%i", sentence_id);
    Ei_lgth = strlen (&(string [0]));
  }

  easy_constituent ();

  varstr_free (easy_vstr), easy_vstr = NULL;
  sxword_free (&easy_hrefs);
  sxfree (Xpq2easy_href), Xpq2easy_href = NULL;
  sxfree (NV_stack), NV_stack = NULL;
}
#endif /* EASY */

static void
xml_gen_header ()
{
    extern char	*ctime ();
    long		date_time;
    
    date_time = time (0);

    if (is_print_headers) {
      fprintf (xml_file,
	       "\n\
<!-- ********************************************************************\n\
\tThe file \"%s\" contains the LFG parse forest in XML format\n\
\twhich has been generated on %s\
\tby the SYNTAX(*) LFG processor SXLFG(*)\n\
     ********************************************************************\n\
\t(*) SYNTAX and SXLFG are trademarks of INRIA.\n\
     ******************************************************************** -->\n\n",
	       xml_file_name, ctime (&date_time));
    }
}

static SXINT
xml_print_terminal_leaves ()
{
  SXINT  Tpq, T, p, q, trans, tok_no, t_nb, i;
  SXINT  *tok_no_stack, *top_tok_no_stack;
  char   *terminal_string, *short_tstring, *c;

  fputs ("  <terminals>\n", xml_file); 

  t_nb = 0;

  for (Tpq = 1; Tpq <= maxxt; Tpq++) {
    /* T est un terminal */
    T = -Tij2T (Tpq);

    p = Tij2i (Tpq);
    q = Tij2j (Tpq);

    short_tstring = strdup (spf.inputG.tstring [T]);
    c = strchr(short_tstring,'_');
    if (c)
      *c = NUL;

    if (p >= 0 && q >= 0) {
      tok_no_stack = spf_get_Tij2tok_no_stack (Tpq);
      top_tok_no_stack = tok_no_stack + *tok_no_stack;

      while (++tok_no_stack <= top_tok_no_stack) {
	tok_no = *tok_no_stack;
	/* La transition tok_no entre p et q contient bien le terminal T */
	terminal_string = sxstrget (SXGET_TOKEN (tok_no).string_table_entry);
	t_nb++;
	fprintf (xml_file,
		 "    <terminal id=\"T%i\" name=\"%s\" realName=\"%s\" form=\"%s\" lb=\"%i\" ub=\"%i\"/>\n",
		 Tpq,
		 short_tstring,
		 spf.inputG.tstring [T],
		 terminal_string,
		 p,
		 q
		 );

      }
#if 0
      /* p et q sont des mlstn */
      p = mlstn2dag_state (p);
      q = mlstn2dag_state (q);

      /* En "standard" on dispose de X et Zforeach sur dag_hd (voir sxearley_main.c) */
      XxYxZ_Xforeach (dag_hd, p, trans) {
	if (XxYxZ_Z (dag_hd, trans) == q) {
	  tok_no = XxYxZ_Y (dag_hd, trans);

	  if (SXBA_bit_is_set (glbl_source [tok_no], T)) {
	    /* La transition tok_no entre p et q contient bien le terminal T */
	    terminal_string = sxstrget (toks_buf [tok_no].string_table_entry);
	    t_nb++;
	    fprintf (xml_file,
		     "    <terminal id=\"T%i\" name=\"%s\" realName=\"%s\" form=\"%s\" lb=\"%i\" ub=\"%i\"/>\n",
		     Tpq,
		     short_tstring,
		     spf.inputG.tstring [T],
		     terminal_string,
		     p,
		     q
		     );
	  }
	}
      }
#endif /* 0 */
    }
    else {
      /* Cas d'un xt */
      t_nb++;
      fprintf (xml_file,
	       "    <terminal id=\"T%i\" rcvr=\"+\" name=\"%s\" realName=\"%s\" lb=\"%i\" ub=\"%i\"/>\n",
	       Tpq,
	       short_tstring,
	       spf.inputG.tstring [T],
	       p,
	       q
	       );
      
    }
  } 

  fputs ("  </terminals>\n", xml_file); 

  return t_nb;
}


static void
xml_print_nt_set (SXBA Aij_set)
{
  SXINT Aij, hook, prod;
  
  fputs ("  <non_terminals>\n", xml_file); 

  Aij = 0;

  while ((Aij = sxba_scan (Aij_set, Aij)) > 0) {
    if (Aij <= spf.outputG.maxnt) {
      if (Aij2i(Aij) < Aij2j(Aij)) { /* on n'affiche pas les Aij vides */
	/* nt */
	fprintf (xml_file,
		 "    <non_terminal id=\"N%i\" name=\"%s\" lb=\"%i\" ub=\"%i\">\n",
		 Aij,
		 spf.inputG.ntstring [Aij2A (Aij)],
		 Aij2i (Aij),
		 Aij2j (Aij)
		 );
	
	hook = spf.outputG.lhs [spf.outputG.maxxprod+Aij].prolon;
	while ((prod = spf.outputG.rhs [hook++].lispro) != 0) {
	  /* On parcourt les productions dont la lhs est Aij */
	  if (prod > 0)
	    /* Non vide */
	    fprintf (xml_file, "      <rule idref=\"E%iR_%i_%i\"/>\n", sentence_id, prod, spf.outputG.lhs [prod].init_prod);
	}
	fputs("    </non_terminal>\n",xml_file);
      }
    }
    else {
      /* xnt */
      fprintf (xml_file,
	       "    <non_terminal id=\"N%i\" rcvr=\"+\" name=\"%s\" lb=\"%i\" ub=\"%i\"/>\n",
	       Aij,
	       spf.inputG.ntstring [Aij2A (Aij)],
	       Aij2i (Aij) /* -1 => undef */,
	       Aij2j (Aij) /* -1 => undef */
	       );
      /* On enleve les xnt pour la suite */
      SXBA_0_bit (Aij_set, Aij);
    }
  }

  fputs ("  </non_terminals>\n", xml_file); 
}

static SXINT offset;

static void
xml_print_terminal (SXINT Tij)
{
  SXINT i;
  char *comment, *str1, *str2, *str3;
  SXINT local_f=0, last_f=0, tok_no;
  struct sxtoken *tok_ptr;

  for (i=0; i<offset; i++)
    fputs("  ",xml_file);
  fprintf(xml_file,"<T id=\"E%iT%i\" name=\"%s\">\n",sentence_id, Tij,spf.inputG.tstring[-Tij2T (Tij)]);

  for (i=0; i<=offset; i++)
    fputs("  ",xml_file);

  if (!SXBA_bit_is_set (spf.outputG.Tpq_rcvr_set, Tij)) {
    tok_no = spf_get_Tij2tok_no_stack (Tij)[1]; /* on ne prend que le premier token :( */
    comment = SXGET_TOKEN (tok_no).comment;
    
    if (comment) {
      comment++;
      for (comment; *comment != NUL; comment++) {
	str1 = strchr(comment,(int)'F');
	str2 = strchr(str1+3,(int)'F');
	str1 /* str3 */ = strchr(str2,(int)'"');
	*str1=NUL;
	local_f=atoi(str2+1);
	*str1='"';
	if (local_f > last_f) {
	  last_f=local_f;
	  for (comment; *comment != '}'; comment++){
	    if (*comment == '&')
	      fprintf(xml_file,"&amp;");
	    else
	      fprintf(xml_file,"%c",*comment);
	  }
	} else {
	  comment = strchr(comment,(int)'>');
	  comment = strchr(comment+1,(int)'>');
	  comment+=2;
	}
      }
      fputs("\n",xml_file);
    } else { // pas de commentaires: on sort la ff dans un commentaire XML
      fprintf(xml_file,"<!-- %s -->\n",sxstrget (tok_ptr->string_table_entry));
    }
  } else {
      fprintf(xml_file,"<!-- (error) -->\n");
  }
  for (i=0; i<offset; i++)
    fputs("  ",xml_file);
  fputs("</T>\n",xml_file);
}

static void
xml_print_unique_tree (SXINT Aij)
{
  SXINT Pij, Xpq, i, hook;
  struct lhs *plhs;
  struct rhs *prhs;
  char* ntstring;
  BOOLEAN hide = FALSE;

  if (Aij2i (Aij) == Aij2j (Aij) && Aij2i (Aij) > 0)
    return;

  ntstring = spf.inputG.ntstring[Aij2A (Aij)];
  if (ntstring[0] == '[')
    hide = TRUE;

  if (strlen(ntstring) > 6 && strncmp(ntstring, "DISJ__", 6) == 0)
    hide = TRUE;

  if (strlen(ntstring) > 7 && strncmp(ntstring + strlen(ntstring) - 7, "___PLUS", 7) == 0)
    hide = TRUE;
  if (strlen(ntstring) > 7 && strncmp(ntstring + strlen(ntstring) - 7, "___STAR", 7) == 0)
    hide = TRUE;

  hook = spf.outputG.lhs [spf.outputG.maxxprod+Aij].prolon; 
  while ((Pij = spf.outputG.rhs [hook++].lispro) < 0);
  if (Pij == 0) {
    return;
#if EBUG
    sxtrap(ME,"xml_print_unique_tree: emtpy node found");
#endif /* EBUG */
  }

  if (!hide) {
    for (i=0; i<offset; i++)
      fputs("  ",xml_file);
    fprintf(xml_file,"<node name=\"%s\" lb=\"%i\" ub=\"%i\" id=\"E%iR_%i_%i\">\n", ntstring, Aij2i (Aij), Aij2j (Aij), sentence_id, Pij, spf.outputG.lhs [Pij].init_prod);
    offset++;
  }

  plhs = spf.outputG.lhs+Pij; /* on accede a l'entree de la prod dans lhs */
  prhs = spf.outputG.rhs+plhs->prolon; /* on accede a la partie droite de la prod dans rhs */
  while ((Xpq = (prhs++)->lispro) != 0) /* on parcourt la partie droite */
    if (Xpq < 0) /* terminal */
      xml_print_terminal (-Xpq);
    else
      xml_print_unique_tree (Xpq);

  if (!hide) {
    offset--;
    for (i=0; i<offset; i++)
      fputs("  ",xml_file);
    fprintf(xml_file,"</node>\n");
  }

}

static void
xml_print_forest ()
{
  SXINT  Aij, hook, prod, item, Xpq, t_nb;
  SXBA prod_set, Aij_set;

  prod_set = sxba_calloc (spf.outputG.maxxprod+1);
  Aij_set = sxba_calloc (spf.outputG.maxxnt+1);

  fprintf (xml_file,
	   "<parse_forest t_nb=\"%i\" nt_nb=\"%i\" prod_nb=\"%i\">\n",
	   t_nb,
	   sxba_cardinal (Aij_set),
	   sxba_cardinal (prod_set)
	   );

  fprintf (xml_file, "  <axiom idref=\"E%iN%i\"/>\n",sentence_id, spf.outputG.start_symbol);

  for (Aij = 1; Aij <= spf.outputG.maxnt; Aij++) {
    /* nt ... */
    if (Aij2i (Aij) < Aij2j (Aij)) {
      /* ... non vide */
      /* On ne sort ni les nt vides ni leurs nt_prod */
      hook = spf.outputG.lhs [spf.outputG.maxxprod+Aij].prolon;

      while ((prod = spf.outputG.rhs [hook].lispro) != 0) {
	/* On parcourt les productions dont la lhs est Aij */
	if (prod > 0) {
	  SXBA_1_bit (prod_set, prod);
	  SXBA_1_bit (Aij_set, Aij);

	  /* On capture aussi les xnt */
	  item = spf.outputG.lhs [prod].prolon;

	  while ((Xpq = spf.outputG.rhs [item++].lispro) != 0) {
	    if (Xpq > 0)
	      /* nt ou xnt */
	      SXBA_1_bit (Aij_set, Xpq);
	  }
	}

	hook++;
      }
    }
  }

  t_nb = xml_print_terminal_leaves ();

  /* Sortir les non-terminaux */
  xml_print_nt_set (Aij_set);
  /* Les xnt ont ete enleves de Aij_set */
  
  fputs ("  <rules>\n", xml_file); 

  prod = 0;

  while ((prod = sxba_scan (prod_set, prod)) > 0) {
    item = spf.outputG.lhs [prod].prolon;
    Aij = spf.outputG.lhs [prod].lhs;

    fprintf (xml_file,
	     "    <rule id=\"E%iR_%i_%i\">\n",
	     sentence_id,
	     prod,
	     spf.outputG.lhs [prod].init_prod);

    spf_print_iprod (xml_file, prod, "      <!-- ", " -->\n");

    fprintf (xml_file, "      <lhs_node idref=\"E%iN%i\"/>\n", sentence_id, Aij);

    while ((Xpq = spf.outputG.rhs [item++].lispro) != 0) {
      /* On parcourt la RHS de prod */
      if (Xpq > 0) {
	/* nt ... */
	if (Xpq > spf.outputG.maxnt || Aij2i (Xpq) < Aij2j (Xpq)) {
	  /* ... non vide ou xnt */
	  fprintf (xml_file, "      <rhs_node idref=\"E%iN%i\"/>\n", sentence_id, Xpq);
	}
      }
      else {
	/* T */
	fprintf (xml_file, "      <rhs_node idref=\"E%iT%i\"/>\n", sentence_id, -Xpq);
      }
    }

    fputs ("    </rule>\n", xml_file);
  }

  fputs ("  </rules>\n", xml_file); 

  fputs ("</parse_forest>\n", xml_file);

  sxfree (prod_set);
  sxfree (Aij_set);
}


static void
xml_escape_fprintf (FILE *file, char *string)
{
  SXINT length = strlen (string);

  while (length) {
    if (*string == '&') {
      fprintf (file, "&amp;");
    } else if (*string == '<') {
      fprintf (file, "&lt;");
    } else if (*string == '>') {
      fprintf (file, "&gt;");
    } else {
      fprintf (file, "%c", *string);
    }
    length--;
    string++;
  }
}

static void
xml_output (BOOLEAN print_unique_tree)
{
  SXINT tok_no, last_tok_no /* = valeur myst�rieuse A FAIRE!!!!!!!!!! */;

  if (is_print_xml_headers) {
    fputs ("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n", xml_file); 
    fputs ("<DOCUMENT parserGenerator=\"sxlfg\">\n", xml_file); 
  }

  xml_gen_header ();

  fprintf (xml_file, "<E sid=\"E%i\"", sentence_id); 
  /* if (on veut l'input comme attribut de l'�l�ment E) { */
  fprintf (xml_file, " input=\"");

  for (tok_no = 1; tok_no <= idag.toks_buf_top; tok_no++) {
    xml_escape_fprintf (xml_file, sxstrget (SXGET_TOKEN (tok_no).string_table_entry));
    fprintf (xml_file, "%s", tok_no == last_tok_no ? "" : " ");
  }

#if 0
  for (tok_no = 1; tok_no <= last_tok_no; tok_no++) {
    xml_escape_fprintf (xml_file, sxstrget (toks_buf [tok_no].string_table_entry));
    fprintf (xml_file, "%s", tok_no == last_tok_no ? "" : " ");
  }
#endif /* 0 */

  fprintf (xml_file, "\" rcvr=\"%i\" consistent=\"%i\"", spf.outputG.is_error_detected, is_relaxed_run ? 0 : is_consistent); 
  /* } */
  fprintf (xml_file, ">\n"); 

  if (print_unique_tree) {
    fputs("<c_structure>\n",xml_file);
    offset = 1;
    xml_print_unique_tree (spf.outputG.start_symbol);
    fputs("</c_structure>\n",xml_file);
  }
  else
    xml_print_forest ();

  fputs("</E>\n",xml_file);
  
  if (is_print_xml_headers)
    fputs ("</DOCUMENT>\n", xml_file); 
}
#endif /* !no_lfg */

extern SXINT print_chunks (SXINT Pij) ;

extern SXINT print_props (SXINT Pij) ;


static void
dag_output ()
{
  if (spf.outputG.is_error_detected)
    compact_infos[1]='R';

  spf_yield2dfa (FALSE /*= DAG ; TRUE = UDAG */);
}

#ifndef no_chunker
extern char* ntstring2chunksname(char* ntstring);

static void
chunks_output ()
{
  fprintf(xml_file,"<E sid=\"E%i\">\n",sentence_id);
  fprintf(xml_file,"<constituants>\n");
  nl_done=TRUE;
  spf_output_partial_forest (ntstring2chunksname, "Groupe", "G");
  //  spf_topological_walk (spf.outputG.start_symbol,NULL,print_chunks);
  fprintf(xml_file,"</constituants>\n");
  fprintf(xml_file,"</E>\n");
}
#endif /* !no_chunker */

#ifndef no_proper
extern char* ntstring2propsname (char* ntstring);

static void
propositions_output ()
{
  SXINT i;

  rec_level=0;

  fprintf(xml_file,"<E sid=\"E%i\">\n",sentence_id);
  fprintf(xml_file,"<propositions>\n");
  nl_done=TRUE;
  spf_output_partial_forest (ntstring2propsname, "Proposition", "P");
  /*  spf_output_partial_forest (ntstring2propname, "Proposition", "P", 1);*/
  fprintf(xml_file,"</propositions>\n");
  fprintf(xml_file,"</E>\n");
}
#endif /* !no_proper */

static void
props_output ()
{
  fprintf(xml_file,"<E sid=\"E%i\">\n",sentence_id);
  nl_done=TRUE;
  spf_topological_walk (spf.outputG.start_symbol,NULL,print_props);
  fprintf(xml_file,"</E>\n");
}

static void
phrases_output ()
{
  SXINT i;

  rec_level=0;

  fprintf(xml_file,"<E sid=\"E%i\">\n",sentence_id);
  nl_done=TRUE;
  spf_output_partial_forest (ntstring2phrasename, "Syntagme", "S");
  fprintf(xml_file,"</E>\n");
}

static SXINT
output_sem_pass ()
{
  char msg[30];
  SXINT sentence_no;

  /*  extern void print_c_structures();
      spf_topological_walk (spf.outputG.start_symbol, print_c_structures, NULL);*/

  sentence_no = easy_get_sentence_no ();
  if (sentence_no != 0)
    /* On force sentence_id */
    sentence_id = sentence_no;
  else
    sentence_id = 0;

  maxxt = -spf.outputG.maxt;

  switch (output_sem_pass_arg) {
  case 'd':
    if (odag_file_name) {
      if ((odag_file = sxfopen (odag_file_name, "w")) == NULL) {
	fprintf (sxstderr, "%s: Cannot open (create) ", ME);
	sxperror (odag_file_name);
	return FALSE;
      }
    } else
    odag_file=stdout;
    break;
  default:
    if (xml_file_name) {
      if ((xml_file = sxfopen (xml_file_name, "w")) == NULL) {
	fprintf (sxstderr, "%s: Cannot open (create) ", ME);
	sxperror (xml_file_name);
	return FALSE;
      }
    } else
      xml_file=stdout;
    break;
  }

  switch (output_sem_pass_arg) {
  case 'f':
    spf_print_td_forest(stdout, spf.outputG.start_symbol);
    break;
  case 'd':
    dag_output();
    break;
#ifndef no_proper
  case 'p':
       propositions_output();
/*     props_output(); */
    break;
#else /* !no_proper */
    sxtrap(ME,"output_sem_pass (this executable has been compiled with the -Dno_proper option ; you can't call the proper output)");
#endif /* !no_proper */
  case 'S':
    phrases_output();
    break;
  case 'E':
#ifndef no_chunker
    chunks_output();
#else /* !no_chunker */
    sxtrap(ME,"output_sem_pass (this executable has been compiled with the -Dno_chunker option ; you can't call the chunker output)");
#endif /* !no_chunker */
    break;
  default:
    sxtrap(ME,"output_sem_pass (unknown action for output_semact module)");
    break;
  }

#if LOG
  printf ("\n ******* Tagged input sentence with f_structure filtering:\n");
  spf_yield2dfa (FALSE /*= DAG ; TRUE = UDAG */);

  if (is_print_time) {
    sprintf(msg, "\tOutput (%c)", output_sem_pass_arg);
    sxtime (ACTION,msg);
  }

  return 1;
}

void
output_sem_final () {
  if (xml_file && xml_file != stdout)
    fclose(xml_file);
  if (odag_file && odag_file != stdout)
    fclose(odag_file);
}

void
output_semact ()
{
  for_semact.sem_pass = output_sem_pass;
  for_semact.process_args = output_args_decode; /* decodage des options de output */
  for_semact.string_args = output_args_usage; /* decodage des options de output */
  for_semact.ME = output_ME; /* decodage des options de output */
  for_semact.sem_final = output_sem_final;
}

