# AC_PATH_SXLFGCORE
# Test for sxlfg-core installation
# defines sxlfgcoredir
AC_DEFUN([AC_PATH_SXLFGCORE], [
	AC_MSG_CHECKING(for SxLFG core)

	AC_ARG_WITH(
		sxlfgcoresrc,
		AC_HELP_STRING(
		       --with-sxlfgcoresrc=DIR,
		       path where sxlfg-core directory is installed
		),
		sxlfgcoresrc=$withval,
		if test -d $datadir/sxlfg-core; then
			sxlfgcoresrc=$datadir/sxlfg-core
		else
			if test -d /usr/local/share/sxlfg-core; then
				sxlfgcoresrc=/usr/local/share/sxlfg-core
			else
				if test -d /usr/share/sxlfg-core; then
					sxlfgcoresrc=/usr/share/sxlfg-core
				fi
			fi
		fi
	)

	AC_ARG_WITH(
		sxlfgcoreincl,
		AC_HELP_STRING(
			--with-sxlfgcoreincl=DIR,
			sxlfgcore include directory
		),
		sxlfgcoreincl=$withval,
		if test -d /usr/local/include/sxlfg-core; then
			sxlfgcoreincl=/usr/local/include/sxlfg-core
		else
			AC_MSG_ERROR("sxlfgcore include directory not found")
		fi
	)

	AC_ARG_WITH(
		sxlfgcorebin,
		AC_HELP_STRING(
			--with-sxlfgcorebin=DIR,
			sxlfg-core bin directory
		),
		sxlfgcorebin=$withval,
		if test -d /usr/local/bin && test -x /usr/local/bin/disamb; then
			sxlfgcorebin=/usr/local/bin
		else
			AC_MSG_ERROR("bin executables not found")
		fi
	)

	AC_SUBST(sxlfgcoresrc)
	AC_SUBST(sxlfgcorebin)
	AC_SUBST(sxlfgcoreincl)
	AC_MSG_RESULT(yes
		\$sxlfgcorebin = $sxlfgcorebin
		\$sxlfgcoresrc = $sxlfgcoresrc
		\$sxlfgcoreincl = $sxlfgcoreincl)

])
