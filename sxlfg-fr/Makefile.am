AUTOMAKE_OPTIONS = foreign

SXSRC=$(syntaxsrc)
SXINCL=$(syntaxincl)
#SXETCINCL=$(syntaxetcincl)

EMBIN=/Users/bsagot/Documents/Travail/chaines/lingwb/error_mining/trunk

BIBS= $(syntaxlib)
BIBS_DEBUG= $(syntaxlibdir)/libsx.debug.a

#   EBUG4		Verifie la coherence de l'analyse (sxtrap).
#   EBUG3		Imprime les clauses instanciees
#   EBUG2		Permet de connaitre qq quantites (nb d'appels de predicats, de clauses, ...)
#			Ds le cas mapping inverse de 2var-form ne'cessite le de'pliage de la fore^t partage'e
#   EBUG		Imprime le deroulement d'une analyse

GCC=gcc

CC=$(GCC) -I$(SXINCL) -I$(sxlfgcoreincl) -I./incl -c 
CC_O2=$(GCC) -I$(SXINCL) -I$(sxlfgcoreincl) -I./incl -O2 -c
CC_DEBUG=$(GCC) -I$(SXINCL) -I$(sxlfgcoreincl) -I./incl -ggdb -c
#PREPRO=cd $(LFG); umask 7; exec gcc-2.96 -I$(SXINCL) -I$(sxlfgcoreincl) -Iincl -Wp,-C,-dD,-dM -E -x c


#Options de compil pour le type de source
NODAG_NOSDAG =	-Dis_dag=0		\
		-Dis_sdag=0
NODAG_SDAG =	-Dis_dag=0		\
		-Dis_sdag=1
DAG_NOSDAG =	-Dis_dag=1		\
		-Dis_sdag=0
DAG_SDAG =	-Dis_dag=1		\
		-Dis_sdag=1
# ...  et la lexicalisation
noSA_noCRS =	
noSA_CRS =	-Dis_check_rhs_sequences=1
SA_noCRS =	-Dis_set_automaton=1
SA_CRS =	-Dis_set_automaton=1	\
		-Dis_check_rhs_sequences=1
LEX =	-Dis_lex=1

LC_COLLATE = C
LC_CTYPE = C
LC_ALL = C
LANG = C



all:	sxlfg-fr
debug:	sxlfg-fr.d

.PRECIOUS: spec/sxlfg-fr.tpl spec/sxlfg-fr.cpl spec/sxlfg-fr.lfg_lex spec/easy.lfg_lex	\
		spec/test.lfg_lex lib/%.o src/%_logprob.c %_weights2proba %.stats

#######################################################################################################
### partie dépendante de la grammaire ###

# nouvelle grammaire LFG du français ("grammaire de Benoît", qui inclut le "chunker de Benoît")
spec/sxlfg-fr.tpl: $(lefffdir)/templates
	@echo
	@echo "### Creating templates for sxlfg-fr"
	grep -v "/xlfg" $< | perl -pe "s/\/sxlfg\///" > $@

spec/sxlfg-fr.lfg_lex: $(sxlfgcorebin)/lex2sxlfg.pl $(lefffdir)/*.lex spec/sxlfg-fr.tpl
	@echo
	@echo "### Creating lexicon for sxlfg-fr"
	$(sxlfgcorebin)/lex2sxlfg.pl $(lefffdir) -tpl=spec/sxlfg-fr.tpl -vbl=spec/sxlfg-fr.vbl -sxlfg-fr > $@

# ancienne version de la nouvelle grammaire LFG du français ("grammaire de Benoît", qui inclut le "chunker de Benoît")
# beasy : tout est manuel

# grammaire LFG du français qui est issue de la grammaire utilisée pour EASy, elle-même descendante de
# la "grammaire de Lionel"
spec/easy.lfg_lex: $(sxlfgcorebin)/lex2sxlfg.pl $(lefffdir)/*.lex
	@echo
	@echo "### Creating lexicon for easy"
	$(sxlfgcorebin)/lex2sxlfg.pl $(lefffdir) -easy -tpl=spec/easy.tpl -vbl=spec/easy.vbl > $@


# test (utilise sxlfg-fr.tpl)
spec/test.lfg_lex: $(sxlfgcorebin)/lex2sxlfg.pl $(lefffdir)/*.lex spec/sxlfg-fr.tpl
	@echo
	@echo "### Creating lexicon for sxlfg-fr"
	$(sxlfgcorebin)/lex2sxlfg.pl $(lefffdir) -tpl=spec/sxlfg-fr.tpl -vbl=spec/test.vbl > $@



#######################################################################################################
### autogenese ###

spec/%.scats: spec/%.lfg_lex
	@echo
	@echo "### Extracting scats from $*.lfg_lex"
	echo "SCATS" > $@
	grep "^#SCAT	.*	\(.*:.*\|\)$$" $< | grep -v "[	,][^:]*," | cut -f2- >> $@

spec/%.terms: spec/%.lfg_lex
	@echo
	@echo "### Extracting terminals from $*.lfg_lex"
	grep -v "^#" $< | grep ".	.*	" | cut -f3 | sort -u | grep -v "^$$" > $@

spec/%.lfg: spec/%.mlfg spec/%.scats $(sxlfgcorebin)/mlfg2lfg.pl
	@echo
	@echo "### Compiling $*.mlfg grammar into $*.lfg"
	-mkdir incl 2>/dev/null
	-mkdir lib 2>/dev/null
	$(sxlfgcorebin)/mlfg2lfg.pl -all -scats spec/$* > $@

spec/%-c.lfg_lex: spec/%.lfg_lex
	cp -p $< $@
spec/%-c.disamb: spec/%.disamb
	cp -p $< $@
spec/%-c.chunkf: spec/%.chunkf
	cp -p $< $@
spec/%-c.tdef: spec/%.tdef
	cp -p $< $@
spec/%-c.tpl: spec/%.tpl
	cp -p $< $@
spec/%-c.cpl: spec/%.cpl
	cp -p $< $@

spec/%-c.lfg: spec/%.mlfg spec/%-c.scats $(sxlfgcorebin)/mlfg2lfg.pl
	@echo
	@echo "### Compiling $*.mlfg grammar into $*.lfg"
	-mkdir incl 2>/dev/null
	-mkdir lib 2>/dev/null
	$(sxlfgcorebin)/mlfg2lfg.pl -scats spec/$* > $@

incl/%_lfg.h: spec/%.lfg spec/%.tpl spec/%.cpl $(syntaxbin)/lfg
	@echo
	@echo "### Parsing $*.lfg and generating $*_big.bnf and $*_lfg.h"
	$(syntaxbin)/lfg -bnf spec/$*_big.bnf -vbl spec/$*.vbl $< > $@

incl/%_big.h: incl/%_lfg.h $(syntaxbin)/bnf
	@echo
	@echo "### Generating tables file for $*_big"
	cd spec;\
	$(syntaxbin)/bnf -nsc -nls -h $*_big.bnf > ../$@
	cd ..

spec/%.bnf: bin/%_make_proper spec/%.terms
	@echo
	@echo "### Filtering bnf for $* according to the lexicon thanks to the specific make_proper"
	./bin/$*_make_proper -pm -tm -nm spec/$*.terms > $@

incl/%_mp.h: spec/%.bnf $(sxlfgcorebin)/bnf2mp_table.pl
	@echo
	@echo "### Generating correspondance table between big and filtered bnf for $*"
	$(sxlfgcorebin)/bnf2mp_table.pl $< > $@

incl/%.h: spec/%.bnf $(syntaxbin)/bnf
	@echo
	@echo "### Generating tables file for $*"
	cd spec;\
	$(syntaxbin)/bnf -nsc -nls -h $*.bnf > ../$@
	cd ..

incl/%_lfg_disamb.h: spec/%.disamb $(sxlfgcorebin)/disamb
	@echo
	@echo "### Parsing $*.disamb and generating disambiguation code"
	$(sxlfgcorebin)/disamb $< > $@

incl/%_chunker_ntf.h: spec/%.chunkf $(sxlfgcorebin)/ntf
	@echo
	@echo "### Parsing $*.chunkf and generating chunk-based filtering data"
	$(sxlfgcorebin)/ntf $< > $@$

incl/%_proper_ntf.h: spec/%.propf $(sxlfgcorebin)/ntf
	@echo
	@echo "### Parsing $*.propf and generating prop-based filtering data"
	$(sxlfgcorebin)/ntf $< > $@$

# incl/%.h: spec/%.bt $(syntaxbin)/csynt_lc
# 	@echo
# 	@echo "### Generating lc tables for $* and syntactic parser data"
# 	cd spec;\
# 	$(syntaxbin)/csynt_lc -lc $* > ../$@;\
# 	cd ..

# spec/%.bt: incl/%_lfg.h $(syntaxbin)/bnf
# 	@echo
# 	@echo "### Generating bt file for $*"
# 	cd spec;\
# 	$(syntaxbin)/bnf -nsc -nls $*.bnf
# 	cd ..

# incl/%_td.h: spec/%.bt spec/%.tdef $(syntaxbin)/tdef
# 	@echo
# 	@echo "### Parsing $*.tdef"
# 	cd spec;\
# 	$(syntaxbin)/tdef $*.tdef > ../$@;\
# 	cd ..

incl/%_td.h: spec/%.tdef bin/%_tdef
	@echo
	@echo "### Parsing $*.tdef"
	cd spec;\
	../bin/sxlfg-fr_tdef $*.tdef > ../$@;\
	cd ..

%-lex: spec/%.lfg_lex incl/%_lfg.h $(syntaxbin)/lfg_lex
	@echo
	@echo "### Building the optimized C code of the binary lexicon for $*"
	$(syntaxbin)/lfg_lex -iw -mp spec/$*.lfg_lex > incl/$*_lex_lfg.h

incl/%_lex_lfg.h: spec/%.lfg_lex incl/%_lfg.h $(syntaxbin)/lfg_lex
	@echo
	@echo "### Building the C code for the binary lexicon for $*"
	$(syntaxbin)/lfg_lex -nodico -iw -mp $< > $@


#######################################################################################################
### compilations indépendantes de la grammaire ###

lib/earley_main.o:	$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
                        $(SXINCL)/udag_scanner.h         \
			$(SXSRC)/sxearley_main.c
	@echo
	@echo "### Compiling the generic Earley module"
	$(CC_O2) $(SXSRC)/sxearley_main.c -o lib/earley_main.o

lib/earley_main.d.o:$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
                        $(SXINCL)/udag_scanner.h         \
			$(SXSRC)/sxearley_main.c
	$(CC_DEBUG) $(SXSRC)/sxearley_main.c -o lib/earley_main.d.o

lib/sxsem_mngr.o: $(SXINCL)/sxunix.h	\
		$(SXINCL)/earley.h		\
		$(SXINCL)/varstr.h		\
		$(SXSRC)/sxsem_mngr.c
	@echo
	@echo "### Compiling the semantic passes manager"
	$(CC_O2) $(SXSRC)/sxsem_mngr.c -o lib/sxsem_mngr.o	\
		-Doutput -Dlfg -Dnbest -Dweights -Dstructure -Dchunker -Dproper

lib/sxsem_mngr.d.o: $(SXINCL)/sxunix.h	\
		$(SXINCL)/earley.h		\
		$(SXINCL)/varstr.h		\
		$(SXSRC)/sxsem_mngr.c
	$(CC_DEBUG) $(SXSRC)/sxsem_mngr.c -o lib/sxsem_mngr.d.o	\
		-Doutput -Dlfg -Dnbest -Dweights -Dstructure -Dchunker -Dproper


#######################################################################################################
### compilations indépendantes du type d'analyseur ###

# Le .h contenant le lexique est généré par lfg_lex
# - avec option -nodico si on veut utiliser sxword
# - sans option (ou -dico) si on veut utiliser dico
# La compilation et l'utilisation sont identiques dans les 2 cas
lib/%_lex_lfg.o:	incl/%_lex_lfg.h	 		\
                        $(SXSRC)/lex_lfg.c
	@echo
	@echo "### Compiling the binary lexicon for $*"
	$(CC_O2) $(SXSRC)/lex_lfg.c -o lib/$*_lex_lfg.o		\
			-Dlex_lfg_h=\"$*_lex_lfg.h\" 		\
			-DCOMPOUND				\
			-DICO					\
			-DICO_COMPOUND				\
			-DLEXICON				\
			-DICO_INFLECTED_FORMS			\
			-DEASY					\
			-Dinclude_full_lexicon			\
			-Dstatic=""

#######################################################################################################
### analyseur standard optimisé ###

%:		$(BIBS) \
		lib/earley_main.o			\
		lib/%_lex_lfg.o			\
		lib/%_udag_scanner.o		\
		lib/%_lexicalizer.o	\
		lib/%_earley.o	\
		lib/sxsem_mngr.o			\
		lib/%_logprob.o			\
		lib/%_chunker_semact.o			\
		lib/%_weights_semact.o			\
		lib/%_structure_semact.o			\
		lib/%_proper_semact.o			\
		lib/%_lfg_semact.o			\
		lib/%_lfg_output_semact.o
	@echo '	cc -o $@';\
	$(GCC) -I$(SXINCL) -I$(SXETCINCL) -Iincl -O2 -o $@ -lm \
		lib/earley_main.o			\
		lib/$*_lex_lfg.o			\
		lib/$*_udag_scanner.o			\
		lib/$*_lexicalizer.o	\
		lib/$*_earley.o		\
		lib/sxsem_mngr.o			\
		lib/$*_logprob.o			\
		lib/$*_chunker_semact.o			\
		lib/$*_structure_semact.o			\
		lib/$*_weights_semact.o			\
		lib/$*_proper_semact.o			\
		lib/$*_lfg_semact.o			\
		lib/$*_lfg_output_semact.o			\
		$(BIBS)


lib/%_lexicalizer.o:	$(SXINCL)/sxunix.h		\
			incl/%.h			\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxstack.h         	\
			$(SXSRC)/lexicalizer_mngr.c
	@echo
	@echo "### Compiling the lexicalizer for $*"
	$(CC_O2) $(SXSRC)/lexicalizer_mngr.c -o lib/$*_lexicalizer.o	\
		-DLC_TABLES_H=\"$*.h\"	\
		$(SA_CRS)		\
		$(DAG_SDAG)

# Avec recherche ds le dico incl/sxlfg-fr_lex_lfg.h construit par :
# lfg_lexspec/sxlfg-fr.lex > incl/sxlfg-fr_lex_lfg.h
lib/%_udag_scanner.o:	$(SXINCL)/sxunix.h	\
                        $(SXINCL)/udag_scanner.h       	\
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxdico.h              \
                        $(SXINCL)/lex_lfg.h 		\
			incl/%_lex_lfg.h		\
			incl/%_mp.h			\
                        $(SXSRC)/udag_scanner.c
	@echo
	@echo "### Compiling the DAG scanner for $*"
	$(CC) $(SXSRC)/udag_scanner.c -o lib/$*_udag_scanner.o\
			-Dlex_lfg_h=\"$*_lex_lfg.h\"	\
			-Dmp_h=\"$*_mp.h\"		\
			$(DAG_SDAG)

lib/%_earley.o: $(SXINCL)/sxunix.h	\
		$(SXINCL)/varstr.h		\
		incl/%.h		\
		$(SXINCL)/earley.h		\
		$(SXINCL)/sxspf.h		\
                $(SXINCL)/udag_scanner.h		\
                $(SXINCL)/sxba.h		\
		$(SXSRC)/earley_parser.c
	@echo
	@echo "### Compiling the Earley parser for $*"
	$(CC_O2) $(SXSRC)/earley_parser.c -o lib/$*_earley.o	\
		-DLC_TABLES_H=\"$*.h\"	\
		-Dis_recognizer=1	\
		-Dis_parser=1 		\
		-DSEMANTICS="sxsem_mngr_semact"	\
		-Dis_rcvr=1 		\
		-Dis_1LA=0		\
		$(DAG_SDAG)		\
		-Dis_supertagging=0	\
		$(LEX)			\
		-Dhas_Lex=0		\
		-Dis_guide=0		\
		-DMEASURES=0		\
		-Dhas_output_nt		\
		-Dis_make_oracle=0

lib/%_weights_semact.o:	$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxba.h                \
			$(SXINCL)/sxstack.h		\
			$(SXINCL)/XH.h			\
			incl/%_lfg.h		\
			incl/%_lex_lfg.h	\
			$(SXINCL)/lex_lfg.h 		\
			incl/%_mp.h			\
			$(sxlfgcoresrc)/weights_semact.c
	@echo
	@echo "### Compiling the weights module"
	$(CC_O2) $(sxlfgcoresrc)/weights_semact.c -o lib/$*_weights_semact.o	\
		$(DAG_SDAG)				\
		-Dmp_h=\"$*_mp.h\"		\
		-Dlfg_h=\"$*_lfg.h\"		\
		-Dlex_lfg_h=\"$*_lex_lfg.h\"

lib/%_structure_semact.o:	$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxba.h                \
			$(SXINCL)/sxstack.h		\
			$(SXINCL)/XH.h			\
			incl/%_lfg.h		\
			incl/%_lex_lfg.h	\
			$(SXINCL)/lex_lfg.h 		\
			incl/%_mp.h			\
			$(sxlfgcoresrc)/structure_semact.c
	@echo
	@echo "### Compiling the structure module"
	$(CC_O2) $(sxlfgcoresrc)/structure_semact.c -o lib/$*_structure_semact.o	\
		$(DAG_SDAG)

lib/%_chunker_semact.o: $(SXINCL)/sxunix.h		\
                        $(SXINCL)/udag_scanner.h         \
                        $(SXINCL)/earley.h              \
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxstack.h             \
			incl/%_td.h			\
			incl/%_chunker_ntf.h	\
                        $(sxlfgcoreincl)/snt_semact.h	\
                        $(sxlfgcoresrc)/snt_semact.c
	@echo
	@echo "### Compiling the chunker module for $*"
	$(CC_O2) $(sxlfgcoresrc)/snt_semact.c -o lib/$*_chunker_semact.o	\
			$(DAG_SDAG)					\
			-Dtd_h=\"$*_td.h\"				\
			-Dntf_h=\"$*_chunker_ntf.h\"			\
			-Dcheck_adjunct=1

lib/%_proper_semact.o: $(SXINCL)/sxunix.h		\
                        $(SXINCL)/udag_scanner.h         \
                        $(SXINCL)/earley.h              \
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxstack.h             \
			incl/%_td.h			\
			incl/%_proper_ntf.h	\
                        $(sxlfgcoreincl)/snt_semact.h	\
                        $(sxlfgcoresrc)/snt_semact.c
	@echo
	@echo "### Compiling the proper module for $*"
	$(CC_O2) $(sxlfgcoresrc)/snt_semact.c -o lib/$*_proper_semact.o	\
			$(DAG_SDAG)					\
			-Dtd_h=\"$*_td.h\"				\
			-Dntf_h=\"$*_proper_ntf.h\"			\
			-Dcheck_adjunct=1

lib/%_lfg_output_semact.o:	$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxba.h                \
			$(SXINCL)/sxstack.h		\
			$(SXINCL)/XH.h			\
			$(sxlfgcoreincl)/lfg_semact.h		\
			incl/%_td.h		\
			incl/%_lfg.h		\
			incl/%_lex_lfg.h	\
			incl/%_lfg_disamb.h	\
			$(SXINCL)/lex_lfg.h 		\
			$(sxlfgcoresrc)/lfg_output_semact.c
	@echo
	@echo "### Compiling the output module"
	$(CC_O2) $(sxlfgcoresrc)/lfg_output_semact.c -o lib/$*_lfg_output_semact.o	\
		-Doutput -Dlfg -Dnbest -Dweights -Dstructure -Dchunker -Dproper \
		$(DAG_SDAG)				\
		-DLEXICON				\
		-DEASY					\
		-Dno_print_relations			\
		-Dlfg_h=\"$*_lfg.h\"		\
		-Dlex_lfg_h=\"$*_lex_lfg.h\"	\
		-Dlfg_disamb_h=\"$*_lfg_disamb.h\"	\
		-Dtd_h=\"$*_td.h\"

lib/%_lfg_semact.o:	$(SXINCL)/sxunix.h          	\
			$(SXINCL)/nbest.h		\
                        $(SXINCL)/udag_scanner.h         \
                        $(SXINCL)/earley.h              \
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxstack.h             \
			incl/%_lex_lfg.h		\
			incl/%_lfg.h			\
			incl/%_lfg_disamb.h		\
			incl/%_td.h			\
			incl/%_mp.h			\
                        $(sxlfgcoresrc)/lfg_semact.c
	@echo
	@echo "### Compiling the LFG module (f-structure computation)"
	$(CC) $(sxlfgcoresrc)/lfg_semact.c -o lib/$*_lfg_semact.o			\
			$(DAG_SDAG)					\
			-Dlex_lfg_h=\"$*_lex_lfg.h\" 		\
			-Dlfg_h=\"$*_lfg.h\"			\
			-Dtd_h=\"$*_td.h\"			\
			-Dmp_h=\"$*_mp.h\"			\
			-Dlfg_disamb_h=\"$*_lfg_disamb.h\"	\
			-DEASY						\
			-Dcheck_adjunct=1				\
			-DFS_PROBAS=1

#			-DFS_PROBAS=1 permet (si appele' avec l'option '-prpr') d'utiliser le ranking 'p'


#######################################################################################################
### analyseur de débug ###

%.d:	$(BIBS_DEBUG) \
		lib/earley_main.d.o			\
		lib/%_lex_lfg.o			\
		lib/%_udag_scanner.d.o		\
		lib/%_lexicalizer.d.o		\
		lib/%_earley.d.o		\
		lib/sxsem_mngr.d.o		\
		lib/%_logprob.o			\
		lib/%_weights_semact.d.o	\
		lib/%_structure_semact.d.o	\
		lib/%_chunker_semact.d.o	\
		lib/%_proper_semact.d.o		\
		lib/%_lfg_semact.d.o		\
		lib/nbest_semact.d.o			\
		lib/%_lfg_output_semact.d.o
	@echo '	cc -o $@';\
	$(GCC) -I$(SXINCL) -I$(SXETCINCL) -Iincl -ggdb -o $@ -lm \
		lib/earley_main.d.o			\
		lib/$*_lex_lfg.o		\
		lib/$*_udag_scanner.d.o		\
		lib/$*_lexicalizer.d.o		\
		lib/$*_earley.d.o		\
		lib/sxsem_mngr.d.o		\
		lib/$*_logprob.o		\
		lib/$*_weights_semact.d.o	\
		lib/$*_structure_semact.d.o	\
		lib/$*_chunker_semact.d.o	\
		lib/$*_proper_semact.d.o	\
		lib/$*_lfg_semact.d.o		\
		lib/nbest_semact.d.o			\
		lib/$*_lfg_output_semact.d.o	\
		$(BIBS_DEBUG)


lib/%_lexicalizer.d.o:	$(SXINCL)/sxunix.h		\
					incl/%.h		\
					$(SXINCL)/earley.h		\
					$(SXINCL)/sxstack.h         	\
					$(SXSRC)/lexicalizer_mngr.c
	$(CC_DEBUG) $(SXSRC)/lexicalizer_mngr.c -o lib/$*_lexicalizer.d.o	\
		-DEBUG				\
		-DLOG				\
		-DLC_TABLES_H=\"$*.h\"	\
		$(SA_CRS)			\
		-DMULTI_ANCHOR			\
		$(DAG_SDAG)

# Avec recherche ds le dico incl/sxlfg-fr_lex_lfg.h construit par :
# lfg_lexspec/sxlfg-fr.lex > incl/sxlfg-fr_lex_lfg.h
lib/%_udag_scanner.d.o:	$(SXINCL)/sxunix.h	\
                        $(SXINCL)/udag_scanner.h       	\
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxdico.h              \
                        $(SXINCL)/lex_lfg.h 		\
			incl/%_lex_lfg.h		\
			incl/%_mp.h			\
                        $(SXSRC)/udag_scanner.c
	$(CC_DEBUG) $(SXSRC)/udag_scanner.c -o lib/$*_udag_scanner.d.o\
			-DEBUG						\
			-DLOG						\
			-Dlex_lfg_h=\"$*_lex_lfg.h\"	\
			-Dmp_h=\"$*_mp.h\"		\
			$(DAG_SDAG)

lib/%_earley.d.o: $(SXINCL)/sxunix.h	\
		$(SXINCL)/varstr.h		\
		incl/%.h		\
		$(SXINCL)/earley.h		\
		$(SXINCL)/sxspf.h		\
                $(SXINCL)/udag_scanner.h		\
                $(SXINCL)/sxba.h		\
		$(SXSRC)/earley_parser.c
	$(CC_DEBUG) $(SXSRC)/earley_parser.c -o lib/$*_earley.d.o	\
		-DEBUG				\
		-DLOG				\
		-DLC_TABLES_H=\"$*.h\"	\
		-Dis_recognizer=1		\
		-Dis_parser=1 			\
		-DSEMANTICS="sxsem_mngr_semact"\
		-Dis_rcvr=1 			\
		-Dis_1LA=0			\
		$(DAG_SDAG)			\
		-Dis_supertagging=0		\
		$(LEX)				\
		-Dhas_Lex=0			\
		-Dis_guide=0			\
		-DMEASURES=0			\
		-Dhas_output_nt		\
		-Dis_make_oracle=0

lib/%_structure_semact.d.o:	$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxba.h                \
			$(SXINCL)/sxstack.h		\
			$(SXINCL)/XH.h			\
			incl/%_lfg.h		\
			incl/%_lex_lfg.h	\
			incl/%_mp.h		\
			$(SXINCL)/lex_lfg.h 		\
			$(sxlfgcoresrc)/structure_semact.c
	@echo
	@echo "### Compiling the structure module"
	$(CC_DEBUG) $(sxlfgcoresrc)/structure_semact.c -o lib/$*_structure_semact.d.o	\
		-DEBUG					\
		$(DAG_SDAG)

lib/%_weights_semact.d.o:	$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxba.h                \
			$(SXINCL)/sxstack.h		\
			$(SXINCL)/XH.h			\
			incl/%_lfg.h		\
			incl/%_lex_lfg.h	\
			incl/%_mp.h		\
			$(SXINCL)/lex_lfg.h 		\
			$(sxlfgcoresrc)/weights_semact.c
	@echo
	@echo "### Compiling the weights module"
	$(CC_DEBUG) $(sxlfgcoresrc)/weights_semact.c -o lib/$*_weights_semact.d.o	\
		-DEBUG					\
		$(DAG_SDAG)				\
		-Dmp_h=\"$*_mp.h\"		\
		-Dlfg_h=\"$*_lfg.h\"		\
		-Dlex_lfg_h=\"$*_lex_lfg.h\"

lib/%_chunker_semact.d.o: $(SXINCL)/sxunix.h		\
                        $(SXINCL)/udag_scanner.h         \
                        $(SXINCL)/earley.h              \
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxstack.h             \
			incl/%_td.h		\
			incl/%_chunker_ntf.h	\
                        $(sxlfgcoreincl)/snt_semact.h	\
                        $(sxlfgcoresrc)/snt_semact.c
	$(CC_DEBUG) $(sxlfgcoresrc)/snt_semact.c -o lib/$*_chunker_semact.d.o	\
			-DEBUG						\
			$(DAG_SDAG)					\
			-Dtd_h=\"$*_td.h\"				\
			-Dntf_h=\"$*_chunker_ntf.h\"			\
			-Dcheck_adjunct=1

lib/%_proper_semact.d.o: $(SXINCL)/sxunix.h		\
                        $(SXINCL)/udag_scanner.h         \
                        $(SXINCL)/earley.h              \
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxstack.h             \
			incl/%_td.h		\
			incl/%_proper_ntf.h	\
                        $(sxlfgcoreincl)/snt_semact.h	\
                        $(sxlfgcoresrc)/snt_semact.c
	$(CC_DEBUG) $(sxlfgcoresrc)/snt_semact.c -o lib/$*_proper_semact.d.o	\
			-DEBUG						\
			$(DAG_SDAG)					\
			-Dtd_h=\"$*_td.h\"				\
			-Dntf_h=\"$*_proper_ntf.h\"			\
			-Dcheck_adjunct=1

lib/nbest_semact.d.o: $(SXINCL)/sxunix.h		\
		$(SXINCL)/nbest.h		\
		$(SXINCL)/earley.h		\
		$(SXINCL)/sxstack.h		\
                $(SXINCL)/XH.h			\
		$(SXSRC)/nbest_semact.c
	$(CC_DEBUG) $(SXSRC)/nbest_semact.c -o lib/nbest_semact.d.o\
		-DEBUG					\
		-DLOG					\
		-DLLOG

lib/%_lfg_output_semact.d.o:$(SXINCL)/sxunix.h		\
			$(SXINCL)/earley.h		\
			$(SXINCL)/sxba.h                \
			$(SXINCL)/sxstack.h		\
			$(SXINCL)/XH.h			\
			$(sxlfgcoreincl)/lfg_semact.h		\
			incl/%_lfg.h		\
			incl/%_lex_lfg.h	\
			incl/%_lfg_disamb.h	\
			$(SXINCL)/lex_lfg.h 		\
			$(sxlfgcoresrc)/lfg_output_semact.c
	$(CC_DEBUG) $(sxlfgcoresrc)/lfg_output_semact.c -o lib/$*_lfg_output_semact.d.o	\
		-DEBUG					\
		-DLOG					\
		-DLLOG					\
		$(DAG_SDAG)				\
		-DLEXICON				\
		-DEASY					\
		-Dno_print_relations			\
		-Dlfg_h=\"$*_lfg.h\"		\
		-Dlex_lfg_h=\"$*_lex_lfg.h\"	\
		-Dlfg_disamb_h=\"$*_lfg_disamb.h\"	\
		-Dtd_h=\"$*_td.h\"

lib/%_lfg_semact.d.o:	$(SXINCL)/sxunix.h          	\
			$(SXINCL)/nbest.h		\
                        $(SXINCL)/udag_scanner.h         \
                        $(SXINCL)/earley.h              \
                        $(SXINCL)/sxba.h                \
                        $(SXINCL)/sxstack.h             \
			incl/%_lex_lfg.h		\
			incl/%_lfg_disamb.h		\
			incl/%_lfg.h			\
			incl/%_td.h			\
			incl/%_mp.h			\
                        $(sxlfgcoresrc)/lfg_semact.c
	$(CC_DEBUG) $(sxlfgcoresrc)/lfg_semact.c -o lib/$*_lfg_semact.d.o	\
			-DEBUG					\
			-DLOG					\
			$(DAG_SDAG)				\
			-Dlex_lfg_h=\"$*_lex_lfg.h\" 		\
			-Dlfg_h=\"$*_lfg.h\"			\
			-Dtd_h=\"$*_td.h\"			\
			-Dmp_h=\"$*_mp.h\"			\
			-Dlfg_disamb_h=\"$*_lfg_disamb.h\"	\
			-DEASY						\
			-Dcheck_adjunct=1				\
			-DFS_PROBAS=1


#			-DFS_PROBAS=1 permet (si appele' avec l'option '-prpr') d'utiliser le ranking 'p'





#####################################################################################################################
# Fabrication d'un make_proper pour une grosse grammaire GG
# On a donc exe'cute'
# bnf -nsc -nls -h GG.bnf > ../incl/GG.h
# Les bnf/src/make_proper_t.c sont faites par un autogenese ds bnf

bin/%_make_proper:	$(BIBS) \
		lib/%_make_proper_main.o		\
		lib/%_make_proper_lexicalizer.o	\
		lib/make_proper_t.o
	@echo
	@echo "### Compiling specific make_proper for $*"
	-mkdir bin 2>/dev/null
	$(GCC) -O2 -I$(SXINCL) -I$(SXETCINCL) -Iincl -o $@ \
		lib/$*_make_proper_main.o		\
		lib/$*_make_proper_lexicalizer.o	\
		lib/make_proper_t.o			\
		$(BIBS)

lib/%_make_proper_main.o:	$(SXINCL)/sxunix.h	\
			$(SXINCL)/earley.h		\
                        $(SXINCL)/sxspf.h         	\
                        $(SXINCL)/sxword.h         	\
                        incl/%_big.h	 		\
			$(SXSRC)/make_proper_main.c
	@echo
	@echo "### Compiling make_proper data (1)"
	$(CC_O2) $(SXSRC)/make_proper_main.c -o lib/$*_make_proper_main.o	\
		-DLC_TABLES_H=\"$*_big.h\"

lib/%_make_proper_lexicalizer.o:	$(SXINCL)/sxunix.h	\
			$(SXINCL)/earley.h		\
                        $(SXINCL)/sxspf.h         	\
                        $(SXINCL)/sxstack.h         	\
                        incl/%_big.h	 		\
			$(SXSRC)/lexicalizer_mngr.c
	@echo
	@echo "### Compiling make_proper data (2)"
	$(CC_O2) $(SXSRC)/lexicalizer_mngr.c -o lib/$*_make_proper_lexicalizer.o	\
		-DLC_TABLES_H=\"$*_big.h\"	\
		$(noSA_noCRS)			\
		$(NODAG_NOSDAG)			\
		-DMAKE_PROPER			\
		-DMULTI_ANCHOR

lib/make_proper_t.o: 	$(SXINCL)/sxunix.h		\
			$(SXSRC)/make_proper_t.c
	@echo
	@echo "### Compiling make_proper tables"
	$(CC) $(SXSRC)/make_proper_t.c -o lib/make_proper_t.o	\
		-DSCANACT=make_proper_scanact	\
		-DSEMACT=make_proper_semact


#####################################################################################################################
# Fabrication de make_tdef (analogue de tdef mais marche avec une grosse grammaire GG)
# On a donc exe'cute'
# bnf -nsc -nls -h GG.bnf > ../incl/GG.h

bin/%_tdef:	$(BIBS) \
		lib/%_tdef_main.o		\
		lib/tdef_t.o
	-mkdir bin 2>/dev/null
	@echo
	@echo "### Compiling specific tdef for $*"
	$(GCC) -I$(SXINCL) -I$(SXETCINCL) -Iincl -o $@ -lm \
		lib/$*_tdef_main.o	\
		lib/tdef_t.o		\
		$(BIBS)

lib/%_tdef_main.o:$(SXINCL)/sxunix.h		\
                        $(SXINCL)/sxword.h         	\
                        incl/%.h	 		\
			$(SXSRC)/make_tdef_main.c
	@echo
	@echo "### Compiling specific tdef data for $*"
	$(CC) $(SXSRC)/make_tdef_main.c -o lib/$*_tdef_main.o	\
		-DLC_TABLES_H=\"$*.h\"

lib/tdef_t.o: 		$(SXINCL)/sxunix.h		\
			$(SXSRC)/tdef_t.c
	@echo
	@echo "### Compiling tdef tables"
	$(CC) $(SXSRC)/tdef_t.c -o lib/tdef_t.o	\
		-DSCANACT=make_tdef_scanact	\
		-DSEMACT=make_tdef_semact


#####################################################################################################################
### construction des tables de probabilités ###

# c'est ce .o qui est utilisé par le linkeur pour produire l'analyseur
# ce .o est obtenu par compilation d'un .c généré automatiquement par l'exécutable sxlfg-fr_weights2proba, à
#     partir du fichier de statistiques .stat
# cet exécutable dépend de la grammaire (des tables lc)
lib/%_logprob.o:	src/%_logprob.c
	@echo
	@echo "### Compiling the binary probabilistic data for $*"
	$(CC_O2) $< -o $@


src/%_logprob.c:	spec/%.stats		\
			bin/%_weights2proba
	@echo
	@echo "### Generating C code for the binary probabilistic data for $*"
	-mkdir src 2>/dev/null
	cat $< | bin/$*_weights2proba > $@

bin/%_weights2proba:	$(BIBS)		\
			lib/%_weights2proba.o
	@echo
	@echo "### Building the probabilities extractor for $* from $*.stats"
	-mkdir bin 2>/dev/null
	$(GCC) -O2 -o $@ -lm \
			lib/$*_weights2proba.o	\
			$(BIBS)


lib/%_weights2proba.o: $(SXINCL)/sxunix.h	\
		incl/%.h			\
		$(SXINCL)/earley.h		\
		$(SXSRC)/weights2proba.c
	@echo
	@echo "### Compiling the probabilities extractor core for $*"
	$(CC) $(SXSRC)/weights2proba.c -o lib/$*_weights2proba.o	\
		-DLC_TABLES_H=\"$*.h\"

# par défaut...
spec/%.stats:
	echo "0 0 0 0 0 0 0" > $@


#####################################################################################################################
### stats

%.png: ../pls/%.pls
	 ploticus -png -o $@ $<

estats: corpus_stats.png const_stats.png kind_stats.png corpus_rel_stats.png rel_stats.png kind_rel_stats.png
stats: distrib_length.png distrib_success.png distrib_success_rate.png distrib_timeout.png distrib_time_noto.png distrib_time_med.png distrib_time.png 

%.stats: %.dblog
	perl logdb2stats.pl --rn=$*
	-rm $*/*.png
	make -C $* -f ../Makefile stats

%.dblog: %
	perl log2db.pl --rn=$*
	touch $@

%.estats: %.edblog
	perl elogdb2stats.pl --rn=$*
	-rm $*/*.png
	make -C $* -f ../Makefile estats

%.easy-tmp:
	-rm -rf $*/easyresults-tmp
	mkdir $*/easyresults-tmp
	for i in `ls $*/results/*.xml`; do echo $$i && cat $$i | ../sxlfg-core/bin/lfg2easy.pl > `echo $$i | perl -pe "s/results/easyresults-tmp/"`; done
	touch $@

%.easy: %.easy-tmp
	-rm -rf $*/easyresults
	mkdir $*/easyresults
	for i in `ls $*/easyresults-tmp/*.xml`; do echo $$i && cat $$i | ../sxlfg-core/bin/easyfix.pl $$i > `echo $$i | perl -pe "s/-tmp//"`; done
	touch $@

%.easy-tmp-2:
	-rm -rf $*/easyresults-tmp-2
	mkdir $*/easyresults-tmp-2
	for i in `ls $*/results/*.xml`; do echo $$i && cat $$i | ../sxlfg-core/bin/lfg2easy-2.pl > `echo $$i | perl -pe "s/results/easyresults-tmp-2/"`; done
	touch $@

%.easy-2: %.easy-tmp-2
	-rm -rf $*/easyresults-2
	mkdir $*/easyresults-2
	for i in `ls $*/easyresults-tmp-2/*.xml`; do echo $$i && cat $$i | ../sxlfg-core/bin/easyfix.pl $$i > `echo $$i | perl -pe "s/-tmp//"`; done
	touch $@


%.eval: %.easy
	-mkdir $*/easyeval
	cd $*/easyeval && ../../../../../../chaines/lingwb/corpus_proc/trunk/easymerge.pl -ref ../../../../../passage/easy/modified-reference -hyp ../easyresults -html eval.html >/dev/null && cd ../..
	touch $@

%.evisu: %.eval
	for i in `ls $*/easyeval/*.merged.xml`; do echo $$i && xsltproc -novalid ../../../../chaines/lingwb/corpus_proc/trunk/easyalt2html.xsl $$i > `echo $$i | perl -pe "s/xml/html/"`; done

%.edblog: %
	perl elog2db.pl --rn=$*
	perl erel2db.pl --rn=$*
	touch $@

%.em:
	perl $(EMBIN)/errors2stats.pl --results=$*/logs --maxiter=50 --dag=udags
	-mkdir $*/dbdir
	perl $(EMBIN)/mine2db.pl --results=$*/logs --iter=50
	touch $@

%.em-l:
	perl $(EMBIN)/errors2stats.pl --results=$*/logs --maxiter=50 --l=1 --dag=udags
	-mkdir $*/dbdir
	perl $(EMBIN)/mine2db.pl --results=$*/logs --iter=50 --l=1
	touch $@
%.em-2:
	perl $(EMBIN)/errors2stats.pl --results=$*/logs --maxiter=50 --ngm=2 --dag=udags
	-mkdir $*/dbdir
	perl $(EMBIN)/mine2db.pl --results=$*/logs --iter=50 --ngm=2
	touch $@

%.megam: %.em
	 /usr/local/bin/megam.opt -fvals binary $*/logs/me50-1.txt | ./megam2mine.pl $*/logs/shortmine50-1.csv > $*/logs/mine50-1EM.csv
	 perl $(EMBIN)/mine2db.pl --results=$*/logs --ngm=1 --algo=EM

%.megam-2g: %.em-2
	 /usr/local/bin/megam.opt -fvals binary $*/logs/me50-2.txt | ./megam2mine.pl $*/logs/shortmine50-2.csv > $*/logs/mine50-2EM.csv
	 perl $(EMBIN)/mine2db.pl --results=$*/logs --ngm=2 --algo=EM

%.vn: %.em
	 cat $*/logs/vn50-1.txt | ./vn2mine.pl $*/logs/shortmine50-1.csv > $*/logs/mine50-1EM.csv
	 perl $(EMBIN)/mine2db.pl --results=$*/logs --ngm=1 --algo=VN

%.vn-2: %.em
	 cat $*/logs/vn50-2.txt | ./vn2mine.pl $*/logs/shortmine50-2.csv > $*/logs/mine50-2EM.csv
	 perl $(EMBIN)/mine2db.pl --results=$*/logs --ngm=2 --algo=VN

mdsxlfg10-1.tex: $(EMBIN)/errors2table.pl comments.txt
	perl $(EMBIN)/errors2table.pl -from 1 -to 11  --dbdir=run_new_general_lemonde_CZKlX_er/dbdir --comments=comments.txt  --iter=50 > $@

mdsxlfg20-1.tex: $(EMBIN)/errors2table.pl comments.txt
	perl $(EMBIN)/errors2table.pl -from 1 -to 20  --dbdir=run_new_general_lemonde_CZKlX_er/dbdir --comments=comments.txt  --iter=50 > $@

mdsxlfg100-1.tex: $(EMBIN)/errors2table.pl comments.txt
	perl $(EMBIN)/errors2table.pl -from 100 -to 108  --dbdir=run_new_general_lemonde_CZKlX_er/dbdir --comments=comments.txt --iter=50 > $@


