#############################################################
# DECLARATION DES OBJETS DE LA GRAMMAIRE
#############################################################

#############################################################
# d�claration des attributs
#############################################################

ATTRIBUTE
	Suj = <[]>;
	Obj = <[]>;
	Obj� = <[]>;
	Objde = <[]>;
	Loc = <[]>;
	Dloc = <[]>;
	Att = <[]>;
	Obl = <[]>;
	Obl2 = <[]>;
	adjunct = {[]}; 	// modifieur : �pith�te, relative, compl�ment de nom, compl�tive de nom ou d'adj, adverbe, etc.
	arg1 = <[]>;		// argument d'une coordonn�e ou juxtapos�e
	arg2 = <[]>;		// argument d'une coordonn�e ou juxtapos�e

	// REALISATIONS
	real = {apr�s-sn, avec-sn, cla, cln, cld, comme-sa, comme-sn, contre-sn, dans-sn, de-sa, de-scompl, de-sinf, de-sn, devant-sn, en, en-sn, loc-sn, par-sn, pour-sa, pour-scompl, pour-sinf, pour-sn, qcompl, sa, sadv, scompl, sinf, sn, sous-sn, sur-sn, vers-sn, y, �-sinf, �-sn, aupr�s_de-sn, autour_de-sn, de_la_part_de-sn, sans-sn};

//	intro = {apr�s, avec, comme, contre, dans, de, devant, en, loc, par, pour, sous, sur, vers, �};
//	real = {cln, cla, cld, en, y, sn, sinf, scompl, qcompl, sa, sadv};

	//------------------------------------------------------------
	// attributs � valeur atomique
	//------------------------------------------------------------
	gender = {masc, fem}; 
	number = {sg, pl}; 
	person = {1, 2, 3};
	inflnumber = {sg, pl}; 
	inflperson = {1, 2, 3};
	numberposs = {sg, pl};		// pour les possessifs
	persposs = {1, 2, 3};
	case = {acc, dat, nom, loc, gen, rfl};
//	mode = {indicative, conditional, subjonctive, imperative, infinitive, participle};
//	tense = {present, past, imperfect, future, pluperfect, past-historic, past-anterior, future-perfect, perfect};
//	aspect = {neutral, inchoative, terminative, durative, notperfect, perfect};
//	v-form = {tensed, participle, infinitive};
	mode = {indicative, subjonctive, conditional, imperative};
	tense = {future, future-perfect, imperfect, past, past-anterior, past-historic, perfect, pluperfect, present, none};
	aspect = {neutral, inchoative, terminative, durative};
	v-form = {infinitive, participle, past-participle, tensed};
	form = {il, �a, si, que, comme, virgule};
	define = {+, -};
//	partitive = {+, -};
	demonstrative = {+, -};
	wh = {+, -};
	que = {+};
	possessive = {+, -};
	pcas = {�, de, dans, sur, vers, par, devant, en, contre, apr�s, avec, autre, pour, comme};
	neg = {+, -};
	clneg = {+, -};
	type = {ind, card, ord, poss, int, qual, part, excl, rel, dem, def};
	diathesis = {active, passive};
	aux-req = {�tre, avoir};
	form-aux = {�tre, avoir, aller, venir, venir-de};

//	degree = {+, -};
	qu = {+,-};
	det = {+,-};
	pre_det = {+};
	adv_kind = {intens,tr�s,modnc};
	advGP = {+};
	refl = {+};
	imp = {+,-};
	clloc = {+, -};
	clgen = {+, -};
	clle = {+, -};
	clla = {+, -};
	clles = {+, -};
	
	//----------restriction de s�lection--------------------------------------------------
	hum = {+, -};
//	anim = {+, -};
//	conc = {+, -};
	locative = {+, -};
	time = {+, -};
	weekday = {+, -};
	year = {+, -};
//	pobody = {+, -};

	//----------pour traduction automatique-----------------------------------------------
	cat = {adj, nc, np, v, adv, prep, coo, etr};

	//----------pour SXLFG-----------------------------------------------
	aij = {Aij, Pij ...}2;

	//--------- OUBLIS ? ---------------
//        dem = {+};
	  pro = {+, -};

	  //--------- verbes support et assimil�s ---------
	lightverb = {-,faire,prendre,avoir,donner,rester,garder,tomber,mettre};
	synt_head = {...};


############## d�sambigu�sation LFG ###############
	/* 0 : hasard */
	/* 1 : Calcul des priorites des f_structures a partir des priorites des elements lexicaux */
	/* 2 : preferer les arguments aux modifieurs */
	/* 3 : Preferer les arguments proches */
	/* 4 : Preferer les structures les + enchassees */
	/* 5 : Preferer la valeurs atomique locale la plus faible du champ "special_atomic_field_id"
	   de la structure principale.  Pour l'instant special_atomic_field_id == "mode" ds la campagne easy */
	/* 6 : Preferer ds l'ordre les rattachements adv adv, adv adj puis adv v 
	   le cas adv nc depend de l'adv */ 
	/* 7 : Preferer les aij de couverture maximale */
	/* 8 : Preferer les nc avec articles */
	/* 9 : minimiser les adjoints nominaux des noms */
	/* p : preferer les f-structures de proba max */

COMPLETE
	ENONCES '$1824536790';
	PHRASE '$182453679';
	Easy_JUXT2b '$182453679';
	Easy_JUXT '$182453679';
	S4 '$182453679';
	S3 '$182453679';
	SCOMPranked '$18453679';
	SCOMPranked___1 '$18453679';
	SCOMPranked___c '$18453679';
	Pri_WSsubj '$18453679';
	Pri_WSobj '$18453679';
	Pri_WSdans_obj '$18453679';
	Pri_WSadjunct '$18453679';
	Pri_WS_dir '$18453679';
	REL '$18453679';

//	SCOMP2 '18453679';
//	SCOMP '18453679';
//	SCOMP___1 '18453679';
//	SCOMP___c '18453679';
//	WSCOMP '18453679';
//	WSCOMP___1 '18453679';
//	WSCOMP___c '18453679';

	PREVERB '1845679';
	POSTVERB '1845679';
	POSTADJ '1845679';
	SN '$1845679';	// wahouu, le '$' est optimiste... mais cf. les SA avec structures
			// contr�l�es qui n'ont pas de pred... (genre "consistant" pas suivi de "�")
	SA '1845679';
	SNP '1845679';
	SVP '1845679';
	SN___1 '1845679';
	SA___1 '1845679';
	SNP___1 '1845679';
	SVP___1 '1845679';
	SN___c '1845679';
	SA___c '1845679';
	SNP___c '1845679';
	SVP___c '1845679';



#############################################################
# d�claration des cat�gories syntaxiques (auxiliaires)
#############################################################

CATEGORY
	COMP;
	S2;
	S3;
	SCooSubj;
	SCoo;
	WS;
	WREL;
	PWS;
	S;
	SCOMP;
	NP;
	AP;
	AP2;
	ADVA; // adverbe d'adjectif
	ADVP; // adverbe de phrase
	ADVV; // adverbe de verbe
	N;
	N2;
	N3;
	PP;
	VP;
	VP2;
	VPa;
	V;
	S;
	CLITIC;
	PREVERB;
	VERB;
	POSTVERB;
	V;

#SXLFG
	ADVP_ponctw;
	Ponctw_ADVP;
	Ponctw_ADVP_ponctw;
	Ponctw_poncts;
	Ponctw_PP;
	Ponctw_WREL;
	Clneg_advneg;

# Easy
	Easy_NV1;
	Easy_NV2;
	Easy_NV3;
	Easy_NV4;
	Easy_NV5;
	Easy_NV6;
	Easy_NV7;
	Easy_NV8;
	Easy_GN;
	Easy_GA;
	Easy_GP;
	Easy_InflV;
	Easy_InflAuxAvoir;
	Easy_InflAuxEtre;
	Easy_PV;

#############################################################
# d�claration des cat�gories lexicales (terminales)
#############################################################

TERMINAL	// liste p�rim�e mais (semble-t-il) inutilis�e
	adj;
	adv;
	advneg;
	auxAvoir;
	auxEtre;
	auxAller;
	auxVenir;
	cla;
	clar;
	cld;
	cldr;
	clg;
	cln;
	clneg;
	clr;
	cll;
	clneg;
	coo;
	comp;
	csu;
	det;
	int;
	nc;
	ncpred;
	np;
	ponctp;  //-, >>, ponctuations pr�fixes
	ponctf;  //etc., ponctuations suffixes
	poncts;
	ponctw;
	prel;
	prep;
	pro;
	v;
	vSe;
	part;
	affix;
	uw;
	pri;
	pres;
	bug;

#############################################################
# Templates
#############################################################

@m = [gender = masc];
@f = [gender = fem];
@s = [number = sg];
@p = [number = pl];
@1 = [person = 1];
@2 = [person = 2];
@3 = [person = 3];
@13 = [person = 1|3];
@12 = [person = 1|2];
@infls = [inflnumber = sg];
@inflp = [inflnumber = pl];
@infl1 = [inflperson = 1];
@infl2 = [inflperson = 2];
@infl3 = [inflperson = 3];
@infl13 = [inflperson = 1|3];
@infl12 = [inflperson = 1|2];
@ms = [@m, @s];
@mp = [@m, @p];
@fs = [@f, @s];
@fp = [@f, @p];
@1m = [@1, @m];
@2m = [@2, @m];
@3m = [@3, @m];
@1f = [@1, @f];
@2f = [@2, @f];
@3f = [@3, @f];
@1s = [@1, @s];
@2s = [@2, @s];
@3s = [@3, @s];
@1p = [@1, @p];
@2p = [@2, @p];
@3p = [@3, @p];
@infl1s = [@infl1, @infls];
@infl2s = [@infl2, @infls];
@infl3s = [@infl3, @infls];
@infl1p = [@infl1, @inflp];
@infl2p = [@infl2, @inflp];
@infl3p = [@infl3, @inflp];
@infl12s = [@infl12, @infls];
@infl13s = [@infl13, @infls];
@13s = [@13];
@12s = [@12];
@1ms = [@1, @ms];
@2ms = [@2, @ms];
@3ms = [@3, @ms];
@1mp = [@1, @mp];
@2mp = [@2, @mp];
@3mp = [@3, @mp];
@1fs = [@1, @fs];
@2fs = [@2, @fs];
@3fs = [@3, @fs];
@1fp = [@1, @fp];
@2fp = [@2, @fp];
@3fp = [@3, @fp];

@poss = [possessive = +];
@posss = [numberposs = sg];
@possp = [numberposs = pl];
@poss1 = [persposs = 1];
@poss2 = [persposs = 2];
@poss3 = [persposs = 3];

@fp_P1p = [@fp, @poss1, @possp];
@fp_P1s = [@fp, @poss1, @posss];
@fp_P2p = [@fp, @poss2, @possp];
@fp_P2s = [@fp, @poss2, @posss];
@fp_P3p = [@fp, @poss3, @possp];
@fp_P3s = [@fp, @poss3, @posss];
@fs_P1p = [@fs, @poss1, @possp];
@fs_P1s = [@fs, @poss1, @posss];
@fs_P2p = [@fs, @poss2, @possp];
@fs_P2s = [@fs, @poss2, @posss];
@fs_P3p = [@fs, @poss3, @possp];
@fs_P3s = [@fs, @poss3, @posss];
@mp_P1p = [@mp, @poss1, @possp];
@mp_P1s = [@mp, @poss1, @posss];
@mp_P2p = [@mp, @poss2, @possp];
@mp_P2s = [@mp, @poss2, @posss];
@mp_P3p = [@mp, @poss3, @possp];
@mp_P3s = [@mp, @poss3, @posss];
@ms_P1p = [@ms, @poss1, @possp];
@ms_P1s = [@ms, @poss1, @posss];
@ms_P2p = [@ms, @poss2, @possp];
@ms_P2s = [@ms, @poss2, @posss];
@ms_P3p = [@ms, @poss3, @possp];
@ms_P3s = [@ms, @poss3, @posss];
@p_P1p = [@p, @poss1, @possp];
@p_P1s = [@p, @poss1, @posss];
@p_P2p = [@p, @poss2, @possp];
@p_P2s = [@p, @poss2, @posss];
@p_P3p = [@p, @poss3, @possp];
@p_P3s = [@p, @poss3, @posss];
@s_P1p = [@s, @poss1, @possp];
@s_P1s = [@s, @poss1, @posss];
@s_P2p = [@s, @poss2, @possp];
@s_P2s = [@s, @poss2, @posss];
@s_P3p = [@s, @poss3, @possp];
@s_P3s = [@s, @poss3, @posss];

@imperative = [Suj=[pred="pro"]];

// temps simples
@P = [tense = present, mode = indicative, v-form=tensed];
@F = [tense = future, mode = indicative, v-form=tensed];
@I = [tense = imperfect, mode = indicative, v-form=tensed];
@J = [tense = past-historic, mode = indicative, v-form=tensed];
@T = [tense = imperfect, mode = subjonctive, v-form=tensed];
@Y = [tense = present, mode = imperative, v-form=tensed];
@S = [tense = present, mode = subjonctive, v-form=tensed];
@C = [tense = present, mode = conditional, v-form=tensed];

@K = [v-form = past-participle,tense=none];
@G = [v-form = participle];
@W = [v-form = infinitive];

// temps compos�s
@PP = [tense = perfect, mode = indicative];		// Pass� compos�			aux:P
@FF = [tense = future-perfect, mode = indicative];	// Futur ant�rieur			aux:F
@II = [tense = pluperfect, mode = indicative];		// Plus-que-parfait			aux:I
@JJ = [tense = past-anterior, mode = indicative];	// Pass� ant�rieur			aux:J
@SS = [tense = past, mode = subjonctive];		// Subjonctif pass�			aux:S
@TT = [tense = pluperfect, mode = subjonctive];		// Plus-que-parfait du subjonctif	aux:T
@CC = [tense = past, mode = conditional];		// Conditionnel pass� 			aux:C
@YY = [tense = past, mode = imperative];		// Imp�ratif pass�			aux:Y
@GG = [v-form = participle, tense = perfect];		// Participe pass� 2e forme		aux:G
@WW = [v-form = infinitive, tense = perfect];		// Infinitif pass�			aux:W

// temps ambigus
@PS = [tense = present, mode = indicative|subjonctive, v-form=tensed];
@PJ = [tense = present|past-historic, mode = indicative, v-form=tensed];
@YP = [tense = present, mode = imperative|indicative, v-form=tensed];
@ST = [tense = present|imperfect, mode = subjonctive, v-form=tensed];
@PFIJTSC = [mode = indicative|subjonctive|conditional, v-form=tensed];


@Kms = [@K, @m, @s];
@Kmp = [@K, @m, @p];
@Kfs = [@K, @f, @s];
@Kfp = [@K, @f, @p];
@Km = [@K, @m];
@Kf = [@K, @f];
@Ks = [@K, @s];
@Kp = [@K, @p];

@P1s = [@P, @infl1s];
@P2s = [@P, @infl2s];
@P3s = [@P, @infl3s];
@P1p = [@P, @infl1p];
@P2p = [@P, @infl2p];
@P3p = [@P, @infl3p];
@F1s = [@F, @infl1s];
@F2s = [@F, @infl2s];
@F3s = [@F, @infl3s];
@F1p = [@F, @infl1p];
@F2p = [@F, @infl2p];
@F3p = [@F, @infl3p];
@I1s = [@I, @infl1s];
@I2s = [@I, @infl2s];
@I3s = [@I, @infl3s];
@I1p = [@I, @infl1p];
@I2p = [@I, @infl2p];
@I3p = [@I, @infl3p];
@J1s = [@J, @infl1s];
@J2s = [@J, @infl2s];
@J3s = [@J, @infl3s];
@J1p = [@J, @infl1p];
@J2p = [@J, @infl2p];
@J3p = [@J, @infl3p];
@S1s = [@S, @infl1s];
@S2s = [@S, @infl2s];
@S3s = [@S, @infl3s];
@S1p = [@S, @infl1p];
@S2p = [@S, @infl2p];
@S3p = [@S, @infl3p];
@T1s = [@T, @infl1s];
@T2s = [@T, @infl2s];
@T3s = [@T, @infl3s];
@T1p = [@T, @infl1p];
@T2p = [@T, @infl2p];
@T3p = [@T, @infl3p];
@C1s = [@C, @infl1s];
@C2s = [@C, @infl2s];
@C3s = [@C, @infl3s];
@C1p = [@C, @infl1p];
@C2p = [@C, @infl2p];
@C3p = [@C, @infl3p];
@Y2s = [@Y, @infl2s];
@Y1p = [@Y, @infl1p];
@Y2p = [@Y, @infl2p];
@PP1s = [@PP, @infl1s];
@PP2s = [@PP, @infl2s];
@PP3s = [@PP, @infl3s];
@PP1p = [@PP, @infl1p];
@PP2p = [@PP, @infl2p];
@PP3p = [@PP, @infl3p];
@FF1s = [@FF, @infl1s];
@FF2s = [@FF, @infl2s];
@FF3s = [@FF, @infl3s];
@FF1p = [@FF, @infl1p];
@FF2p = [@FF, @infl2p];
@FF3p = [@FF, @infl3p];
@II1s = [@II, @infl1s];
@II2s = [@II, @infl2s];
@II3s = [@II, @infl3s];
@II1p = [@II, @infl1p];
@II2p = [@II, @infl2p];
@II3p = [@II, @infl3p];
@JJ1s = [@JJ, @infl1s];
@JJ2s = [@JJ, @infl2s];
@JJ3s = [@JJ, @infl3s];
@JJ1p = [@JJ, @infl1p];
@JJ2p = [@JJ, @infl2p];
@JJ3p = [@JJ, @infl3p];
@SS1s = [@SS, @infl1s];
@SS2s = [@SS, @infl2s];
@SS3s = [@SS, @infl3s];
@SS1p = [@SS, @infl1p];
@SS2p = [@SS, @infl2p];
@SS3p = [@SS, @infl3p];
@TT1s = [@TT, @infl1s];
@TT2s = [@TT, @infl2s];
@TT3s = [@TT, @infl3s];
@TT1p = [@TT, @infl1p];
@TT2p = [@TT, @infl2p];
@TT3p = [@TT, @infl3p];
@CC1s = [@CC, @infl1s];
@CC2s = [@CC, @infl2s];
@CC3s = [@CC, @infl3s];
@CC1p = [@CC, @infl1p];
@CC2p = [@CC, @infl2p];
@CC3p = [@CC, @infl3p];
@YY2s = [@YY, @infl2s];
@YY1p = [@YY, @infl1p];
@YY2p = [@YY, @infl2p];
@CC12s = [@CC, @infl12s];
@II12s = [@II, @infl12s];
@JJ12s = [@JJ, @infl12s];

@ST2s = [@ST, @infl2s];
@C12s = [@C, @infl12s];
@S13s = [@S, @infl13s];
@PS13s = [@PS, @infl13s];
@PS3p = [@PS, @infl3p];
@PS3s = [@PS, @infl3s];
@PS3 = [@PS, @infl3];
@PS2s = [@PS, @infl2s];
@I12s = [@I, @infl12s];
@J12s = [@J, @infl12s];
@P12s = [@P, @infl12s];
@PJ12s = [@PJ, @infl12s];
@YP2p = [@YP, @infl2p];

@avoir = [aux-req = avoir];
@�tre = [aux-req = �tre];
@favoir = [form-aux = avoir];
@f�tre = [form-aux = �tre];
@faller = [form-aux = aller];
@fvenir = [form-aux = venir];

@pcas� = [pcas = �];
@pcasde = [pcas = de];
@pcasavec = [pcas = avec];
@pcasapr�s = [pcas = apr�s];
@pcascomme = [pcas = comme];
@pcascontre = [pcas = contre];
@pcassur = [pcas = sur];
@pcasvers = [pcas = vers];
@pcaspour = [pcas = pour];
@pcasdans = [pcas = dans];
@pcaspar = [pcas = par];
@pcasdevant = [pcas = devant];
@pcasen = [pcas = en];

@pro_loc = [case = loc];
@pro_acc = [case = acc];
@pro_nom = [case = nom];
@pro_gen = [case = gen];

@ord = [type = ord];
@card = [type = card];

# restrictions
@hum = [hum = +];
@time = [time = +];
@weekday = [weekday = +];
@year = [year = +];
@loc = [locative = +];

@active = [diathesis = active];
@passive = [diathesis = passive, aux-req = �tre];
@pron = [refl =c +];
@pron_possible = [];
@impers = [imp =c +];
@pers = [imp = -];
@pseudo-en = [clgen =c +];
@pseudo-en_possible = [];
@pseudo-y = [clloc =c +];
@pseudo-le = [clle =c +];
@pseudo-la = [clla =c +];
@pseudo-les = [clles =c +];
@n�gatif = [clneg =c +];

// compl�tive objet=patient (actif)
@CompSubj = [];
@CompSujSubj = [];
@CompInd = [];
@CompSujInd = [];
@ACompSubj = [];
@ACompInd = [];
@DeCompSubj = [];
@DeCompInd = [];


// contr�le et attributif
@CtrlObjAtt = [Obj =? []1, Att =? [Suj =? []1]];
@CtrlObjObjde = [Obj =? []1, Objde =? [Suj =? []1]];
@CtrlObjObj� = [Obj =? []1, Obj� =? [Suj =? []1]];
@CtrlObj�Obj = [Obj� =? []1, Obj =? [Suj =? []1]];
@CtrlObj�Suj = [Obj� =? []1, Suj =? [Suj =? []1]];
@CtrlObj�Obl = [Obj� =? []1, Obl =? [Suj =? []1]];
@CtrlObjSuj = [Obj =? []1, Suj =? [Suj =? []1]];
@CtrlSujAtt = [Suj =? []1, Att =? [Suj =? []1]];
@CtrlSujLoc = [Suj =? []1, Loc =? [Suj =? []1]];
@CtrlSujObj = [Suj =? []1, Obj =? [Suj =? []1]];
@CtrlSujObjde = [Suj =? []1, Objde =? [Suj =? []1]];
@CtrlSujObj� = [Suj =? []1, Obj� =? [Suj =? []1]];
@CtrlSujObl = [Suj =? []1, Obl =? [Suj =? []1]];
@AttSuj = [Suj =? []1,Att =? [Suj =? []1]];
@AttObj = [Obj =? []1,Att =? [Suj =? []1]];
