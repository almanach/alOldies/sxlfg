#############################################################
# DECLARATION DES OBJETS DE LA GRAMMAIRE
#############################################################

#############################################################
# d�claration des attributs
#############################################################

ATTRIBUTE
//	temp = <[]>;		// champs pipo pour la LFG
//	temp3 = <[]>;		// 
//	temp4 = <[]>;		// 
//	tempC = <[]>;		// 
	subj = <[]>;		// sujet
	vsubj = <[]>;		// sujet phrastique infinitif 
	de-vsubj = <[]>;		// sujet phrastique de-infinitif 
	ssubj = <[]>;		// sujet phrastique fini 
	obj = <[]>;		// objet 
	npredobj = <[]>;		// pseudo-objet pr�dicatif d'un verbe support ("partie" dans "faire partie (de)")
	�-obj = <[]>;		// objet pr�positionnel en � 
	de-obj = <[]>;		// objet pr�positionnel en de 
	par-obj = <[]>;		// compl�ment oblique
	devant-obj = <[]>;	// compl�ment oblique
	dans-obj = <[]>;		// compl�ment oblique
	sur-obj = <[]>;		// compl�ment oblique
	en-obj = <[]>;		// compl�ment oblique
	contre-obj = <[]>;	// compl�ment oblique
	apr�s-obj = <[]>;		// compl�ment oblique
	avec-obj = <[]>;		// compl�ment oblique
	vers-obj = <[]>;		// compl�ment oblique
	pour-obj = <[]>;		// compl�ment oblique
	comme-obj = <[]>;		// compl�ment oblique
	sous-obj = <[]>;		// ABSENT DE LA LFG DE LIONEL
	autre-obj = <[]>; //A VOIR
	scomp = <[]>;		// compl�tive
	whcomp = <[]>;		// compl�tive introduite par combien, quand, comment, pourquoi
	�-whcomp = <[]>;		// compl�tive introduite par combien, quand, comment, pourquoi
	de-whcomp = <[]>;		// compl�tive introduite par combien, quand, comment, pourquoi
	�-scomp = <[]>;		// compl�tive � ce que
	de-scomp = <[]>;		// compl�tive de ce que
	dans-scomp = <[]>; //A VOIR
	sur-scomp = <[]>; //A VOIR
	vers-scomp = <[]>; //A VOIR
	par-scomp = <[]>; //A VOIR
	devant-scomp = <[]>; //A VOIR
	en-scomp = <[]>; //A VOIR
	contre-scomp = <[]>; //A VOIR
	apr�s-scomp = <[]>; //A VOIR
	avec-scomp = <[]>; //A VOIR
	autre-scomp = <[]>; //A VOIR
	pour-scomp = <[]>; //A VOIR
	comme-scomp = <[]>; //A VOIR
	vcomp = <[]>;		// infinitive
	�-vcomp = <[]>;		// infinitive compl�menteur �
	de-vcomp = <[]>;		// infinitive compl�menteur de
	par-vcomp = <[]>;		// infinitive compl�menteur par
	// -----
	// n'existe pas, mais peut �tre cr�� artificiellement par $$ ($i(pcas)-vcomp)
	// -----
	devant-vcomp = <[]>; 
	dans-vcomp = <[]>; 
	sur-vcomp = <[]>; 
	en-vcomp = <[]>; 
	contre-vcomp = <[]>; 
	apr�s-vcomp = <[]>; 
	avec-vcomp = <[]>; 
	vers-vcomp = <[]>; 
	pour-vcomp = <[]>; 
	comme-vcomp = <[]>; 
	// -----
//	prep-vcomp = <[]>; 	// infinitive compl�menteur oblique
	autre-vcomp = <[]>; //A VOIR
	acomp = <[]>; 		// nom / adjectif attribut : Jean est malade 
	�-acomp = <[]>;
	de-acomp = <[]>; 	// adjectif attribut oblique : Jean est comme malade. Jean passe pour malade 
	pour-acomp = <[]>; 	// adjectif attribut oblique : Jean est comme malade. Jean passe pour malade 
	comme-acomp = <[]>; 	// adjectif attribut oblique : Jean est comme malade. Jean passe pour malade 
	en-acomp = <[]>; 	// adjectif attribut oblique : Jean est comme malade. Jean passe pour malade 
	dans-acomp = <[]>;
	sur-acomp = <[]>;
	vers-acomp = <[]>;
	par-acomp = <[]>;
	devant-acomp = <[]>;
	contre-acomp = <[]>;
	apr�s-acomp = <[]>;
	avec-acomp = <[]>;
	autre-acomp = <[]>;

	adjunct = {[]}; 	// modifieur : �pith�te, relative, compl�ment de nom, compl�tive de nom ou d'adj, adverbe, etc.
//	vadjunct = {[]}; 	// participiale 
//	aadjunct = {[]}; 	// adjectif appos� 
//	topic = [];		// topicalisation d'un �l�ment extrait
//	focus = [];		// focalisation d'un �l�ment extrait
	arg1 = <[]>;		// argument d'une coordonn�e ou juxtapos�e
	arg2 = <[]>;		// argument d'une coordonn�e ou juxtapos�e

	//------------------------------------------------------------
	// attributs � valeur atomique
	//------------------------------------------------------------
	gender = {masc, fem}; 
	number = {sg, pl}; 
	person = {1, 2, 3};
	inflnumber = {sg, pl}; 
	inflperson = {1, 2, 3};
	numberposs = {sg, pl};		// pour les possessifs
	persposs = {1, 2, 3};
	case = {acc, dat, nom, loc, gen, rfl};
//	mode = {indicative, conditional, subjonctive, imperative, infinitive, participle};
//	tense = {present, past, imperfect, future, pluperfect, past-historic, past-anterior, future-perfect, perfect};
//	aspect = {neutral, inchoative, terminative, durative, notperfect, perfect};
//	v-form = {tensed, participle, infinitive};
	mode = {indicative, subjonctive, conditional, imperative};
	tense = {future, future-perfect, imperfect, past, past-anterior, past-historic, perfect, pluperfect, present, none};
	aspect = {neutral, inchoative, terminative, durative};
	v-form = {infinitive, participle, past-participle, tensed};
	form = {il, �a, si, que, comme, virgule};
	define = {+, -};
//	partitive = {+, -};
	demonstrative = {+, -};
	wh = {+, -};
	que = {+};
	possessive = {+, -};
	pcas = {�, de, dans, sur, vers, par, devant, en, contre, apr�s, avec, autre, pour, comme};
	neg = {+, -};
	clneg = {+, -};
	clgen = {+, -};
	type = {ind, card, ord, poss, int, qual, part, excl, rel, dem, def};
	diathesis = {active, passive};
	aux-req = {�tre, avoir};
	form-aux = {�tre, avoir, aller, venir, venir-de};
	fct = {subj, obj, de-obj, �-obj, dans-obj, vers-obj, en-obj}; // fonctions marqu�es par le lexique (relative)
	lightverb = {-,faire,prendre,avoir,donner,rester,garder,tomber,mettre};
	synt_head = {...};
//	degree = {+, -};
	qu = {+,-};
	det = {+,-};
	pre_det = {+};
	adv_kind = {intens,tr�s,modnc};
	advGP = {+};
	
	//----------restriction de s�lection--------------------------------------------------
	hum = {+, -};
//	anim = {+, -};
//	conc = {+, -};
	locative = {+, -};
	time = {+, -};
//	pobody = {+, -};

	//----------pour traduction automatique-----------------------------------------------
	cat = {adj, nc, np, v, adv, prep, coo, etr};

	//----------pour SXLFG-----------------------------------------------
	aij = {Aij, Pij ...}2;

	//--------- OUBLIS ? ---------------
//        dem = {+};
	  pro = {+, -};

#############################################################
# d�claration des cat�gories syntaxiques (auxiliaires)
#############################################################

CATEGORY
	COMP;
	S2;
	S3;
	SCooSubj;
	SCoo;
	WS;
	WREL;
	PWS;
	S;
	SCOMP;
	NP;
	AP;
	AP2;
	ADVA; // adverbe d'adjectif
	ADVP; // adverbe de phrase
	ADVV; // adverbe de verbe
	N;
	N2;
	N3;
	PP;
	VP;
	VP2;
	VPa;
	V;
	S;
	CLITIC;
	PREVERB;
	VERB;
	POSTVERB;
	V;

#SXLFG
	ADVP_ponctw;
	Ponctw_ADVP;
	Ponctw_ADVP_ponctw;
	Ponctw_poncts;
	Ponctw_PP;
	Ponctw_WREL;
	Clneg_advneg;

# Easy
	Easy_NV1;
	Easy_NV2;
	Easy_NV3;
	Easy_NV4;
	Easy_NV5;
	Easy_NV6;
	Easy_NV7;
	Easy_NV8;
	Easy_GN;
	Easy_GA;
	Easy_GP;
	Easy_InflV;
	Easy_InflAuxAvoir;
	Easy_InflAuxEtre;
	Easy_PV;

#############################################################
# d�claration des cat�gories lexicales (terminales)
#############################################################

TERMINAL	// liste p�rim�e mais (semble-t-il) inutilis�e
	adj;
	adv;
	advneg;
	auxAvoir;
	auxEtre;
	auxAller;
	auxVenir;
	cla;
	clar;
	cld;
	cldr;
	clg;
	cln;
	clneg;
	clr;
	cll;
	coo;
	comp;
	csu;
	det;
	int;
	nc;
	ncpred;
	np;
	ponctp;  //-, >>, ponctuations pr�fixes
	ponctf;  //etc., ponctuations suffixes
	poncts;
	ponctw;
	prel;
	prep;
	pro;
	v;
	vSe;
	part;
	affix;
	uw;
	pri;
	pres;
	bug;

#############################################################
# Liste des non-terminaux qui doivent etre complets et coherents et
# sur lesquels on fait du ranking
#############################################################
COMPLETE
	ENONCES '$182453679pn0';
	PHRASE '$182453679pn';
	S4 '$182453679pn';
	S3 '$182453679pn';
	S2 '$182453679pn';
	SCOMPranked '$18453679pn';
	Pri_WSsubj '$18453679pn';
	Pri_WSobj '$18453679pn';
	Pri_WSdans_obj '$18453679pn';
	Pri_WSadjunct '$18453679pn';
	Pri_WS_dir '$18453679pn';
	WREL '$18453679pn';

# met-on 'p' ou 'n' sur ceux qui suivent ?
	PREVERB '1845679'; // pas 2
	POSTVERB '1845679'; // pas 2
	NP '1845679'; // pas de 2
#	PREVERB '18345679'; // pas 2
#	POSTVERB '18345679'; // pas 2
#	NP '18345679'; // pas de 2

#############################################################
# Templates
#############################################################

@m = [gender = masc];
@f = [gender = fem];
@s = [number = sg];
@p = [number = pl];
@1 = [person = 1];
@2 = [person = 2];
@3 = [person = 3];
@13 = [person = 1|3];
@12 = [person = 1|2];
@infls = [inflnumber = sg];
@inflp = [inflnumber = pl];
@infl1 = [inflperson = 1];
@infl2 = [inflperson = 2];
@infl3 = [inflperson = 3];
@infl13 = [inflperson = 1|3];
@infl12 = [inflperson = 1|2];
@ms = [@m, @s];
@mp = [@m, @p];
@fs = [@f, @s];
@fp = [@f, @p];
@1m = [@1, @m];
@2m = [@2, @m];
@3m = [@3, @m];
@1f = [@1, @f];
@2f = [@2, @f];
@3f = [@3, @f];
@1s = [@1, @s];
@2s = [@2, @s];
@3s = [@3, @s];
@1p = [@1, @p];
@2p = [@2, @p];
@3p = [@3, @p];
@infl1s = [@infl1, @infls];
@infl2s = [@infl2, @infls];
@infl3s = [@infl3, @infls];
@infl1p = [@infl1, @inflp];
@infl2p = [@infl2, @inflp];
@infl3p = [@infl3, @inflp];
@infl12s = [@infl12, @infls];
@infl13s = [@infl13, @infls];
@13s = [@13];
@12s = [@12];
@1ms = [@1, @ms];
@2ms = [@2, @ms];
@3ms = [@3, @ms];
@1mp = [@1, @mp];
@2mp = [@2, @mp];
@3mp = [@3, @mp];
@1fs = [@1, @fs];
@2fs = [@2, @fs];
@3fs = [@3, @fs];
@1fp = [@1, @fp];
@2fp = [@2, @fp];
@3fp = [@3, @fp];
//	@1ms-s = [@1ms, numberposs = sg]; // le mien
//	@2ms-s = [@2ms, numberposs = sg]; // le tien
//	@3ms-s = [@3ms, numberposs = sg]; // le sien
//	@1mp-s = [@1mp, numberposs = sg]; // les miens
//	@2mp-s = [@2mp, numberposs = sg]; // les tiens
//	@3mp-s = [@3mp, numberposs = sg]; // les siens
//	@1fs-s = [@1fs, numberposs = sg]; // la mienne
//	@2fs-s = [@2fs, numberposs = sg]; // la tienne
//	@3fs-s = [@3fs, numberposs = sg]; // la sienne
//	@1fp-s = [@1fp, numberposs = sg]; // les miennes
//	@2fp-s = [@2fp, numberposs = sg]; // les tiennes
//	@3fp-s = [@3fp, numberposs = sg]; // les siennes
//	@1ms-p = [@1ms, numberposs = pl]; // le n�tre
//	@2ms-p = [@2ms, numberposs = pl]; // le v�tre
//	@3ms-p = [@3ms, numberposs = pl]; // le leur
//	@1mp-p = [@1mp, numberposs = pl]; // les n�tres
//	@2mp-p = [@2mp, numberposs = pl]; // les v�tres
//	@3mp-p = [@3mp, numberposs = pl]; // les leurs
//	@1fs-p = [@1fs, numberposs = pl]; // la n�tre
//	@2fs-p = [@2fs, numberposs = pl]; // la v�tre
//	@3fs-p = [@3fs, numberposs = pl]; // la leur
//	@1fp-p = [@1fp, numberposs = pl]; // Les n�tres
//	@2fp-p = [@2fp, numberposs = pl]; // les v�tres
//	@3fp-p = [@3fp, numberposs = pl]; // les leurs

@poss = [possessive = +];
@posss = [numberposs = sg];
@possp = [numberposs = pl];
@poss1 = [persposs = 1];
@poss2 = [persposs = 2];
@poss3 = [persposs = 3];

@fp_P1p = [@fp, @poss1, @possp];
@fp_P1s = [@fp, @poss1, @posss];
@fp_P2p = [@fp, @poss2, @possp];
@fp_P2s = [@fp, @poss2, @posss];
@fp_P3p = [@fp, @poss3, @possp];
@fp_P3s = [@fp, @poss3, @posss];
@fs_P1p = [@fs, @poss1, @possp];
@fs_P1s = [@fs, @poss1, @posss];
@fs_P2p = [@fs, @poss2, @possp];
@fs_P2s = [@fs, @poss2, @posss];
@fs_P3p = [@fs, @poss3, @possp];
@fs_P3s = [@fs, @poss3, @posss];
@mp_P1p = [@mp, @poss1, @possp];
@mp_P1s = [@mp, @poss1, @posss];
@mp_P2p = [@mp, @poss2, @possp];
@mp_P2s = [@mp, @poss2, @posss];
@mp_P3p = [@mp, @poss3, @possp];
@mp_P3s = [@mp, @poss3, @posss];
@ms_P1p = [@ms, @poss1, @possp];
@ms_P1s = [@ms, @poss1, @posss];
@ms_P2p = [@ms, @poss2, @possp];
@ms_P2s = [@ms, @poss2, @posss];
@ms_P3p = [@ms, @poss3, @possp];
@ms_P3s = [@ms, @poss3, @posss];
@p_P1p = [@p, @poss1, @possp];
@p_P1s = [@p, @poss1, @posss];
@p_P2p = [@p, @poss2, @possp];
@p_P2s = [@p, @poss2, @posss];
@p_P3p = [@p, @poss3, @possp];
@p_P3s = [@p, @poss3, @posss];
@s_P1p = [@s, @poss1, @possp];
@s_P1s = [@s, @poss1, @posss];
@s_P2p = [@s, @poss2, @possp];
@s_P2s = [@s, @poss2, @posss];
@s_P3p = [@s, @poss3, @possp];
@s_P3s = [@s, @poss3, @posss];

@Ams = [subj = [@m, @s]];
@Amp = [subj = [@m, @p]];
@Afs = [subj = [@f, @s]];
@Afp = [subj = [@f, @p]];

	@imperative = [subj=[pred="pro"]];

// temps simples
	@P = [tense = present, mode = indicative, v-form=tensed];
	@F = [tense = future, mode = indicative, v-form=tensed];
	@I = [tense = imperfect, mode = indicative, v-form=tensed];
	@J = [tense = past-historic, mode = indicative, v-form=tensed];
	@T = [tense = imperfect, mode = subjonctive, v-form=tensed];
	@Y = [tense = present, mode = imperative, v-form=tensed];
	@S = [tense = present, mode = subjonctive, v-form=tensed];
	@C = [tense = present, mode = conditional, v-form=tensed];

	@K = [v-form = past-participle,tense=none];
	@G = [v-form = participle];
@W = [v-form = infinitive];

// temps compos�s
@PP = [tense = perfect, mode = indicative];		// Pass� compos�			aux:P
@FF = [tense = future-perfect, mode = indicative];	// Futur ant�rieur			aux:F
@II = [tense = pluperfect, mode = indicative];		// Plus-que-parfait			aux:I
@JJ = [tense = past-anterior, mode = indicative];	// Pass� ant�rieur			aux:J
@SS = [tense = past, mode = subjonctive];		// Subjonctif pass�			aux:S
@TT = [tense = pluperfect, mode = subjonctive];		// Plus-que-parfait du subjonctif	aux:T
@CC = [tense = past, mode = conditional];		// Conditionnel pass� 			aux:C
@YY = [tense = past, mode = imperative];		// Imp�ratif pass�			aux:Y
@GG = [v-form = participle, tense = perfect];		// Participe pass� 2e forme		aux:G
@WW = [v-form = infinitive, tense = perfect];		// Infinitif pass�			aux:W

// temps ambigus
@PS = [tense = present, mode = indicative|subjonctive, v-form=tensed];
@PJ = [tense = present|past-historic, mode = indicative, v-form=tensed];
@YP = [tense = present, mode = imperative|indicative, v-form=tensed];
@ST = [tense = present|imperfect, mode = subjonctive, v-form=tensed];
@PFIJTSC = [mode = indicative|subjonctive|conditional, v-form=tensed];


@Kms = [@K, @m, @s];
@Kmp = [@K, @m, @p];
@Kfs = [@K, @f, @s];
@Kfp = [@K, @f, @p];
@Km = [@K, @m];
@Kf = [@K, @f];
@Ks = [@K, @s];
@Kp = [@K, @p];

@P1s = [@P, @infl1s];
@P2s = [@P, @infl2s];
@P3s = [@P, @infl3s];
@P1p = [@P, @infl1p];
@P2p = [@P, @infl2p];
@P3p = [@P, @infl3p];
@F1s = [@F, @infl1s];
@F2s = [@F, @infl2s];
@F3s = [@F, @infl3s];
@F1p = [@F, @infl1p];
@F2p = [@F, @infl2p];
@F3p = [@F, @infl3p];
@I1s = [@I, @infl1s];
@I2s = [@I, @infl2s];
@I3s = [@I, @infl3s];
@I1p = [@I, @infl1p];
@I2p = [@I, @infl2p];
@I3p = [@I, @infl3p];
@J1s = [@J, @infl1s];
@J2s = [@J, @infl2s];
@J3s = [@J, @infl3s];
@J1p = [@J, @infl1p];
@J2p = [@J, @infl2p];
@J3p = [@J, @infl3p];
@S1s = [@S, @infl1s];
@S2s = [@S, @infl2s];
@S3s = [@S, @infl3s];
@S1p = [@S, @infl1p];
@S2p = [@S, @infl2p];
@S3p = [@S, @infl3p];
@T1s = [@T, @infl1s];
@T2s = [@T, @infl2s];
@T3s = [@T, @infl3s];
@T1p = [@T, @infl1p];
@T2p = [@T, @infl2p];
@T3p = [@T, @infl3p];
@C1s = [@C, @infl1s];
@C2s = [@C, @infl2s];
@C3s = [@C, @infl3s];
@C1p = [@C, @infl1p];
@C2p = [@C, @infl2p];
@C3p = [@C, @infl3p];
@Y2s = [@Y, @infl2s];
@Y1p = [@Y, @infl1p];
@Y2p = [@Y, @infl2p];
@PP1s = [@PP, @infl1s];
@PP2s = [@PP, @infl2s];
@PP3s = [@PP, @infl3s];
@PP1p = [@PP, @infl1p];
@PP2p = [@PP, @infl2p];
@PP3p = [@PP, @infl3p];
@FF1s = [@FF, @infl1s];
@FF2s = [@FF, @infl2s];
@FF3s = [@FF, @infl3s];
@FF1p = [@FF, @infl1p];
@FF2p = [@FF, @infl2p];
@FF3p = [@FF, @infl3p];
@II1s = [@II, @infl1s];
@II2s = [@II, @infl2s];
@II3s = [@II, @infl3s];
@II1p = [@II, @infl1p];
@II2p = [@II, @infl2p];
@II3p = [@II, @infl3p];
@JJ1s = [@JJ, @infl1s];
@JJ2s = [@JJ, @infl2s];
@JJ3s = [@JJ, @infl3s];
@JJ1p = [@JJ, @infl1p];
@JJ2p = [@JJ, @infl2p];
@JJ3p = [@JJ, @infl3p];
@SS1s = [@SS, @infl1s];
@SS2s = [@SS, @infl2s];
@SS3s = [@SS, @infl3s];
@SS1p = [@SS, @infl1p];
@SS2p = [@SS, @infl2p];
@SS3p = [@SS, @infl3p];
@TT1s = [@TT, @infl1s];
@TT2s = [@TT, @infl2s];
@TT3s = [@TT, @infl3s];
@TT1p = [@TT, @infl1p];
@TT2p = [@TT, @infl2p];
@TT3p = [@TT, @infl3p];
@CC1s = [@CC, @infl1s];
@CC2s = [@CC, @infl2s];
@CC3s = [@CC, @infl3s];
@CC1p = [@CC, @infl1p];
@CC2p = [@CC, @infl2p];
@CC3p = [@CC, @infl3p];
@YY2s = [@YY, @infl2s];
@YY1p = [@YY, @infl1p];
@YY2p = [@YY, @infl2p];
@CC12s = [@CC, @infl12s];
@II12s = [@II, @infl12s];
@JJ12s = [@JJ, @infl12s];

@ST2s = [@ST, @infl2s];
@C12s = [@C, @infl12s];
@S13s = [@S, @infl13s];
@PS13s = [@PS, @infl13s];
@PS3p = [@PS, @infl3p];
@PS3s = [@PS, @infl3s];
@PS3 = [@PS, @infl3];
@PS2s = [@PS, @infl2s];
@I12s = [@I, @infl12s];
@J12s = [@J, @infl12s];
@P12s = [@P, @infl12s];
@PJ12s = [@PJ, @infl12s];
@YP2p = [@YP, @infl2p];

@avoir = [aux-req = avoir];
@�tre = [aux-req = �tre];
@favoir = [form-aux = avoir];
@f�tre = [form-aux = �tre];
@faller = [form-aux = aller];
@fvenir = [form-aux = venir];

@pcas� = [pcas = �];
@pcasde = [pcas = de];
@pcasavec = [pcas = avec];
@pcasapr�s = [pcas = apr�s];
@pcascomme = [pcas = comme];
@pcascontre = [pcas = contre];
@pcassur = [pcas = sur];
@pcasvers = [pcas = vers];
@pcaspour = [pcas = pour];
@pcasdans = [pcas = dans];
@pcaspar = [pcas = par];
@pcasdevant = [pcas = devant];
@pcasen = [pcas = en];

@pro_loc = [fct = dans-obj|�-obj|vers-obj|en-obj];
@pro_acc = [fct = obj];
@pro_nom = [fct = subj];
@pro_gen = [fct = de-obj];

@ord = [type = ord];
@card = [type = card];

# restrictions
@hum = [hum = +];
@time = [time = +];
@loc = [locative = +];

@active = [diathesis = active];
@passive = [diathesis = passive];
@pron = [refl =c +];
@pron_possible = [];
@impers = [imp =c +];
@pers = [imp = -];
@pseudo-en = [clgen =c +];
@pseudo-y = [clloc =c +];
@pseudo-le = [clle =c +];
@pseudo-la = [clla =c +];
@pseudo-les = [clles =c +];
@n�gatif = [clneg =c +];

// compl�tive objet=patient (actif)
@CompSubj = [];
@CompSujSubj = [];
@CompInd = [];
@CompSujInd = [];
@ACompSubj = [];
@ACompInd = [];
@DeCompSubj = [];
@DeCompInd = [];


// contr�le et attributif
@CtrlObjAtt = [];
@CtrlObjObjde = [];
@CtrlObjObj� = [];
@CtrlObj�Obj = [];
@CtrlObj�Suj = [];
@CtrlObjSuj = [];
@CtrlSujAtt = [];
@CtrlSujLoc = [];
@CtrlSujObj = [];
@CtrlSujObjde = [];
@CtrlSujObj� = [];
@CtrlSujObl = [];
@AttSuj = [];
@AttObj = [];
